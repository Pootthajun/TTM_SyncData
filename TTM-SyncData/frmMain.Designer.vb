﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtHH = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtMM = New System.Windows.Forms.TextBox()
        Me.cbAuto = New System.Windows.Forms.CheckBox()
        Me.TimerAutoSync = New System.Windows.Forms.Timer(Me.components)
        Me.txtAttention = New System.Windows.Forms.TextBox()
        Me.btnDbSetting = New System.Windows.Forms.Button()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblLastSync_tb_PK_COURSE = New System.Windows.Forms.Label()
        Me.lblRec_tb_PK_COURSE = New System.Windows.Forms.Label()
        Me.lblStatus_tb_PK_COURSE = New System.Windows.Forms.Label()
        Me.btn_tb_PK_COURSE = New System.Windows.Forms.Button()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.btn_tb_PN_POSITION_FLD_FUNC = New System.Windows.Forms.Button()
        Me.lblLastSync_tb_PN_POSITION_FLD_FUNC = New System.Windows.Forms.Label()
        Me.lblRec_tb_PN_POSITION_FLD_FUNC = New System.Windows.Forms.Label()
        Me.lblStatus_tb_PN_POSITION_FLD_FUNC = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblLastSync_tb_PN_LEAVE_T = New System.Windows.Forms.Label()
        Me.lblRec_tb_PN_LEAVE_T = New System.Windows.Forms.Label()
        Me.lblStatus_tb_PN_LEAVE_T = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.btn_tb_PN_POSITION_T = New System.Windows.Forms.Button()
        Me.btn_tb_PN_POSITION_MGR_R = New System.Windows.Forms.Button()
        Me.btn_tb_PN_POSITION_FLD_R = New System.Windows.Forms.Button()
        Me.btn_tb_PN_PERSONAL_VIEW = New System.Windows.Forms.Button()
        Me.btn_tb_PN_PERSONAL_STAT_R = New System.Windows.Forms.Button()
        Me.btn_tb_PN_DEPARTMENT_R = New System.Windows.Forms.Button()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.lblLastSync_tb_PN_POSITION_T = New System.Windows.Forms.Label()
        Me.lblLastSync_tb_PN_POSITION_MGR_R = New System.Windows.Forms.Label()
        Me.lblLastSync_tb_PN_POSITION_FLD_R = New System.Windows.Forms.Label()
        Me.lblLastSync_tb_PN_PERSONAL_VIEW = New System.Windows.Forms.Label()
        Me.lblLastSync_tb_PN_PERSONAL_STAT_R = New System.Windows.Forms.Label()
        Me.lblLastSync_tb_PN_DEPARTMENT_R = New System.Windows.Forms.Label()
        Me.lblRec_tb_PN_POSITION_T = New System.Windows.Forms.Label()
        Me.lblRec_tb_PN_POSITION_MGR_R = New System.Windows.Forms.Label()
        Me.lblRec_tb_PN_POSITION_FLD_R = New System.Windows.Forms.Label()
        Me.lblRec_tb_PN_PERSONAL_VIEW = New System.Windows.Forms.Label()
        Me.lblRec_tb_PN_PERSONAL_STAT_R = New System.Windows.Forms.Label()
        Me.lblRec_tb_PN_DEPARTMENT_R = New System.Windows.Forms.Label()
        Me.lblStatus_tb_PN_POSITION_T = New System.Windows.Forms.Label()
        Me.lblStatus_tb_PN_POSITION_MGR_R = New System.Windows.Forms.Label()
        Me.lblStatus_tb_PN_POSITION_FLD_R = New System.Windows.Forms.Label()
        Me.lblStatus_tb_PN_PERSONAL_VIEW = New System.Windows.Forms.Label()
        Me.lblStatus_tb_PN_PERSONAL_STAT_R = New System.Windows.Forms.Label()
        Me.lblStatus_tb_PN_DEPARTMENT_R = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btn_tb_PN_LEAVE_T = New System.Windows.Forms.Button()
        Me.btn_tb_PK_SUBJ_G = New System.Windows.Forms.Button()
        Me.lblLastSync_tb_PK_SUBJ_G = New System.Windows.Forms.Label()
        Me.lblRec_tb_PK_SUBJ_G = New System.Windows.Forms.Label()
        Me.lblStatus_tb_PK_SUBJ_G = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.btn_tb_PN_POSITION_RETURN2CLASS = New System.Windows.Forms.Button()
        Me.lblLastSync_tb_PN_POSITION_RETURN2CLASS = New System.Windows.Forms.Label()
        Me.lblRec_tb_PN_POSITION_RETURN2CLASS = New System.Windows.Forms.Label()
        Me.lblStatus_tb_PN_POSITION_RETURN2CLASS = New System.Windows.Forms.Label()
        Me.lblLastSync_tb_PN_POSITION_HISTORY_T = New System.Windows.Forms.Label()
        Me.lblRec_tb_PN_POSITION_HISTORY_T = New System.Windows.Forms.Label()
        Me.lblStatus_tb_PN_POSITION_HISTORY_T = New System.Windows.Forms.Label()
        Me.btn_tb_PN_POSITION_HISTORY_T = New System.Windows.Forms.Button()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.btn_tb_CP_SUBJECT_PERSONAL = New System.Windows.Forms.Button()
        Me.lblLastSync_tb_CP_SUBJECT_PERSONAL = New System.Windows.Forms.Label()
        Me.lblRec_tb_CP_SUBJECT_PERSONAL = New System.Windows.Forms.Label()
        Me.lblStatus_tb_CP_SUBJECT_PERSONAL = New System.Windows.Forms.Label()
        Me.lblLastSync_tb_CP_SUBJECT_MASTER = New System.Windows.Forms.Label()
        Me.lblRec_tb_CP_SUBJECT_MASTER = New System.Windows.Forms.Label()
        Me.lblStatus_tb_CP_SUBJECT_MASTER = New System.Windows.Forms.Label()
        Me.btn_tb_CP_SUBJECT_MASTER = New System.Windows.Forms.Button()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.btn_tb_PN_PUNISH_CODE_R = New System.Windows.Forms.Button()
        Me.lblLastSync_tb_PN_PUNISH_CODE_R = New System.Windows.Forms.Label()
        Me.lblRec_tb_PN_PUNISH_CODE_R = New System.Windows.Forms.Label()
        Me.lblStatus_tb_PN_PUNISH_CODE_R = New System.Windows.Forms.Label()
        Me.lblLastSync_tb_PN_PUNISH_T = New System.Windows.Forms.Label()
        Me.lblRec_tb_PN_PUNISH_T = New System.Windows.Forms.Label()
        Me.lblStatus_tb_PN_PUNISH_T = New System.Windows.Forms.Label()
        Me.btn_tb_PN_PUNISH_T = New System.Windows.Forms.Button()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.btn_tb_PK_EMP_SELECT = New System.Windows.Forms.Button()
        Me.lblLastSync_tb_PK_EMP_SELECT = New System.Windows.Forms.Label()
        Me.lblRec_tb_PK_EMP_SELECT = New System.Windows.Forms.Label()
        Me.lblStatus_tb_PK_EMP_SELECT = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnStartAll = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(145, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Sync Time (HH:MM)"
        '
        'txtHH
        '
        Me.txtHH.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtHH.Location = New System.Drawing.Point(163, 15)
        Me.txtHH.MaxLength = 2
        Me.txtHH.Name = "txtHH"
        Me.txtHH.Size = New System.Drawing.Size(37, 22)
        Me.txtHH.TabIndex = 1
        Me.txtHH.Text = "00"
        Me.txtHH.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(201, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(12, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = ":"
        '
        'txtMM
        '
        Me.txtMM.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtMM.Location = New System.Drawing.Point(214, 15)
        Me.txtMM.MaxLength = 2
        Me.txtMM.Name = "txtMM"
        Me.txtMM.Size = New System.Drawing.Size(37, 22)
        Me.txtMM.TabIndex = 3
        Me.txtMM.Text = "00"
        Me.txtMM.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cbAuto
        '
        Me.cbAuto.AutoSize = True
        Me.cbAuto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbAuto.Location = New System.Drawing.Point(268, 17)
        Me.cbAuto.Name = "cbAuto"
        Me.cbAuto.Size = New System.Drawing.Size(58, 20)
        Me.cbAuto.TabIndex = 4
        Me.cbAuto.Text = "Auto"
        Me.cbAuto.UseVisualStyleBackColor = True
        '
        'TimerAutoSync
        '
        Me.TimerAutoSync.Enabled = True
        Me.TimerAutoSync.Interval = 60000
        '
        'txtAttention
        '
        Me.txtAttention.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAttention.Location = New System.Drawing.Point(12, 630)
        Me.txtAttention.Multiline = True
        Me.txtAttention.Name = "txtAttention"
        Me.txtAttention.Size = New System.Drawing.Size(796, 40)
        Me.txtAttention.TabIndex = 41
        '
        'btnDbSetting
        '
        Me.btnDbSetting.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDbSetting.FlatAppearance.BorderSize = 0
        Me.btnDbSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDbSetting.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnDbSetting.ForeColor = System.Drawing.Color.White
        Me.btnDbSetting.Image = Global.TTM_SyncData.My.Resources.Resources.server
        Me.btnDbSetting.Location = New System.Drawing.Point(753, 10)
        Me.btnDbSetting.Name = "btnDbSetting"
        Me.btnDbSetting.Size = New System.Drawing.Size(48, 32)
        Me.btnDbSetting.TabIndex = 42
        Me.btnDbSetting.Tag = ""
        Me.btnDbSetting.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.ToolTip.SetToolTip(Me.btnDbSetting, "Database Setting")
        Me.btnDbSetting.UseVisualStyleBackColor = False
        '
        'lblLastSync_tb_PK_COURSE
        '
        Me.lblLastSync_tb_PK_COURSE.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_PK_COURSE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_PK_COURSE.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_PK_COURSE.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_PK_COURSE.Location = New System.Drawing.Point(515, 333)
        Me.lblLastSync_tb_PK_COURSE.Name = "lblLastSync_tb_PK_COURSE"
        Me.lblLastSync_tb_PK_COURSE.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_PK_COURSE.TabIndex = 108
        Me.lblLastSync_tb_PK_COURSE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_PK_COURSE
        '
        Me.lblRec_tb_PK_COURSE.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_PK_COURSE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_PK_COURSE.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_PK_COURSE.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_PK_COURSE.Location = New System.Drawing.Point(421, 333)
        Me.lblRec_tb_PK_COURSE.Name = "lblRec_tb_PK_COURSE"
        Me.lblRec_tb_PK_COURSE.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_PK_COURSE.TabIndex = 107
        Me.lblRec_tb_PK_COURSE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_PK_COURSE
        '
        Me.lblStatus_tb_PK_COURSE.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_PK_COURSE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_PK_COURSE.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_PK_COURSE.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_PK_COURSE.Location = New System.Drawing.Point(242, 333)
        Me.lblStatus_tb_PK_COURSE.Name = "lblStatus_tb_PK_COURSE"
        Me.lblStatus_tb_PK_COURSE.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_PK_COURSE.TabIndex = 106
        Me.lblStatus_tb_PK_COURSE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_tb_PK_COURSE
        '
        Me.btn_tb_PK_COURSE.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_PK_COURSE.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_PK_COURSE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_PK_COURSE.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_PK_COURSE.ForeColor = System.Drawing.Color.White
        Me.btn_tb_PK_COURSE.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_PK_COURSE.Location = New System.Drawing.Point(718, 333)
        Me.btn_tb_PK_COURSE.Name = "btn_tb_PK_COURSE"
        Me.btn_tb_PK_COURSE.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_PK_COURSE.TabIndex = 105
        Me.btn_tb_PK_COURSE.Tag = ""
        Me.btn_tb_PK_COURSE.Text = "Start"
        Me.btn_tb_PK_COURSE.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_PK_COURSE.UseVisualStyleBackColor = False
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.Color.White
        Me.Label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(20, 333)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(223, 32)
        Me.Label16.TabIndex = 104
        Me.Label16.Text = "tb_PK_COURSE"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btn_tb_PN_POSITION_FLD_FUNC
        '
        Me.btn_tb_PN_POSITION_FLD_FUNC.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_PN_POSITION_FLD_FUNC.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_PN_POSITION_FLD_FUNC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_PN_POSITION_FLD_FUNC.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_PN_POSITION_FLD_FUNC.ForeColor = System.Drawing.Color.White
        Me.btn_tb_PN_POSITION_FLD_FUNC.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_PN_POSITION_FLD_FUNC.Location = New System.Drawing.Point(718, 302)
        Me.btn_tb_PN_POSITION_FLD_FUNC.Name = "btn_tb_PN_POSITION_FLD_FUNC"
        Me.btn_tb_PN_POSITION_FLD_FUNC.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_PN_POSITION_FLD_FUNC.TabIndex = 103
        Me.btn_tb_PN_POSITION_FLD_FUNC.Tag = ""
        Me.btn_tb_PN_POSITION_FLD_FUNC.Text = "Start"
        Me.btn_tb_PN_POSITION_FLD_FUNC.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_PN_POSITION_FLD_FUNC.UseVisualStyleBackColor = False
        '
        'lblLastSync_tb_PN_POSITION_FLD_FUNC
        '
        Me.lblLastSync_tb_PN_POSITION_FLD_FUNC.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_PN_POSITION_FLD_FUNC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_PN_POSITION_FLD_FUNC.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_PN_POSITION_FLD_FUNC.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_PN_POSITION_FLD_FUNC.Location = New System.Drawing.Point(515, 302)
        Me.lblLastSync_tb_PN_POSITION_FLD_FUNC.Name = "lblLastSync_tb_PN_POSITION_FLD_FUNC"
        Me.lblLastSync_tb_PN_POSITION_FLD_FUNC.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_PN_POSITION_FLD_FUNC.TabIndex = 102
        Me.lblLastSync_tb_PN_POSITION_FLD_FUNC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_PN_POSITION_FLD_FUNC
        '
        Me.lblRec_tb_PN_POSITION_FLD_FUNC.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_PN_POSITION_FLD_FUNC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_PN_POSITION_FLD_FUNC.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_PN_POSITION_FLD_FUNC.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_PN_POSITION_FLD_FUNC.Location = New System.Drawing.Point(421, 302)
        Me.lblRec_tb_PN_POSITION_FLD_FUNC.Name = "lblRec_tb_PN_POSITION_FLD_FUNC"
        Me.lblRec_tb_PN_POSITION_FLD_FUNC.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_PN_POSITION_FLD_FUNC.TabIndex = 101
        Me.lblRec_tb_PN_POSITION_FLD_FUNC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_PN_POSITION_FLD_FUNC
        '
        Me.lblStatus_tb_PN_POSITION_FLD_FUNC.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_PN_POSITION_FLD_FUNC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_PN_POSITION_FLD_FUNC.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_PN_POSITION_FLD_FUNC.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_PN_POSITION_FLD_FUNC.Location = New System.Drawing.Point(242, 302)
        Me.lblStatus_tb_PN_POSITION_FLD_FUNC.Name = "lblStatus_tb_PN_POSITION_FLD_FUNC"
        Me.lblStatus_tb_PN_POSITION_FLD_FUNC.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_PN_POSITION_FLD_FUNC.TabIndex = 100
        Me.lblStatus_tb_PN_POSITION_FLD_FUNC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.White
        Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(20, 302)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(223, 32)
        Me.Label14.TabIndex = 99
        Me.Label14.Text = "tb_PN_POSITION_FLD_FUNC"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLastSync_tb_PN_LEAVE_T
        '
        Me.lblLastSync_tb_PN_LEAVE_T.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_PN_LEAVE_T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_PN_LEAVE_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_PN_LEAVE_T.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_PN_LEAVE_T.Location = New System.Drawing.Point(515, 271)
        Me.lblLastSync_tb_PN_LEAVE_T.Name = "lblLastSync_tb_PN_LEAVE_T"
        Me.lblLastSync_tb_PN_LEAVE_T.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_PN_LEAVE_T.TabIndex = 98
        Me.lblLastSync_tb_PN_LEAVE_T.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_PN_LEAVE_T
        '
        Me.lblRec_tb_PN_LEAVE_T.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_PN_LEAVE_T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_PN_LEAVE_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_PN_LEAVE_T.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_PN_LEAVE_T.Location = New System.Drawing.Point(421, 271)
        Me.lblRec_tb_PN_LEAVE_T.Name = "lblRec_tb_PN_LEAVE_T"
        Me.lblRec_tb_PN_LEAVE_T.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_PN_LEAVE_T.TabIndex = 97
        Me.lblRec_tb_PN_LEAVE_T.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_PN_LEAVE_T
        '
        Me.lblStatus_tb_PN_LEAVE_T.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_PN_LEAVE_T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_PN_LEAVE_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_PN_LEAVE_T.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_PN_LEAVE_T.Location = New System.Drawing.Point(242, 271)
        Me.lblStatus_tb_PN_LEAVE_T.Name = "lblStatus_tb_PN_LEAVE_T"
        Me.lblStatus_tb_PN_LEAVE_T.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_PN_LEAVE_T.TabIndex = 96
        Me.lblStatus_tb_PN_LEAVE_T.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.White
        Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(20, 271)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(223, 32)
        Me.Label13.TabIndex = 95
        Me.Label13.Text = "tb_PN_LEAVE_T"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btn_tb_PN_POSITION_T
        '
        Me.btn_tb_PN_POSITION_T.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_PN_POSITION_T.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_PN_POSITION_T.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_PN_POSITION_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_PN_POSITION_T.ForeColor = System.Drawing.Color.White
        Me.btn_tb_PN_POSITION_T.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_PN_POSITION_T.Location = New System.Drawing.Point(718, 240)
        Me.btn_tb_PN_POSITION_T.Name = "btn_tb_PN_POSITION_T"
        Me.btn_tb_PN_POSITION_T.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_PN_POSITION_T.TabIndex = 93
        Me.btn_tb_PN_POSITION_T.Tag = ""
        Me.btn_tb_PN_POSITION_T.Text = "Start"
        Me.btn_tb_PN_POSITION_T.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_PN_POSITION_T.UseVisualStyleBackColor = False
        '
        'btn_tb_PN_POSITION_MGR_R
        '
        Me.btn_tb_PN_POSITION_MGR_R.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_PN_POSITION_MGR_R.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_PN_POSITION_MGR_R.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_PN_POSITION_MGR_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_PN_POSITION_MGR_R.ForeColor = System.Drawing.Color.White
        Me.btn_tb_PN_POSITION_MGR_R.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_PN_POSITION_MGR_R.Location = New System.Drawing.Point(718, 209)
        Me.btn_tb_PN_POSITION_MGR_R.Name = "btn_tb_PN_POSITION_MGR_R"
        Me.btn_tb_PN_POSITION_MGR_R.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_PN_POSITION_MGR_R.TabIndex = 92
        Me.btn_tb_PN_POSITION_MGR_R.Tag = ""
        Me.btn_tb_PN_POSITION_MGR_R.Text = "Start"
        Me.btn_tb_PN_POSITION_MGR_R.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_PN_POSITION_MGR_R.UseVisualStyleBackColor = False
        '
        'btn_tb_PN_POSITION_FLD_R
        '
        Me.btn_tb_PN_POSITION_FLD_R.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_PN_POSITION_FLD_R.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_PN_POSITION_FLD_R.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_PN_POSITION_FLD_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_PN_POSITION_FLD_R.ForeColor = System.Drawing.Color.White
        Me.btn_tb_PN_POSITION_FLD_R.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_PN_POSITION_FLD_R.Location = New System.Drawing.Point(718, 178)
        Me.btn_tb_PN_POSITION_FLD_R.Name = "btn_tb_PN_POSITION_FLD_R"
        Me.btn_tb_PN_POSITION_FLD_R.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_PN_POSITION_FLD_R.TabIndex = 91
        Me.btn_tb_PN_POSITION_FLD_R.Tag = ""
        Me.btn_tb_PN_POSITION_FLD_R.Text = "Start"
        Me.btn_tb_PN_POSITION_FLD_R.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_PN_POSITION_FLD_R.UseVisualStyleBackColor = False
        '
        'btn_tb_PN_PERSONAL_VIEW
        '
        Me.btn_tb_PN_PERSONAL_VIEW.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_PN_PERSONAL_VIEW.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_PN_PERSONAL_VIEW.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_PN_PERSONAL_VIEW.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_PN_PERSONAL_VIEW.ForeColor = System.Drawing.Color.White
        Me.btn_tb_PN_PERSONAL_VIEW.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_PN_PERSONAL_VIEW.Location = New System.Drawing.Point(718, 147)
        Me.btn_tb_PN_PERSONAL_VIEW.Name = "btn_tb_PN_PERSONAL_VIEW"
        Me.btn_tb_PN_PERSONAL_VIEW.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_PN_PERSONAL_VIEW.TabIndex = 90
        Me.btn_tb_PN_PERSONAL_VIEW.Tag = ""
        Me.btn_tb_PN_PERSONAL_VIEW.Text = "Start"
        Me.btn_tb_PN_PERSONAL_VIEW.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_PN_PERSONAL_VIEW.UseVisualStyleBackColor = False
        '
        'btn_tb_PN_PERSONAL_STAT_R
        '
        Me.btn_tb_PN_PERSONAL_STAT_R.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_PN_PERSONAL_STAT_R.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_PN_PERSONAL_STAT_R.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_PN_PERSONAL_STAT_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_PN_PERSONAL_STAT_R.ForeColor = System.Drawing.Color.White
        Me.btn_tb_PN_PERSONAL_STAT_R.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_PN_PERSONAL_STAT_R.Location = New System.Drawing.Point(718, 116)
        Me.btn_tb_PN_PERSONAL_STAT_R.Name = "btn_tb_PN_PERSONAL_STAT_R"
        Me.btn_tb_PN_PERSONAL_STAT_R.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_PN_PERSONAL_STAT_R.TabIndex = 89
        Me.btn_tb_PN_PERSONAL_STAT_R.Tag = ""
        Me.btn_tb_PN_PERSONAL_STAT_R.Text = "Start"
        Me.btn_tb_PN_PERSONAL_STAT_R.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_PN_PERSONAL_STAT_R.UseVisualStyleBackColor = False
        '
        'btn_tb_PN_DEPARTMENT_R
        '
        Me.btn_tb_PN_DEPARTMENT_R.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_PN_DEPARTMENT_R.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_PN_DEPARTMENT_R.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_PN_DEPARTMENT_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_PN_DEPARTMENT_R.ForeColor = System.Drawing.Color.White
        Me.btn_tb_PN_DEPARTMENT_R.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_PN_DEPARTMENT_R.Location = New System.Drawing.Point(718, 86)
        Me.btn_tb_PN_DEPARTMENT_R.Name = "btn_tb_PN_DEPARTMENT_R"
        Me.btn_tb_PN_DEPARTMENT_R.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_PN_DEPARTMENT_R.TabIndex = 88
        Me.btn_tb_PN_DEPARTMENT_R.Tag = ""
        Me.btn_tb_PN_DEPARTMENT_R.Text = "Start"
        Me.btn_tb_PN_DEPARTMENT_R.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_PN_DEPARTMENT_R.UseVisualStyleBackColor = False
        '
        'Label31
        '
        Me.Label31.BackColor = System.Drawing.Color.CornflowerBlue
        Me.Label31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label31.ForeColor = System.Drawing.Color.White
        Me.Label31.Location = New System.Drawing.Point(717, 54)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(91, 32)
        Me.Label31.TabIndex = 87
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLastSync_tb_PN_POSITION_T
        '
        Me.lblLastSync_tb_PN_POSITION_T.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_PN_POSITION_T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_PN_POSITION_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_PN_POSITION_T.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_PN_POSITION_T.Location = New System.Drawing.Point(515, 240)
        Me.lblLastSync_tb_PN_POSITION_T.Name = "lblLastSync_tb_PN_POSITION_T"
        Me.lblLastSync_tb_PN_POSITION_T.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_PN_POSITION_T.TabIndex = 86
        Me.lblLastSync_tb_PN_POSITION_T.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLastSync_tb_PN_POSITION_MGR_R
        '
        Me.lblLastSync_tb_PN_POSITION_MGR_R.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_PN_POSITION_MGR_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_PN_POSITION_MGR_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_PN_POSITION_MGR_R.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_PN_POSITION_MGR_R.Location = New System.Drawing.Point(515, 209)
        Me.lblLastSync_tb_PN_POSITION_MGR_R.Name = "lblLastSync_tb_PN_POSITION_MGR_R"
        Me.lblLastSync_tb_PN_POSITION_MGR_R.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_PN_POSITION_MGR_R.TabIndex = 85
        Me.lblLastSync_tb_PN_POSITION_MGR_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLastSync_tb_PN_POSITION_FLD_R
        '
        Me.lblLastSync_tb_PN_POSITION_FLD_R.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_PN_POSITION_FLD_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_PN_POSITION_FLD_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_PN_POSITION_FLD_R.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_PN_POSITION_FLD_R.Location = New System.Drawing.Point(515, 178)
        Me.lblLastSync_tb_PN_POSITION_FLD_R.Name = "lblLastSync_tb_PN_POSITION_FLD_R"
        Me.lblLastSync_tb_PN_POSITION_FLD_R.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_PN_POSITION_FLD_R.TabIndex = 84
        Me.lblLastSync_tb_PN_POSITION_FLD_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLastSync_tb_PN_PERSONAL_VIEW
        '
        Me.lblLastSync_tb_PN_PERSONAL_VIEW.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_PN_PERSONAL_VIEW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_PN_PERSONAL_VIEW.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_PN_PERSONAL_VIEW.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_PN_PERSONAL_VIEW.Location = New System.Drawing.Point(515, 147)
        Me.lblLastSync_tb_PN_PERSONAL_VIEW.Name = "lblLastSync_tb_PN_PERSONAL_VIEW"
        Me.lblLastSync_tb_PN_PERSONAL_VIEW.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_PN_PERSONAL_VIEW.TabIndex = 83
        Me.lblLastSync_tb_PN_PERSONAL_VIEW.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLastSync_tb_PN_PERSONAL_STAT_R
        '
        Me.lblLastSync_tb_PN_PERSONAL_STAT_R.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_PN_PERSONAL_STAT_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_PN_PERSONAL_STAT_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_PN_PERSONAL_STAT_R.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_PN_PERSONAL_STAT_R.Location = New System.Drawing.Point(515, 116)
        Me.lblLastSync_tb_PN_PERSONAL_STAT_R.Name = "lblLastSync_tb_PN_PERSONAL_STAT_R"
        Me.lblLastSync_tb_PN_PERSONAL_STAT_R.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_PN_PERSONAL_STAT_R.TabIndex = 82
        Me.lblLastSync_tb_PN_PERSONAL_STAT_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLastSync_tb_PN_DEPARTMENT_R
        '
        Me.lblLastSync_tb_PN_DEPARTMENT_R.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_PN_DEPARTMENT_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_PN_DEPARTMENT_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_PN_DEPARTMENT_R.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_PN_DEPARTMENT_R.Location = New System.Drawing.Point(515, 85)
        Me.lblLastSync_tb_PN_DEPARTMENT_R.Name = "lblLastSync_tb_PN_DEPARTMENT_R"
        Me.lblLastSync_tb_PN_DEPARTMENT_R.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_PN_DEPARTMENT_R.TabIndex = 81
        Me.lblLastSync_tb_PN_DEPARTMENT_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_PN_POSITION_T
        '
        Me.lblRec_tb_PN_POSITION_T.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_PN_POSITION_T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_PN_POSITION_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_PN_POSITION_T.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_PN_POSITION_T.Location = New System.Drawing.Point(421, 240)
        Me.lblRec_tb_PN_POSITION_T.Name = "lblRec_tb_PN_POSITION_T"
        Me.lblRec_tb_PN_POSITION_T.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_PN_POSITION_T.TabIndex = 80
        Me.lblRec_tb_PN_POSITION_T.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_PN_POSITION_MGR_R
        '
        Me.lblRec_tb_PN_POSITION_MGR_R.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_PN_POSITION_MGR_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_PN_POSITION_MGR_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_PN_POSITION_MGR_R.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_PN_POSITION_MGR_R.Location = New System.Drawing.Point(421, 209)
        Me.lblRec_tb_PN_POSITION_MGR_R.Name = "lblRec_tb_PN_POSITION_MGR_R"
        Me.lblRec_tb_PN_POSITION_MGR_R.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_PN_POSITION_MGR_R.TabIndex = 79
        Me.lblRec_tb_PN_POSITION_MGR_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_PN_POSITION_FLD_R
        '
        Me.lblRec_tb_PN_POSITION_FLD_R.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_PN_POSITION_FLD_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_PN_POSITION_FLD_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_PN_POSITION_FLD_R.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_PN_POSITION_FLD_R.Location = New System.Drawing.Point(421, 178)
        Me.lblRec_tb_PN_POSITION_FLD_R.Name = "lblRec_tb_PN_POSITION_FLD_R"
        Me.lblRec_tb_PN_POSITION_FLD_R.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_PN_POSITION_FLD_R.TabIndex = 78
        Me.lblRec_tb_PN_POSITION_FLD_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_PN_PERSONAL_VIEW
        '
        Me.lblRec_tb_PN_PERSONAL_VIEW.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_PN_PERSONAL_VIEW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_PN_PERSONAL_VIEW.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_PN_PERSONAL_VIEW.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_PN_PERSONAL_VIEW.Location = New System.Drawing.Point(421, 147)
        Me.lblRec_tb_PN_PERSONAL_VIEW.Name = "lblRec_tb_PN_PERSONAL_VIEW"
        Me.lblRec_tb_PN_PERSONAL_VIEW.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_PN_PERSONAL_VIEW.TabIndex = 77
        Me.lblRec_tb_PN_PERSONAL_VIEW.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_PN_PERSONAL_STAT_R
        '
        Me.lblRec_tb_PN_PERSONAL_STAT_R.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_PN_PERSONAL_STAT_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_PN_PERSONAL_STAT_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_PN_PERSONAL_STAT_R.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_PN_PERSONAL_STAT_R.Location = New System.Drawing.Point(421, 116)
        Me.lblRec_tb_PN_PERSONAL_STAT_R.Name = "lblRec_tb_PN_PERSONAL_STAT_R"
        Me.lblRec_tb_PN_PERSONAL_STAT_R.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_PN_PERSONAL_STAT_R.TabIndex = 76
        Me.lblRec_tb_PN_PERSONAL_STAT_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_PN_DEPARTMENT_R
        '
        Me.lblRec_tb_PN_DEPARTMENT_R.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_PN_DEPARTMENT_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_PN_DEPARTMENT_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_PN_DEPARTMENT_R.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_PN_DEPARTMENT_R.Location = New System.Drawing.Point(421, 85)
        Me.lblRec_tb_PN_DEPARTMENT_R.Name = "lblRec_tb_PN_DEPARTMENT_R"
        Me.lblRec_tb_PN_DEPARTMENT_R.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_PN_DEPARTMENT_R.TabIndex = 75
        Me.lblRec_tb_PN_DEPARTMENT_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_PN_POSITION_T
        '
        Me.lblStatus_tb_PN_POSITION_T.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_PN_POSITION_T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_PN_POSITION_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_PN_POSITION_T.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_PN_POSITION_T.Location = New System.Drawing.Point(242, 240)
        Me.lblStatus_tb_PN_POSITION_T.Name = "lblStatus_tb_PN_POSITION_T"
        Me.lblStatus_tb_PN_POSITION_T.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_PN_POSITION_T.TabIndex = 74
        Me.lblStatus_tb_PN_POSITION_T.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_PN_POSITION_MGR_R
        '
        Me.lblStatus_tb_PN_POSITION_MGR_R.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_PN_POSITION_MGR_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_PN_POSITION_MGR_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_PN_POSITION_MGR_R.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_PN_POSITION_MGR_R.Location = New System.Drawing.Point(242, 209)
        Me.lblStatus_tb_PN_POSITION_MGR_R.Name = "lblStatus_tb_PN_POSITION_MGR_R"
        Me.lblStatus_tb_PN_POSITION_MGR_R.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_PN_POSITION_MGR_R.TabIndex = 73
        Me.lblStatus_tb_PN_POSITION_MGR_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_PN_POSITION_FLD_R
        '
        Me.lblStatus_tb_PN_POSITION_FLD_R.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_PN_POSITION_FLD_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_PN_POSITION_FLD_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_PN_POSITION_FLD_R.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_PN_POSITION_FLD_R.Location = New System.Drawing.Point(242, 178)
        Me.lblStatus_tb_PN_POSITION_FLD_R.Name = "lblStatus_tb_PN_POSITION_FLD_R"
        Me.lblStatus_tb_PN_POSITION_FLD_R.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_PN_POSITION_FLD_R.TabIndex = 72
        Me.lblStatus_tb_PN_POSITION_FLD_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_PN_PERSONAL_VIEW
        '
        Me.lblStatus_tb_PN_PERSONAL_VIEW.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_PN_PERSONAL_VIEW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_PN_PERSONAL_VIEW.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_PN_PERSONAL_VIEW.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_PN_PERSONAL_VIEW.Location = New System.Drawing.Point(242, 147)
        Me.lblStatus_tb_PN_PERSONAL_VIEW.Name = "lblStatus_tb_PN_PERSONAL_VIEW"
        Me.lblStatus_tb_PN_PERSONAL_VIEW.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_PN_PERSONAL_VIEW.TabIndex = 71
        Me.lblStatus_tb_PN_PERSONAL_VIEW.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_PN_PERSONAL_STAT_R
        '
        Me.lblStatus_tb_PN_PERSONAL_STAT_R.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_PN_PERSONAL_STAT_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_PN_PERSONAL_STAT_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_PN_PERSONAL_STAT_R.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_PN_PERSONAL_STAT_R.Location = New System.Drawing.Point(242, 116)
        Me.lblStatus_tb_PN_PERSONAL_STAT_R.Name = "lblStatus_tb_PN_PERSONAL_STAT_R"
        Me.lblStatus_tb_PN_PERSONAL_STAT_R.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_PN_PERSONAL_STAT_R.TabIndex = 70
        Me.lblStatus_tb_PN_PERSONAL_STAT_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_PN_DEPARTMENT_R
        '
        Me.lblStatus_tb_PN_DEPARTMENT_R.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_PN_DEPARTMENT_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_PN_DEPARTMENT_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_PN_DEPARTMENT_R.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_PN_DEPARTMENT_R.Location = New System.Drawing.Point(242, 85)
        Me.lblStatus_tb_PN_DEPARTMENT_R.Name = "lblStatus_tb_PN_DEPARTMENT_R"
        Me.lblStatus_tb_PN_DEPARTMENT_R.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_PN_DEPARTMENT_R.TabIndex = 69
        Me.lblStatus_tb_PN_DEPARTMENT_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.White
        Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(20, 240)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(223, 32)
        Me.Label12.TabIndex = 68
        Me.Label12.Text = "tb_PN_POSITION_T"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.White
        Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(20, 209)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(223, 32)
        Me.Label11.TabIndex = 67
        Me.Label11.Text = "tb_PN_POSITION_MGR_R"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.White
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(20, 178)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(223, 32)
        Me.Label10.TabIndex = 66
        Me.Label10.Text = "tb_PN_POSITION_FLD_R"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.White
        Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(20, 147)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(223, 32)
        Me.Label9.TabIndex = 65
        Me.Label9.Text = "tb_PN_PERSONAL_VIEW"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.White
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(20, 116)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(223, 32)
        Me.Label8.TabIndex = 64
        Me.Label8.Text = "tb_PN_PERSONAL_STAT_R"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.White
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(20, 85)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(223, 32)
        Me.Label7.TabIndex = 63
        Me.Label7.Text = "tb_PN_DEPARTMENT_R"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.CornflowerBlue
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(515, 54)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(203, 32)
        Me.Label6.TabIndex = 62
        Me.Label6.Text = "Last Sync"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.CornflowerBlue
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(421, 54)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(95, 32)
        Me.Label5.TabIndex = 61
        Me.Label5.Text = "Record(s)"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.CornflowerBlue
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(242, 54)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(180, 32)
        Me.Label4.TabIndex = 60
        Me.Label4.Text = "Status"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.CornflowerBlue
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(20, 54)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(223, 32)
        Me.Label3.TabIndex = 59
        Me.Label3.Text = "Table"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_tb_PN_LEAVE_T
        '
        Me.btn_tb_PN_LEAVE_T.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_PN_LEAVE_T.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_PN_LEAVE_T.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_PN_LEAVE_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_PN_LEAVE_T.ForeColor = System.Drawing.Color.White
        Me.btn_tb_PN_LEAVE_T.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_PN_LEAVE_T.Location = New System.Drawing.Point(718, 271)
        Me.btn_tb_PN_LEAVE_T.Name = "btn_tb_PN_LEAVE_T"
        Me.btn_tb_PN_LEAVE_T.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_PN_LEAVE_T.TabIndex = 94
        Me.btn_tb_PN_LEAVE_T.Tag = ""
        Me.btn_tb_PN_LEAVE_T.Text = "Start"
        Me.btn_tb_PN_LEAVE_T.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_PN_LEAVE_T.UseVisualStyleBackColor = False
        '
        'btn_tb_PK_SUBJ_G
        '
        Me.btn_tb_PK_SUBJ_G.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_PK_SUBJ_G.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_PK_SUBJ_G.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_PK_SUBJ_G.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_PK_SUBJ_G.ForeColor = System.Drawing.Color.White
        Me.btn_tb_PK_SUBJ_G.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_PK_SUBJ_G.Location = New System.Drawing.Point(718, 580)
        Me.btn_tb_PK_SUBJ_G.Name = "btn_tb_PK_SUBJ_G"
        Me.btn_tb_PK_SUBJ_G.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_PK_SUBJ_G.TabIndex = 148
        Me.btn_tb_PK_SUBJ_G.Tag = ""
        Me.btn_tb_PK_SUBJ_G.Text = "Start"
        Me.btn_tb_PK_SUBJ_G.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_PK_SUBJ_G.UseVisualStyleBackColor = False
        '
        'lblLastSync_tb_PK_SUBJ_G
        '
        Me.lblLastSync_tb_PK_SUBJ_G.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_PK_SUBJ_G.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_PK_SUBJ_G.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_PK_SUBJ_G.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_PK_SUBJ_G.Location = New System.Drawing.Point(515, 580)
        Me.lblLastSync_tb_PK_SUBJ_G.Name = "lblLastSync_tb_PK_SUBJ_G"
        Me.lblLastSync_tb_PK_SUBJ_G.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_PK_SUBJ_G.TabIndex = 147
        Me.lblLastSync_tb_PK_SUBJ_G.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_PK_SUBJ_G
        '
        Me.lblRec_tb_PK_SUBJ_G.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_PK_SUBJ_G.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_PK_SUBJ_G.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_PK_SUBJ_G.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_PK_SUBJ_G.Location = New System.Drawing.Point(421, 580)
        Me.lblRec_tb_PK_SUBJ_G.Name = "lblRec_tb_PK_SUBJ_G"
        Me.lblRec_tb_PK_SUBJ_G.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_PK_SUBJ_G.TabIndex = 146
        Me.lblRec_tb_PK_SUBJ_G.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_PK_SUBJ_G
        '
        Me.lblStatus_tb_PK_SUBJ_G.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_PK_SUBJ_G.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_PK_SUBJ_G.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_PK_SUBJ_G.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_PK_SUBJ_G.Location = New System.Drawing.Point(242, 580)
        Me.lblStatus_tb_PK_SUBJ_G.Name = "lblStatus_tb_PK_SUBJ_G"
        Me.lblStatus_tb_PK_SUBJ_G.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_PK_SUBJ_G.TabIndex = 145
        Me.lblStatus_tb_PK_SUBJ_G.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.Color.White
        Me.Label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Black
        Me.Label20.Location = New System.Drawing.Point(20, 580)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(223, 32)
        Me.Label20.TabIndex = 144
        Me.Label20.Text = "tb_PK_SUBJ_G"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btn_tb_PN_POSITION_RETURN2CLASS
        '
        Me.btn_tb_PN_POSITION_RETURN2CLASS.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_PN_POSITION_RETURN2CLASS.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_PN_POSITION_RETURN2CLASS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_PN_POSITION_RETURN2CLASS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_PN_POSITION_RETURN2CLASS.ForeColor = System.Drawing.Color.White
        Me.btn_tb_PN_POSITION_RETURN2CLASS.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_PN_POSITION_RETURN2CLASS.Location = New System.Drawing.Point(718, 550)
        Me.btn_tb_PN_POSITION_RETURN2CLASS.Name = "btn_tb_PN_POSITION_RETURN2CLASS"
        Me.btn_tb_PN_POSITION_RETURN2CLASS.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_PN_POSITION_RETURN2CLASS.TabIndex = 143
        Me.btn_tb_PN_POSITION_RETURN2CLASS.Tag = ""
        Me.btn_tb_PN_POSITION_RETURN2CLASS.Text = "Start"
        Me.btn_tb_PN_POSITION_RETURN2CLASS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_PN_POSITION_RETURN2CLASS.UseVisualStyleBackColor = False
        '
        'lblLastSync_tb_PN_POSITION_RETURN2CLASS
        '
        Me.lblLastSync_tb_PN_POSITION_RETURN2CLASS.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_PN_POSITION_RETURN2CLASS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_PN_POSITION_RETURN2CLASS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_PN_POSITION_RETURN2CLASS.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_PN_POSITION_RETURN2CLASS.Location = New System.Drawing.Point(515, 550)
        Me.lblLastSync_tb_PN_POSITION_RETURN2CLASS.Name = "lblLastSync_tb_PN_POSITION_RETURN2CLASS"
        Me.lblLastSync_tb_PN_POSITION_RETURN2CLASS.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_PN_POSITION_RETURN2CLASS.TabIndex = 142
        Me.lblLastSync_tb_PN_POSITION_RETURN2CLASS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_PN_POSITION_RETURN2CLASS
        '
        Me.lblRec_tb_PN_POSITION_RETURN2CLASS.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_PN_POSITION_RETURN2CLASS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_PN_POSITION_RETURN2CLASS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_PN_POSITION_RETURN2CLASS.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_PN_POSITION_RETURN2CLASS.Location = New System.Drawing.Point(421, 550)
        Me.lblRec_tb_PN_POSITION_RETURN2CLASS.Name = "lblRec_tb_PN_POSITION_RETURN2CLASS"
        Me.lblRec_tb_PN_POSITION_RETURN2CLASS.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_PN_POSITION_RETURN2CLASS.TabIndex = 141
        Me.lblRec_tb_PN_POSITION_RETURN2CLASS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_PN_POSITION_RETURN2CLASS
        '
        Me.lblStatus_tb_PN_POSITION_RETURN2CLASS.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_PN_POSITION_RETURN2CLASS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_PN_POSITION_RETURN2CLASS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_PN_POSITION_RETURN2CLASS.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_PN_POSITION_RETURN2CLASS.Location = New System.Drawing.Point(242, 550)
        Me.lblStatus_tb_PN_POSITION_RETURN2CLASS.Name = "lblStatus_tb_PN_POSITION_RETURN2CLASS"
        Me.lblStatus_tb_PN_POSITION_RETURN2CLASS.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_PN_POSITION_RETURN2CLASS.TabIndex = 140
        Me.lblStatus_tb_PN_POSITION_RETURN2CLASS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLastSync_tb_PN_POSITION_HISTORY_T
        '
        Me.lblLastSync_tb_PN_POSITION_HISTORY_T.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_PN_POSITION_HISTORY_T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_PN_POSITION_HISTORY_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_PN_POSITION_HISTORY_T.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_PN_POSITION_HISTORY_T.Location = New System.Drawing.Point(515, 519)
        Me.lblLastSync_tb_PN_POSITION_HISTORY_T.Name = "lblLastSync_tb_PN_POSITION_HISTORY_T"
        Me.lblLastSync_tb_PN_POSITION_HISTORY_T.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_PN_POSITION_HISTORY_T.TabIndex = 139
        Me.lblLastSync_tb_PN_POSITION_HISTORY_T.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_PN_POSITION_HISTORY_T
        '
        Me.lblRec_tb_PN_POSITION_HISTORY_T.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_PN_POSITION_HISTORY_T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_PN_POSITION_HISTORY_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_PN_POSITION_HISTORY_T.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_PN_POSITION_HISTORY_T.Location = New System.Drawing.Point(421, 519)
        Me.lblRec_tb_PN_POSITION_HISTORY_T.Name = "lblRec_tb_PN_POSITION_HISTORY_T"
        Me.lblRec_tb_PN_POSITION_HISTORY_T.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_PN_POSITION_HISTORY_T.TabIndex = 138
        Me.lblRec_tb_PN_POSITION_HISTORY_T.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_PN_POSITION_HISTORY_T
        '
        Me.lblStatus_tb_PN_POSITION_HISTORY_T.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_PN_POSITION_HISTORY_T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_PN_POSITION_HISTORY_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_PN_POSITION_HISTORY_T.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_PN_POSITION_HISTORY_T.Location = New System.Drawing.Point(242, 519)
        Me.lblStatus_tb_PN_POSITION_HISTORY_T.Name = "lblStatus_tb_PN_POSITION_HISTORY_T"
        Me.lblStatus_tb_PN_POSITION_HISTORY_T.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_PN_POSITION_HISTORY_T.TabIndex = 137
        Me.lblStatus_tb_PN_POSITION_HISTORY_T.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_tb_PN_POSITION_HISTORY_T
        '
        Me.btn_tb_PN_POSITION_HISTORY_T.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_PN_POSITION_HISTORY_T.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_PN_POSITION_HISTORY_T.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_PN_POSITION_HISTORY_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_PN_POSITION_HISTORY_T.ForeColor = System.Drawing.Color.White
        Me.btn_tb_PN_POSITION_HISTORY_T.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_PN_POSITION_HISTORY_T.Location = New System.Drawing.Point(718, 519)
        Me.btn_tb_PN_POSITION_HISTORY_T.Name = "btn_tb_PN_POSITION_HISTORY_T"
        Me.btn_tb_PN_POSITION_HISTORY_T.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_PN_POSITION_HISTORY_T.TabIndex = 136
        Me.btn_tb_PN_POSITION_HISTORY_T.Tag = ""
        Me.btn_tb_PN_POSITION_HISTORY_T.Text = "Start"
        Me.btn_tb_PN_POSITION_HISTORY_T.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_PN_POSITION_HISTORY_T.UseVisualStyleBackColor = False
        '
        'Label27
        '
        Me.Label27.BackColor = System.Drawing.Color.White
        Me.Label27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.Black
        Me.Label27.Location = New System.Drawing.Point(20, 550)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(223, 32)
        Me.Label27.TabIndex = 135
        Me.Label27.Text = "tb_PN_POSITION_RETURN2CLASS"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label28
        '
        Me.Label28.BackColor = System.Drawing.Color.White
        Me.Label28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.Black
        Me.Label28.Location = New System.Drawing.Point(20, 519)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(223, 32)
        Me.Label28.TabIndex = 134
        Me.Label28.Text = "tb_PN_POSITION_HISTORY_T"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btn_tb_CP_SUBJECT_PERSONAL
        '
        Me.btn_tb_CP_SUBJECT_PERSONAL.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_CP_SUBJECT_PERSONAL.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_CP_SUBJECT_PERSONAL.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_CP_SUBJECT_PERSONAL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_CP_SUBJECT_PERSONAL.ForeColor = System.Drawing.Color.White
        Me.btn_tb_CP_SUBJECT_PERSONAL.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_CP_SUBJECT_PERSONAL.Location = New System.Drawing.Point(718, 488)
        Me.btn_tb_CP_SUBJECT_PERSONAL.Name = "btn_tb_CP_SUBJECT_PERSONAL"
        Me.btn_tb_CP_SUBJECT_PERSONAL.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_CP_SUBJECT_PERSONAL.TabIndex = 133
        Me.btn_tb_CP_SUBJECT_PERSONAL.Tag = ""
        Me.btn_tb_CP_SUBJECT_PERSONAL.Text = "Start"
        Me.btn_tb_CP_SUBJECT_PERSONAL.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_CP_SUBJECT_PERSONAL.UseVisualStyleBackColor = False
        '
        'lblLastSync_tb_CP_SUBJECT_PERSONAL
        '
        Me.lblLastSync_tb_CP_SUBJECT_PERSONAL.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_CP_SUBJECT_PERSONAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_CP_SUBJECT_PERSONAL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_CP_SUBJECT_PERSONAL.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_CP_SUBJECT_PERSONAL.Location = New System.Drawing.Point(515, 488)
        Me.lblLastSync_tb_CP_SUBJECT_PERSONAL.Name = "lblLastSync_tb_CP_SUBJECT_PERSONAL"
        Me.lblLastSync_tb_CP_SUBJECT_PERSONAL.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_CP_SUBJECT_PERSONAL.TabIndex = 132
        Me.lblLastSync_tb_CP_SUBJECT_PERSONAL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_CP_SUBJECT_PERSONAL
        '
        Me.lblRec_tb_CP_SUBJECT_PERSONAL.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_CP_SUBJECT_PERSONAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_CP_SUBJECT_PERSONAL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_CP_SUBJECT_PERSONAL.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_CP_SUBJECT_PERSONAL.Location = New System.Drawing.Point(421, 488)
        Me.lblRec_tb_CP_SUBJECT_PERSONAL.Name = "lblRec_tb_CP_SUBJECT_PERSONAL"
        Me.lblRec_tb_CP_SUBJECT_PERSONAL.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_CP_SUBJECT_PERSONAL.TabIndex = 131
        Me.lblRec_tb_CP_SUBJECT_PERSONAL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_CP_SUBJECT_PERSONAL
        '
        Me.lblStatus_tb_CP_SUBJECT_PERSONAL.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_CP_SUBJECT_PERSONAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_CP_SUBJECT_PERSONAL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_CP_SUBJECT_PERSONAL.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_CP_SUBJECT_PERSONAL.Location = New System.Drawing.Point(242, 488)
        Me.lblStatus_tb_CP_SUBJECT_PERSONAL.Name = "lblStatus_tb_CP_SUBJECT_PERSONAL"
        Me.lblStatus_tb_CP_SUBJECT_PERSONAL.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_CP_SUBJECT_PERSONAL.TabIndex = 130
        Me.lblStatus_tb_CP_SUBJECT_PERSONAL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLastSync_tb_CP_SUBJECT_MASTER
        '
        Me.lblLastSync_tb_CP_SUBJECT_MASTER.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_CP_SUBJECT_MASTER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_CP_SUBJECT_MASTER.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_CP_SUBJECT_MASTER.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_CP_SUBJECT_MASTER.Location = New System.Drawing.Point(515, 457)
        Me.lblLastSync_tb_CP_SUBJECT_MASTER.Name = "lblLastSync_tb_CP_SUBJECT_MASTER"
        Me.lblLastSync_tb_CP_SUBJECT_MASTER.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_CP_SUBJECT_MASTER.TabIndex = 129
        Me.lblLastSync_tb_CP_SUBJECT_MASTER.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_CP_SUBJECT_MASTER
        '
        Me.lblRec_tb_CP_SUBJECT_MASTER.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_CP_SUBJECT_MASTER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_CP_SUBJECT_MASTER.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_CP_SUBJECT_MASTER.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_CP_SUBJECT_MASTER.Location = New System.Drawing.Point(421, 457)
        Me.lblRec_tb_CP_SUBJECT_MASTER.Name = "lblRec_tb_CP_SUBJECT_MASTER"
        Me.lblRec_tb_CP_SUBJECT_MASTER.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_CP_SUBJECT_MASTER.TabIndex = 128
        Me.lblRec_tb_CP_SUBJECT_MASTER.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_CP_SUBJECT_MASTER
        '
        Me.lblStatus_tb_CP_SUBJECT_MASTER.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_CP_SUBJECT_MASTER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_CP_SUBJECT_MASTER.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_CP_SUBJECT_MASTER.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_CP_SUBJECT_MASTER.Location = New System.Drawing.Point(242, 457)
        Me.lblStatus_tb_CP_SUBJECT_MASTER.Name = "lblStatus_tb_CP_SUBJECT_MASTER"
        Me.lblStatus_tb_CP_SUBJECT_MASTER.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_CP_SUBJECT_MASTER.TabIndex = 127
        Me.lblStatus_tb_CP_SUBJECT_MASTER.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_tb_CP_SUBJECT_MASTER
        '
        Me.btn_tb_CP_SUBJECT_MASTER.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_CP_SUBJECT_MASTER.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_CP_SUBJECT_MASTER.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_CP_SUBJECT_MASTER.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_CP_SUBJECT_MASTER.ForeColor = System.Drawing.Color.White
        Me.btn_tb_CP_SUBJECT_MASTER.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_CP_SUBJECT_MASTER.Location = New System.Drawing.Point(718, 457)
        Me.btn_tb_CP_SUBJECT_MASTER.Name = "btn_tb_CP_SUBJECT_MASTER"
        Me.btn_tb_CP_SUBJECT_MASTER.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_CP_SUBJECT_MASTER.TabIndex = 126
        Me.btn_tb_CP_SUBJECT_MASTER.Tag = ""
        Me.btn_tb_CP_SUBJECT_MASTER.Text = "Start"
        Me.btn_tb_CP_SUBJECT_MASTER.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_CP_SUBJECT_MASTER.UseVisualStyleBackColor = False
        '
        'Label25
        '
        Me.Label25.BackColor = System.Drawing.Color.White
        Me.Label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.Black
        Me.Label25.Location = New System.Drawing.Point(20, 488)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(223, 32)
        Me.Label25.TabIndex = 125
        Me.Label25.Text = "tb_CP_SUBJECT_PERSONAL"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label26
        '
        Me.Label26.BackColor = System.Drawing.Color.White
        Me.Label26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.Black
        Me.Label26.Location = New System.Drawing.Point(20, 457)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(223, 32)
        Me.Label26.TabIndex = 124
        Me.Label26.Text = "tb_CP_SUBJECT_MASTER"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btn_tb_PN_PUNISH_CODE_R
        '
        Me.btn_tb_PN_PUNISH_CODE_R.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_PN_PUNISH_CODE_R.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_PN_PUNISH_CODE_R.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_PN_PUNISH_CODE_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_PN_PUNISH_CODE_R.ForeColor = System.Drawing.Color.White
        Me.btn_tb_PN_PUNISH_CODE_R.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_PN_PUNISH_CODE_R.Location = New System.Drawing.Point(718, 426)
        Me.btn_tb_PN_PUNISH_CODE_R.Name = "btn_tb_PN_PUNISH_CODE_R"
        Me.btn_tb_PN_PUNISH_CODE_R.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_PN_PUNISH_CODE_R.TabIndex = 123
        Me.btn_tb_PN_PUNISH_CODE_R.Tag = ""
        Me.btn_tb_PN_PUNISH_CODE_R.Text = "Start"
        Me.btn_tb_PN_PUNISH_CODE_R.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_PN_PUNISH_CODE_R.UseVisualStyleBackColor = False
        '
        'lblLastSync_tb_PN_PUNISH_CODE_R
        '
        Me.lblLastSync_tb_PN_PUNISH_CODE_R.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_PN_PUNISH_CODE_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_PN_PUNISH_CODE_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_PN_PUNISH_CODE_R.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_PN_PUNISH_CODE_R.Location = New System.Drawing.Point(515, 426)
        Me.lblLastSync_tb_PN_PUNISH_CODE_R.Name = "lblLastSync_tb_PN_PUNISH_CODE_R"
        Me.lblLastSync_tb_PN_PUNISH_CODE_R.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_PN_PUNISH_CODE_R.TabIndex = 122
        Me.lblLastSync_tb_PN_PUNISH_CODE_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_PN_PUNISH_CODE_R
        '
        Me.lblRec_tb_PN_PUNISH_CODE_R.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_PN_PUNISH_CODE_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_PN_PUNISH_CODE_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_PN_PUNISH_CODE_R.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_PN_PUNISH_CODE_R.Location = New System.Drawing.Point(421, 426)
        Me.lblRec_tb_PN_PUNISH_CODE_R.Name = "lblRec_tb_PN_PUNISH_CODE_R"
        Me.lblRec_tb_PN_PUNISH_CODE_R.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_PN_PUNISH_CODE_R.TabIndex = 121
        Me.lblRec_tb_PN_PUNISH_CODE_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_PN_PUNISH_CODE_R
        '
        Me.lblStatus_tb_PN_PUNISH_CODE_R.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_PN_PUNISH_CODE_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_PN_PUNISH_CODE_R.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_PN_PUNISH_CODE_R.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_PN_PUNISH_CODE_R.Location = New System.Drawing.Point(242, 426)
        Me.lblStatus_tb_PN_PUNISH_CODE_R.Name = "lblStatus_tb_PN_PUNISH_CODE_R"
        Me.lblStatus_tb_PN_PUNISH_CODE_R.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_PN_PUNISH_CODE_R.TabIndex = 120
        Me.lblStatus_tb_PN_PUNISH_CODE_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLastSync_tb_PN_PUNISH_T
        '
        Me.lblLastSync_tb_PN_PUNISH_T.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_PN_PUNISH_T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_PN_PUNISH_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_PN_PUNISH_T.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_PN_PUNISH_T.Location = New System.Drawing.Point(515, 395)
        Me.lblLastSync_tb_PN_PUNISH_T.Name = "lblLastSync_tb_PN_PUNISH_T"
        Me.lblLastSync_tb_PN_PUNISH_T.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_PN_PUNISH_T.TabIndex = 119
        Me.lblLastSync_tb_PN_PUNISH_T.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_PN_PUNISH_T
        '
        Me.lblRec_tb_PN_PUNISH_T.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_PN_PUNISH_T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_PN_PUNISH_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_PN_PUNISH_T.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_PN_PUNISH_T.Location = New System.Drawing.Point(421, 395)
        Me.lblRec_tb_PN_PUNISH_T.Name = "lblRec_tb_PN_PUNISH_T"
        Me.lblRec_tb_PN_PUNISH_T.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_PN_PUNISH_T.TabIndex = 118
        Me.lblRec_tb_PN_PUNISH_T.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_PN_PUNISH_T
        '
        Me.lblStatus_tb_PN_PUNISH_T.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_PN_PUNISH_T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_PN_PUNISH_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_PN_PUNISH_T.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_PN_PUNISH_T.Location = New System.Drawing.Point(242, 395)
        Me.lblStatus_tb_PN_PUNISH_T.Name = "lblStatus_tb_PN_PUNISH_T"
        Me.lblStatus_tb_PN_PUNISH_T.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_PN_PUNISH_T.TabIndex = 117
        Me.lblStatus_tb_PN_PUNISH_T.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_tb_PN_PUNISH_T
        '
        Me.btn_tb_PN_PUNISH_T.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_PN_PUNISH_T.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_PN_PUNISH_T.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_PN_PUNISH_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_PN_PUNISH_T.ForeColor = System.Drawing.Color.White
        Me.btn_tb_PN_PUNISH_T.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_PN_PUNISH_T.Location = New System.Drawing.Point(718, 395)
        Me.btn_tb_PN_PUNISH_T.Name = "btn_tb_PN_PUNISH_T"
        Me.btn_tb_PN_PUNISH_T.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_PN_PUNISH_T.TabIndex = 116
        Me.btn_tb_PN_PUNISH_T.Tag = ""
        Me.btn_tb_PN_PUNISH_T.Text = "Start"
        Me.btn_tb_PN_PUNISH_T.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_PN_PUNISH_T.UseVisualStyleBackColor = False
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.Color.White
        Me.Label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.Black
        Me.Label23.Location = New System.Drawing.Point(20, 426)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(223, 32)
        Me.Label23.TabIndex = 115
        Me.Label23.Text = "tb_PN_PUNISH_CODE_R"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label24
        '
        Me.Label24.BackColor = System.Drawing.Color.White
        Me.Label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Black
        Me.Label24.Location = New System.Drawing.Point(20, 395)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(223, 32)
        Me.Label24.TabIndex = 114
        Me.Label24.Text = "tb_PN_PUNISH_T "
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btn_tb_PK_EMP_SELECT
        '
        Me.btn_tb_PK_EMP_SELECT.BackColor = System.Drawing.Color.SlateGray
        Me.btn_tb_PK_EMP_SELECT.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_tb_PK_EMP_SELECT.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tb_PK_EMP_SELECT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_tb_PK_EMP_SELECT.ForeColor = System.Drawing.Color.White
        Me.btn_tb_PK_EMP_SELECT.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btn_tb_PK_EMP_SELECT.Location = New System.Drawing.Point(718, 364)
        Me.btn_tb_PK_EMP_SELECT.Name = "btn_tb_PK_EMP_SELECT"
        Me.btn_tb_PK_EMP_SELECT.Size = New System.Drawing.Size(91, 32)
        Me.btn_tb_PK_EMP_SELECT.TabIndex = 113
        Me.btn_tb_PK_EMP_SELECT.Tag = ""
        Me.btn_tb_PK_EMP_SELECT.Text = "Start"
        Me.btn_tb_PK_EMP_SELECT.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_tb_PK_EMP_SELECT.UseVisualStyleBackColor = False
        '
        'lblLastSync_tb_PK_EMP_SELECT
        '
        Me.lblLastSync_tb_PK_EMP_SELECT.BackColor = System.Drawing.Color.White
        Me.lblLastSync_tb_PK_EMP_SELECT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLastSync_tb_PK_EMP_SELECT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastSync_tb_PK_EMP_SELECT.ForeColor = System.Drawing.Color.Black
        Me.lblLastSync_tb_PK_EMP_SELECT.Location = New System.Drawing.Point(515, 364)
        Me.lblLastSync_tb_PK_EMP_SELECT.Name = "lblLastSync_tb_PK_EMP_SELECT"
        Me.lblLastSync_tb_PK_EMP_SELECT.Size = New System.Drawing.Size(203, 32)
        Me.lblLastSync_tb_PK_EMP_SELECT.TabIndex = 112
        Me.lblLastSync_tb_PK_EMP_SELECT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRec_tb_PK_EMP_SELECT
        '
        Me.lblRec_tb_PK_EMP_SELECT.BackColor = System.Drawing.Color.White
        Me.lblRec_tb_PK_EMP_SELECT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRec_tb_PK_EMP_SELECT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec_tb_PK_EMP_SELECT.ForeColor = System.Drawing.Color.Black
        Me.lblRec_tb_PK_EMP_SELECT.Location = New System.Drawing.Point(421, 364)
        Me.lblRec_tb_PK_EMP_SELECT.Name = "lblRec_tb_PK_EMP_SELECT"
        Me.lblRec_tb_PK_EMP_SELECT.Size = New System.Drawing.Size(95, 32)
        Me.lblRec_tb_PK_EMP_SELECT.TabIndex = 111
        Me.lblRec_tb_PK_EMP_SELECT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus_tb_PK_EMP_SELECT
        '
        Me.lblStatus_tb_PK_EMP_SELECT.BackColor = System.Drawing.Color.White
        Me.lblStatus_tb_PK_EMP_SELECT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus_tb_PK_EMP_SELECT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus_tb_PK_EMP_SELECT.ForeColor = System.Drawing.Color.Black
        Me.lblStatus_tb_PK_EMP_SELECT.Location = New System.Drawing.Point(242, 364)
        Me.lblStatus_tb_PK_EMP_SELECT.Name = "lblStatus_tb_PK_EMP_SELECT"
        Me.lblStatus_tb_PK_EMP_SELECT.Size = New System.Drawing.Size(180, 32)
        Me.lblStatus_tb_PK_EMP_SELECT.TabIndex = 110
        Me.lblStatus_tb_PK_EMP_SELECT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.White
        Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(20, 364)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(223, 32)
        Me.Label15.TabIndex = 109
        Me.Label15.Text = "tb_PK_EMP_SELECT"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnStartAll
        '
        Me.btnStartAll.BackColor = System.Drawing.Color.Transparent
        Me.btnStartAll.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnStartAll.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnStartAll.FlatAppearance.BorderSize = 0
        Me.btnStartAll.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnStartAll.ForeColor = System.Drawing.Color.DimGray
        Me.btnStartAll.Image = Global.TTM_SyncData.My.Resources.Resources.Start
        Me.btnStartAll.Location = New System.Drawing.Point(346, 10)
        Me.btnStartAll.Name = "btnStartAll"
        Me.btnStartAll.Size = New System.Drawing.Size(134, 32)
        Me.btnStartAll.TabIndex = 149
        Me.btnStartAll.Tag = ""
        Me.btnStartAll.Text = "Start All"
        Me.btnStartAll.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnStartAll.UseVisualStyleBackColor = False
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(823, 670)
        Me.Controls.Add(Me.btnStartAll)
        Me.Controls.Add(Me.btn_tb_PK_SUBJ_G)
        Me.Controls.Add(Me.lblLastSync_tb_PK_SUBJ_G)
        Me.Controls.Add(Me.lblRec_tb_PK_SUBJ_G)
        Me.Controls.Add(Me.lblStatus_tb_PK_SUBJ_G)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.btn_tb_PN_POSITION_RETURN2CLASS)
        Me.Controls.Add(Me.lblLastSync_tb_PN_POSITION_RETURN2CLASS)
        Me.Controls.Add(Me.lblRec_tb_PN_POSITION_RETURN2CLASS)
        Me.Controls.Add(Me.lblStatus_tb_PN_POSITION_RETURN2CLASS)
        Me.Controls.Add(Me.lblLastSync_tb_PN_POSITION_HISTORY_T)
        Me.Controls.Add(Me.lblRec_tb_PN_POSITION_HISTORY_T)
        Me.Controls.Add(Me.lblStatus_tb_PN_POSITION_HISTORY_T)
        Me.Controls.Add(Me.btn_tb_PN_POSITION_HISTORY_T)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.btn_tb_CP_SUBJECT_PERSONAL)
        Me.Controls.Add(Me.lblLastSync_tb_CP_SUBJECT_PERSONAL)
        Me.Controls.Add(Me.lblRec_tb_CP_SUBJECT_PERSONAL)
        Me.Controls.Add(Me.lblStatus_tb_CP_SUBJECT_PERSONAL)
        Me.Controls.Add(Me.lblLastSync_tb_CP_SUBJECT_MASTER)
        Me.Controls.Add(Me.lblRec_tb_CP_SUBJECT_MASTER)
        Me.Controls.Add(Me.lblStatus_tb_CP_SUBJECT_MASTER)
        Me.Controls.Add(Me.btn_tb_CP_SUBJECT_MASTER)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.btn_tb_PN_PUNISH_CODE_R)
        Me.Controls.Add(Me.lblLastSync_tb_PN_PUNISH_CODE_R)
        Me.Controls.Add(Me.lblRec_tb_PN_PUNISH_CODE_R)
        Me.Controls.Add(Me.lblStatus_tb_PN_PUNISH_CODE_R)
        Me.Controls.Add(Me.lblLastSync_tb_PN_PUNISH_T)
        Me.Controls.Add(Me.lblRec_tb_PN_PUNISH_T)
        Me.Controls.Add(Me.lblStatus_tb_PN_PUNISH_T)
        Me.Controls.Add(Me.btn_tb_PN_PUNISH_T)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.btn_tb_PK_EMP_SELECT)
        Me.Controls.Add(Me.lblLastSync_tb_PK_EMP_SELECT)
        Me.Controls.Add(Me.lblRec_tb_PK_EMP_SELECT)
        Me.Controls.Add(Me.lblStatus_tb_PK_EMP_SELECT)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.lblLastSync_tb_PK_COURSE)
        Me.Controls.Add(Me.lblRec_tb_PK_COURSE)
        Me.Controls.Add(Me.lblStatus_tb_PK_COURSE)
        Me.Controls.Add(Me.btn_tb_PK_COURSE)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.btn_tb_PN_POSITION_FLD_FUNC)
        Me.Controls.Add(Me.lblLastSync_tb_PN_POSITION_FLD_FUNC)
        Me.Controls.Add(Me.lblRec_tb_PN_POSITION_FLD_FUNC)
        Me.Controls.Add(Me.lblStatus_tb_PN_POSITION_FLD_FUNC)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.lblLastSync_tb_PN_LEAVE_T)
        Me.Controls.Add(Me.lblRec_tb_PN_LEAVE_T)
        Me.Controls.Add(Me.lblStatus_tb_PN_LEAVE_T)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.btn_tb_PN_POSITION_T)
        Me.Controls.Add(Me.btn_tb_PN_POSITION_MGR_R)
        Me.Controls.Add(Me.btn_tb_PN_POSITION_FLD_R)
        Me.Controls.Add(Me.btn_tb_PN_PERSONAL_VIEW)
        Me.Controls.Add(Me.btn_tb_PN_PERSONAL_STAT_R)
        Me.Controls.Add(Me.btn_tb_PN_DEPARTMENT_R)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.lblLastSync_tb_PN_POSITION_T)
        Me.Controls.Add(Me.lblLastSync_tb_PN_POSITION_MGR_R)
        Me.Controls.Add(Me.lblLastSync_tb_PN_POSITION_FLD_R)
        Me.Controls.Add(Me.lblLastSync_tb_PN_PERSONAL_VIEW)
        Me.Controls.Add(Me.lblLastSync_tb_PN_PERSONAL_STAT_R)
        Me.Controls.Add(Me.lblLastSync_tb_PN_DEPARTMENT_R)
        Me.Controls.Add(Me.lblRec_tb_PN_POSITION_T)
        Me.Controls.Add(Me.lblRec_tb_PN_POSITION_MGR_R)
        Me.Controls.Add(Me.lblRec_tb_PN_POSITION_FLD_R)
        Me.Controls.Add(Me.lblRec_tb_PN_PERSONAL_VIEW)
        Me.Controls.Add(Me.lblRec_tb_PN_PERSONAL_STAT_R)
        Me.Controls.Add(Me.lblRec_tb_PN_DEPARTMENT_R)
        Me.Controls.Add(Me.lblStatus_tb_PN_POSITION_T)
        Me.Controls.Add(Me.lblStatus_tb_PN_POSITION_MGR_R)
        Me.Controls.Add(Me.lblStatus_tb_PN_POSITION_FLD_R)
        Me.Controls.Add(Me.lblStatus_tb_PN_PERSONAL_VIEW)
        Me.Controls.Add(Me.lblStatus_tb_PN_PERSONAL_STAT_R)
        Me.Controls.Add(Me.lblStatus_tb_PN_DEPARTMENT_R)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btn_tb_PN_LEAVE_T)
        Me.Controls.Add(Me.btnDbSetting)
        Me.Controls.Add(Me.txtAttention)
        Me.Controls.Add(Me.cbAuto)
        Me.Controls.Add(Me.txtMM)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtHH)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TTM SyncData"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtHH As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtMM As System.Windows.Forms.TextBox
    Friend WithEvents cbAuto As System.Windows.Forms.CheckBox
    Friend WithEvents TimerAutoSync As System.Windows.Forms.Timer
    Friend WithEvents txtAttention As System.Windows.Forms.TextBox
    Friend WithEvents btnDbSetting As System.Windows.Forms.Button
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents lblLastSync_tb_PK_COURSE As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_PK_COURSE As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_PK_COURSE As System.Windows.Forms.Label
    Friend WithEvents btn_tb_PK_COURSE As System.Windows.Forms.Button
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents btn_tb_PN_POSITION_FLD_FUNC As System.Windows.Forms.Button
    Friend WithEvents lblLastSync_tb_PN_POSITION_FLD_FUNC As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_PN_POSITION_FLD_FUNC As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_PN_POSITION_FLD_FUNC As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lblLastSync_tb_PN_LEAVE_T As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_PN_LEAVE_T As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_PN_LEAVE_T As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents btn_tb_PN_POSITION_T As System.Windows.Forms.Button
    Friend WithEvents btn_tb_PN_POSITION_MGR_R As System.Windows.Forms.Button
    Friend WithEvents btn_tb_PN_POSITION_FLD_R As System.Windows.Forms.Button
    Friend WithEvents btn_tb_PN_PERSONAL_VIEW As System.Windows.Forms.Button
    Friend WithEvents btn_tb_PN_PERSONAL_STAT_R As System.Windows.Forms.Button
    Friend WithEvents btn_tb_PN_DEPARTMENT_R As System.Windows.Forms.Button
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents lblLastSync_tb_PN_POSITION_T As System.Windows.Forms.Label
    Friend WithEvents lblLastSync_tb_PN_POSITION_MGR_R As System.Windows.Forms.Label
    Friend WithEvents lblLastSync_tb_PN_POSITION_FLD_R As System.Windows.Forms.Label
    Friend WithEvents lblLastSync_tb_PN_PERSONAL_VIEW As System.Windows.Forms.Label
    Friend WithEvents lblLastSync_tb_PN_PERSONAL_STAT_R As System.Windows.Forms.Label
    Friend WithEvents lblLastSync_tb_PN_DEPARTMENT_R As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_PN_POSITION_T As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_PN_POSITION_MGR_R As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_PN_POSITION_FLD_R As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_PN_PERSONAL_VIEW As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_PN_PERSONAL_STAT_R As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_PN_DEPARTMENT_R As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_PN_POSITION_T As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_PN_POSITION_MGR_R As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_PN_POSITION_FLD_R As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_PN_PERSONAL_VIEW As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_PN_PERSONAL_STAT_R As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_PN_DEPARTMENT_R As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btn_tb_PN_LEAVE_T As System.Windows.Forms.Button
    Friend WithEvents btn_tb_PK_SUBJ_G As System.Windows.Forms.Button
    Friend WithEvents lblLastSync_tb_PK_SUBJ_G As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_PK_SUBJ_G As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_PK_SUBJ_G As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents btn_tb_PN_POSITION_RETURN2CLASS As System.Windows.Forms.Button
    Friend WithEvents lblLastSync_tb_PN_POSITION_RETURN2CLASS As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_PN_POSITION_RETURN2CLASS As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_PN_POSITION_RETURN2CLASS As System.Windows.Forms.Label
    Friend WithEvents lblLastSync_tb_PN_POSITION_HISTORY_T As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_PN_POSITION_HISTORY_T As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_PN_POSITION_HISTORY_T As System.Windows.Forms.Label
    Friend WithEvents btn_tb_PN_POSITION_HISTORY_T As System.Windows.Forms.Button
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents btn_tb_CP_SUBJECT_PERSONAL As System.Windows.Forms.Button
    Friend WithEvents lblLastSync_tb_CP_SUBJECT_PERSONAL As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_CP_SUBJECT_PERSONAL As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_CP_SUBJECT_PERSONAL As System.Windows.Forms.Label
    Friend WithEvents lblLastSync_tb_CP_SUBJECT_MASTER As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_CP_SUBJECT_MASTER As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_CP_SUBJECT_MASTER As System.Windows.Forms.Label
    Friend WithEvents btn_tb_CP_SUBJECT_MASTER As System.Windows.Forms.Button
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents btn_tb_PN_PUNISH_CODE_R As System.Windows.Forms.Button
    Friend WithEvents lblLastSync_tb_PN_PUNISH_CODE_R As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_PN_PUNISH_CODE_R As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_PN_PUNISH_CODE_R As System.Windows.Forms.Label
    Friend WithEvents lblLastSync_tb_PN_PUNISH_T As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_PN_PUNISH_T As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_PN_PUNISH_T As System.Windows.Forms.Label
    Friend WithEvents btn_tb_PN_PUNISH_T As System.Windows.Forms.Button
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents btn_tb_PK_EMP_SELECT As System.Windows.Forms.Button
    Friend WithEvents lblLastSync_tb_PK_EMP_SELECT As System.Windows.Forms.Label
    Friend WithEvents lblRec_tb_PK_EMP_SELECT As System.Windows.Forms.Label
    Friend WithEvents lblStatus_tb_PK_EMP_SELECT As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents btnStartAll As System.Windows.Forms.Button

End Class
