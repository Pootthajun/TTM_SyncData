﻿Imports TTM_SyncData.Org.Mentalis.Files

Public Class frmMain

    Dim INIFileName As String = Application.StartupPath & "\TTM-SyncData.ini"  'Parth ที่ใช้เก็บไฟล์ .ini
    Dim TimerProcress As Boolean = False
    Dim T_tb_PN_DEPARTMENT_R As New Threading.Thread(AddressOf Sync_tb_PN_DEPARTMENT_R)
    Dim T_tb_PN_PERSONAL_STAT_R As New Threading.Thread(AddressOf Sync_tb_PN_PERSONAL_STAT_R)
    Dim T_tb_PN_PERSONAL_VIEW As New Threading.Thread(AddressOf Sync_tb_PN_PERSONAL_VIEW)
    Dim T_tb_PN_POSITION_FLD_R As New Threading.Thread(AddressOf Sync_tb_PN_POSITION_FLD_R)
    Dim T_tb_PN_POSITION_MGR_R As New Threading.Thread(AddressOf Sync_tb_PN_POSITION_MGR_R)
    Dim T_tb_PN_POSITION_T As New Threading.Thread(AddressOf Sync_tb_PN_POSITION_T)
    Dim T_tb_PN_LEAVE_T As New Threading.Thread(AddressOf Sync_tb_PN_LEAVE_T)
    Dim T_tb_PN_POSITION_FLD_FUNC As New Threading.Thread(AddressOf Sync_tb_PN_POSITION_FLD_FUNC)
    Dim T_tb_PK_COURSE As New Threading.Thread(AddressOf Sync_tb_PK_COURSE)
    Dim T_tb_PK_EMP_SELECT As New Threading.Thread(AddressOf Sync_tb_PK_EMP_SELECT)
    Dim T_tb_PN_PUNISH_CODE_R As New Threading.Thread(AddressOf Sync_tb_PN_PUNISH_CODE_R)
    Dim T_tb_PN_PUNISH_T As New Threading.Thread(AddressOf Sync_tb_PN_PUNISH_T)
    Dim T_tb_CP_SUBJECT_MASTER As New Threading.Thread(AddressOf Sync_tb_CP_SUBJECT_MASTER)
    Dim T_tb_CP_SUBJECT_PERSONAL As New Threading.Thread(AddressOf Sync_tb_CP_SUBJECT_PERSONAL)
    Dim T_tb_PN_POSITION_HISTORY_T As New Threading.Thread(AddressOf Sync_tb_PN_POSITION_HISTORY_T)
    Dim T_tb_PN_POSITION_RETURN2CLASS As New Threading.Thread(AddressOf Sync_tb_PN_POSITION_RETURN2CLASS)
    Dim T_tb_PK_SUBJ_G As New Threading.Thread(AddressOf Sync_tb_PK_SUBJ_G)


    Private Sub txtHH_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtHH.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtMM.Focus()
        ElseIf (e.KeyChar < "0" Or e.KeyChar > "9") And Asc(e.KeyChar) <> 8 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtMM_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMM.KeyPress
        If Asc(e.KeyChar) = 13 Then
            cbAuto.Focus()
        ElseIf (e.KeyChar < "0" Or e.KeyChar > "9") And Asc(e.KeyChar) <> 8 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtHH_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHH.LostFocus
        If txtHH.Text.Trim = "" Then
            txtHH.Text = "00"
        Else
            If CInt(txtHH.Text) > 23 Then
                txtHH.Text = "23"
            End If
            txtHH.Text = txtHH.Text.PadLeft(2, "0")
        End If

        Dim ini As New IniReader(INIFileName)
        ini.Section = "Time"
        ini.Write("HH", txtHH.Text)
    End Sub

    Private Sub txtMM_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMM.LostFocus
        If txtMM.Text.Trim = "" Then
            txtMM.Text = "00"
        Else
            If CInt(txtMM.Text) > 59 Then
                txtMM.Text = "59"
            End If
            txtMM.Text = txtMM.Text.PadLeft(2, "0")
        End If

        Dim ini As New IniReader(INIFileName)
        ini.Section = "Time"
        ini.Write("MM", txtMM.Text)
    End Sub

    Private Sub cbAuto_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbAuto.CheckedChanged
        Dim ini As New IniReader(INIFileName)
        ini.Section = "Time"

        If cbAuto.Checked = True Then
            ini.Write("Auto", "T")
        Else
            ini.Write("Auto", "F")
        End If

    End Sub

    Function GetLastSync() As String
        Dim ret As String = ""
        Dim d As String = ""
        Dim m As String = ""
        Dim y As String = ""
        Dim hh As String = ""
        Dim mm As String = ""

        Dim dmy As DateTime = Date.Now
        d = dmy.Day
        m = dmy.Month
        y = dmy.Year
        hh = dmy.Hour
        mm = dmy.Minute
        If y > 2500 Then
            y = y - 543
        End If
        ret = d.ToString.PadLeft(2, "0") & "-" & m.ToString.PadLeft(2, "0") & "-" & y.ToString & "   "
        ret &= hh.ToString.PadLeft(2, "0") & ":" & mm.ToString.PadLeft(2, "0")
        Return ret
    End Function

    Private Sub RecordCountChanged(ByVal TableName As String, ByVal Record As Integer)
        Select Case TableName
            Case "tb_PN_DEPARTMENT_R"
                Application.DoEvents()
                lblRec_tb_PN_DEPARTMENT_R.Text = Record
            Case "tb_PN_PERSONAL_STAT_R"
                Application.DoEvents()
                lblRec_tb_PN_PERSONAL_STAT_R.Text = Record
            Case "tb_PN_PERSONAL_VIEW"
                Application.DoEvents()
                lblRec_tb_PN_PERSONAL_VIEW.Text = Record
            Case "tb_PN_POSITION_FLD_R"
                Application.DoEvents()
                lblRec_tb_PN_POSITION_FLD_R.Text = Record
            Case "tb_PN_POSITION_MGR_R"
                Application.DoEvents()
                lblRec_tb_PN_POSITION_MGR_R.Text = Record
            Case "tb_PN_POSITION_T"
                Application.DoEvents()
                lblRec_tb_PN_POSITION_T.Text = Record
            Case "tb_PN_LEAVE_T"
                Application.DoEvents()
                lblRec_tb_PN_LEAVE_T.Text = Record
            Case "tb_PN_POSITION_FLD_FUNC"
                Application.DoEvents()
                lblRec_tb_PN_POSITION_FLD_FUNC.Text = Record
            Case "tb_PK_COURSE"
                Application.DoEvents()
                lblRec_tb_PK_COURSE.Text = Record
            Case "tb_PK_EMP_SELECT"
                Application.DoEvents()
                lblRec_tb_PK_EMP_SELECT.Text = Record
            Case "tb_PN_PUNISH_CODE_R"
                Application.DoEvents()
                lblRec_tb_PN_PUNISH_CODE_R.Text = Record
            Case "tb_PN_PUNISH_T"
                Application.DoEvents()
                lblRec_tb_PN_PUNISH_T.Text = Record
            Case "tb_CP_SUBJECT_MASTER"
                Application.DoEvents()
                lblRec_tb_CP_SUBJECT_MASTER.Text = Record
            Case "tb_CP_SUBJECT_PERSONAL"
                Application.DoEvents()
                lblRec_tb_CP_SUBJECT_PERSONAL.Text = Record
            Case "tb_PN_POSITION_HISTORY_T"
                Application.DoEvents()
                lblRec_tb_PN_POSITION_HISTORY_T.Text = Record
            Case "tb_PN_POSITION_RETURN2CLASS"
                Application.DoEvents()
                lblRec_tb_PN_POSITION_RETURN2CLASS.Text = Record
            Case "tb_PK_SUBJ_G"
                Application.DoEvents()
                lblRec_tb_PK_SUBJ_G.Text = Record
        End Select
    End Sub

    Private Sub SyncComplete(ByVal TableName As String, ByVal Status As String)
        Dim ini As New IniReader(INIFileName)
        ini.Section = "Time"

        Application.DoEvents()
        Select Case TableName
            Case "tb_PN_DEPARTMENT_R"
                lblStatus_tb_PN_DEPARTMENT_R.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_PN_DEPARTMENT_R.Text = GetLastSync()
                    ini.Write("LastSync_tb_PN_DEPARTMENT_R", lblLastSync_tb_PN_DEPARTMENT_R.Text)
                End If
            Case "tb_PN_PERSONAL_STAT_R"
                lblStatus_tb_PN_PERSONAL_STAT_R.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_PN_PERSONAL_STAT_R.Text = GetLastSync()
                    ini.Write("LastSync_tb_PN_PERSONAL_STAT_R", lblLastSync_tb_PN_PERSONAL_STAT_R.Text)
                End If
            Case "tb_PN_PERSONAL_VIEW"
                lblStatus_tb_PN_PERSONAL_VIEW.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_PN_PERSONAL_VIEW.Text = GetLastSync()
                    ini.Write("LastSync_tb_PN_PERSONAL_VIEW", lblLastSync_tb_PN_PERSONAL_VIEW.Text)
                End If
            Case "tb_PN_POSITION_FLD_R"
                lblStatus_tb_PN_POSITION_FLD_R.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_PN_POSITION_FLD_R.Text = GetLastSync()
                    ini.Write("LastSync_tb_PN_POSITION_FLD_R", lblLastSync_tb_PN_POSITION_FLD_R.Text)
                End If
            Case "tb_PN_POSITION_MGR_R"
                lblStatus_tb_PN_POSITION_MGR_R.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_PN_POSITION_MGR_R.Text = GetLastSync()
                    ini.Write("LastSync_tb_PN_POSITION_MGR_R", lblLastSync_tb_PN_POSITION_MGR_R.Text)
                End If
            Case "tb_PN_POSITION_T"
                lblStatus_tb_PN_POSITION_T.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_PN_POSITION_T.Text = GetLastSync()
                    ini.Write("LastSync_tb_PN_POSITION_T", lblLastSync_tb_PN_POSITION_T.Text)
                End If
            Case "tb_PN_LEAVE_T"
                lblStatus_tb_PN_LEAVE_T.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_PN_LEAVE_T.Text = GetLastSync()
                    ini.Write("LastSync_tb_PN_LEAVE_T", lblLastSync_tb_PN_LEAVE_T.Text)
                End If
            Case "tb_PN_POSITION_FLD_FUNC"
                lblStatus_tb_PN_POSITION_FLD_FUNC.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_PN_POSITION_FLD_FUNC.Text = GetLastSync()
                    ini.Write("LastSync_tb_PN_POSITION_FLD_FUNC", lblLastSync_tb_PN_POSITION_FLD_FUNC.Text)
                End If
            Case "tb_PK_COURSE"
                lblStatus_tb_PK_COURSE.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_PK_COURSE.Text = GetLastSync()
                    ini.Write("LastSync_tb_PK_COURSE", lblLastSync_tb_PK_COURSE.Text)
                End If
            Case "tb_PK_EMP_SELECT"
                lblStatus_tb_PK_EMP_SELECT.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_PK_EMP_SELECT.Text = GetLastSync()
                    ini.Write("LastSync_tb_PK_EMP_SELECT", lblLastSync_tb_PK_EMP_SELECT.Text)
                End If
            Case "tb_PN_PUNISH_CODE_R"
                lblStatus_tb_PN_PUNISH_CODE_R.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_PN_PUNISH_CODE_R.Text = GetLastSync()
                    ini.Write("LastSync_tb_PN_PUNISH_CODE_R", lblLastSync_tb_PN_PUNISH_CODE_R.Text)
                End If
            Case "tb_PN_PUNISH_T"
                lblStatus_tb_PN_PUNISH_T.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_PN_PUNISH_T.Text = GetLastSync()
                    ini.Write("LastSync_tb_PN_PUNISH_T", lblLastSync_tb_PN_PUNISH_T.Text)
                End If
            Case "tb_CP_SUBJECT_MASTER"
                lblStatus_tb_CP_SUBJECT_MASTER.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_CP_SUBJECT_MASTER.Text = GetLastSync()
                    ini.Write("LastSync_tb_CP_SUBJECT_MASTER", lblLastSync_tb_CP_SUBJECT_MASTER.Text)
                End If
            Case "tb_CP_SUBJECT_PERSONAL"
                lblStatus_tb_CP_SUBJECT_PERSONAL.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_CP_SUBJECT_PERSONAL.Text = GetLastSync()
                    ini.Write("LastSync_tb_CP_SUBJECT_PERSONAL", lblLastSync_tb_CP_SUBJECT_PERSONAL.Text)
                End If
            Case "tb_PN_POSITION_HISTORY_T"
                lblStatus_tb_PN_POSITION_HISTORY_T.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_PN_POSITION_HISTORY_T.Text = GetLastSync()
                    ini.Write("LastSync_tb_PN_POSITION_HISTORY_T", lblLastSync_tb_PN_POSITION_HISTORY_T.Text)
                End If
            Case "tb_PN_POSITION_RETURN2CLASS"
                lblStatus_tb_PN_POSITION_RETURN2CLASS.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_PN_POSITION_RETURN2CLASS.Text = GetLastSync()
                    ini.Write("LastSync_tb_PN_POSITION_RETURN2CLASS", lblLastSync_tb_PN_POSITION_RETURN2CLASS.Text)
                End If
            Case "tb_PK_SUBJ_G"
                lblStatus_tb_PK_SUBJ_G.Text = Status
                If Status = "Complete" Then
                    lblLastSync_tb_PK_SUBJ_G.Text = GetLastSync()
                    ini.Write("LastSync_tb_PK_SUBJ_G", lblLastSync_tb_PK_SUBJ_G.Text)
                End If
        End Select
    End Sub

    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Application.Exit()
    End Sub

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CheckForIllegalCrossThreadCalls = False
        Dim ini As New IniReader(INIFileName)
        ini.Section = "Time"
        txtHH.Text = ini.ReadString("HH")
        txtMM.Text = ini.ReadString("MM")
        lblLastSync_tb_PN_DEPARTMENT_R.Text = ini.ReadString("LastSync_tb_PN_DEPARTMENT_R")
        lblLastSync_tb_PN_PERSONAL_STAT_R.Text = ini.ReadString("LastSync_tb_PN_PERSONAL_STAT_R")
        lblLastSync_tb_PN_PERSONAL_VIEW.Text = ini.ReadString("LastSync_tb_PN_PERSONAL_VIEW")
        lblLastSync_tb_PN_POSITION_FLD_R.Text = ini.ReadString("LastSync_tb_PN_POSITION_FLD_R")
        lblLastSync_tb_PN_POSITION_MGR_R.Text = ini.ReadString("LastSync_tb_PN_POSITION_MGR_R")
        lblLastSync_tb_PN_POSITION_T.Text = ini.ReadString("LastSync_tb_PN_POSITION_T")
        lblLastSync_tb_PN_LEAVE_T.Text = ini.ReadString("LastSync_tb_PN_LEAVE_T")
        lblLastSync_tb_PN_POSITION_FLD_FUNC.Text = ini.ReadString("LastSync_tb_PN_POSITION_FLD_FUNC")
        lblLastSync_tb_PK_COURSE.Text = ini.ReadString("LastSync_tb_PK_COURSE")
        lblLastSync_tb_PK_EMP_SELECT.Text = ini.ReadString("LastSync_tb_PK_EMP_SELECT")
        lblLastSync_tb_PN_PUNISH_CODE_R.Text = ini.ReadString("LastSync_tb_PN_PUNISH_CODE_R")
        lblLastSync_tb_PN_PUNISH_T.Text = ini.ReadString("LastSync_tb_PN_PUNISH_T")
        lblLastSync_tb_CP_SUBJECT_MASTER.Text = ini.ReadString("LastSync_tb_CP_SUBJECT_MASTER")
        lblLastSync_tb_CP_SUBJECT_PERSONAL.Text = ini.ReadString("LastSync_tb_CP_SUBJECT_PERSONAL")
        lblLastSync_tb_PN_POSITION_HISTORY_T.Text = ini.ReadString("LastSync_tb_PN_POSITION_HISTORY_T")
        lblLastSync_tb_PN_POSITION_RETURN2CLASS.Text = ini.ReadString("LastSync_tb_PN_POSITION_RETURN2CLASS")
        lblLastSync_tb_PK_SUBJ_G.Text = ini.ReadString("LastSync_tb_PK_SUBJ_G")

        cbAuto.Checked = ini.ReadString("Auto") = "T"
        Me.Text &= " V " & My.Application.Info.Version.ToString
    End Sub

    Private Sub frmMain_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Label1.Focus()
    End Sub

    Private Sub btn_tb_PN_DEPARTMENT_R_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_PN_DEPARTMENT_R.Click
        If btn_tb_PN_DEPARTMENT_R.Text = "Start" Then
            T_tb_PN_DEPARTMENT_R = New Threading.Thread(AddressOf Sync_tb_PN_DEPARTMENT_R)
            T_tb_PN_DEPARTMENT_R.Start()
            T_tb_PN_DEPARTMENT_R.IsBackground = True
        Else
            T_tb_PN_DEPARTMENT_R.Abort()
            btn_tb_PN_DEPARTMENT_R.Text = "Start"
            btn_tb_PN_DEPARTMENT_R.Image = My.Resources.Start
            lblStatus_tb_PN_DEPARTMENT_R.Text = "Stop"
        End If
    End Sub
    Public Sub Sync_tb_PN_DEPARTMENT_R()
        Try
            btn_tb_PN_DEPARTMENT_R.Text = "Stop"
            btn_tb_PN_DEPARTMENT_R.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_PN_DEPARTMENT_R.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_PN_DEPARTMENT_R()
        Catch ex As Exception
            If lblStatus_tb_PN_DEPARTMENT_R.Text <> "Stop" Then
                lblStatus_tb_PN_DEPARTMENT_R.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_PN_DEPARTMENT_R.Text = "Start"
        btn_tb_PN_DEPARTMENT_R.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_PN_PERSONAL_STAT_R_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_PN_PERSONAL_STAT_R.Click
        If btn_tb_PN_PERSONAL_STAT_R.Text = "Start" Then
            T_tb_PN_PERSONAL_STAT_R = New Threading.Thread(AddressOf Sync_tb_PN_PERSONAL_STAT_R)
            T_tb_PN_PERSONAL_STAT_R.Start()
            T_tb_PN_PERSONAL_STAT_R.IsBackground = True
        Else
            T_tb_PN_PERSONAL_STAT_R.Abort()
            btn_tb_PN_PERSONAL_STAT_R.Text = "Start"
            btn_tb_PN_PERSONAL_STAT_R.Image = My.Resources.Start
            lblStatus_tb_PN_PERSONAL_STAT_R.Text = "Stop"
        End If
    End Sub
    Public Sub Sync_tb_PN_PERSONAL_STAT_R()
        Try
            btn_tb_PN_PERSONAL_STAT_R.Text = "Stop"
            btn_tb_PN_PERSONAL_STAT_R.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_PN_PERSONAL_STAT_R.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_PN_PERSONAL_STAT_R()
        Catch ex As Exception
            If lblStatus_tb_PN_PERSONAL_STAT_R.Text <> "Stop" Then
                lblStatus_tb_PN_PERSONAL_STAT_R.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_PN_PERSONAL_STAT_R.Text = "Start"
        btn_tb_PN_PERSONAL_STAT_R.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_PN_PERSONAL_VIEW_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_PN_PERSONAL_VIEW.Click
        If btn_tb_PN_PERSONAL_VIEW.Text = "Start" Then
            T_tb_PN_PERSONAL_VIEW = New Threading.Thread(AddressOf Sync_tb_PN_PERSONAL_VIEW)
            T_tb_PN_PERSONAL_VIEW.Start()
            T_tb_PN_PERSONAL_VIEW.IsBackground = True
        Else
            T_tb_PN_PERSONAL_VIEW.Abort()
            btn_tb_PN_PERSONAL_VIEW.Text = "Start"
            btn_tb_PN_PERSONAL_VIEW.Image = My.Resources.Start
            lblStatus_tb_PN_PERSONAL_VIEW.Text = "Stop"
        End If
    End Sub
    Public Sub Sync_tb_PN_PERSONAL_VIEW()
        Try
            btn_tb_PN_PERSONAL_VIEW.Text = "Stop"
            btn_tb_PN_PERSONAL_VIEW.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_PN_PERSONAL_VIEW.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_PN_PERSONAL_VIEW()
        Catch ex As Exception
            If lblStatus_tb_PN_PERSONAL_VIEW.Text <> "Stop" Then
                lblStatus_tb_PN_PERSONAL_VIEW.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_PN_PERSONAL_VIEW.Text = "Start"
        btn_tb_PN_PERSONAL_VIEW.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_PN_POSITION_FLD_R_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_PN_POSITION_FLD_R.Click
        If btn_tb_PN_POSITION_FLD_R.Text = "Start" Then
            T_tb_PN_POSITION_FLD_R = New Threading.Thread(AddressOf Sync_tb_PN_POSITION_FLD_R)
            T_tb_PN_POSITION_FLD_R.Start()
            T_tb_PN_POSITION_FLD_R.IsBackground = True
        Else
            T_tb_PN_POSITION_FLD_R.Abort()
            btn_tb_PN_POSITION_FLD_R.Text = "Start"
            btn_tb_PN_POSITION_FLD_R.Image = My.Resources.Start
            lblStatus_tb_PN_POSITION_FLD_R.Text = "Stop"
        End If
    End Sub
    Public Sub Sync_tb_PN_POSITION_FLD_R()
        Try
            btn_tb_PN_POSITION_FLD_R.Text = "Stop"
            btn_tb_PN_POSITION_FLD_R.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_PN_POSITION_FLD_R.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_PN_POSITION_FLD_R()
        Catch ex As Exception
            If lblStatus_tb_PN_POSITION_FLD_R.Text <> "Stop" Then
                lblStatus_tb_PN_POSITION_FLD_R.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_PN_POSITION_FLD_R.Text = "Start"
        btn_tb_PN_POSITION_FLD_R.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_PN_POSITION_MGR_R_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_PN_POSITION_MGR_R.Click
        If btn_tb_PN_POSITION_MGR_R.Text = "Start" Then
            T_tb_PN_POSITION_MGR_R = New Threading.Thread(AddressOf Sync_tb_PN_POSITION_MGR_R)
            T_tb_PN_POSITION_MGR_R.Start()
            T_tb_PN_POSITION_MGR_R.IsBackground = True
        Else
            T_tb_PN_POSITION_MGR_R.Abort()
            btn_tb_PN_POSITION_MGR_R.Text = "Start"
            btn_tb_PN_POSITION_MGR_R.Image = My.Resources.Start
            lblStatus_tb_PN_POSITION_MGR_R.Text = "Stop"
        End If
    End Sub
    Public Sub Sync_tb_PN_POSITION_MGR_R()
        Try
            btn_tb_PN_POSITION_MGR_R.Text = "Stop"
            btn_tb_PN_POSITION_MGR_R.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_PN_POSITION_MGR_R.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_PN_POSITION_MGR_R()
        Catch ex As Exception
            If lblStatus_tb_PN_POSITION_MGR_R.Text <> "Stop" Then
                lblStatus_tb_PN_POSITION_MGR_R.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_PN_POSITION_MGR_R.Text = "Start"
        btn_tb_PN_POSITION_MGR_R.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_PN_POSITION_T_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_PN_POSITION_T.Click
        If btn_tb_PN_POSITION_T.Text = "Start" Then
            T_tb_PN_POSITION_T = New Threading.Thread(AddressOf Sync_tb_PN_POSITION_T)
            T_tb_PN_POSITION_T.Start()
            T_tb_PN_POSITION_T.IsBackground = True
        Else
            T_tb_PN_POSITION_T.Abort()
            btn_tb_PN_POSITION_T.Text = "Start"
            btn_tb_PN_POSITION_T.Image = My.Resources.Start
            lblStatus_tb_PN_POSITION_T.Text = "Stop"
        End If
    End Sub
    Public Sub Sync_tb_PN_POSITION_T()
        Try
            btn_tb_PN_POSITION_T.Text = "Stop"
            btn_tb_PN_POSITION_T.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_PN_POSITION_T.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_PN_POSITION_T()
        Catch ex As Exception
            If lblStatus_tb_PN_POSITION_T.Text <> "Stop" Then
                lblStatus_tb_PN_POSITION_T.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_PN_POSITION_T.Text = "Start"
        btn_tb_PN_POSITION_T.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_PN_LEAVE_T_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_PN_LEAVE_T.Click
        If btn_tb_PN_LEAVE_T.Text = "Start" Then
            T_tb_PN_LEAVE_T = New Threading.Thread(AddressOf Sync_tb_PN_LEAVE_T)
            T_tb_PN_LEAVE_T.Start()
            T_tb_PN_LEAVE_T.IsBackground = True
        Else
            T_tb_PN_LEAVE_T.Abort()
            btn_tb_PN_LEAVE_T.Text = "Start"
            btn_tb_PN_LEAVE_T.Image = My.Resources.Start
            lblStatus_tb_PN_LEAVE_T.Text = "Stop"
        End If
    End Sub
    Public Sub Sync_tb_PN_LEAVE_T()
        Try
            btn_tb_PN_LEAVE_T.Text = "Stop"
            btn_tb_PN_LEAVE_T.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_PN_LEAVE_T.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_PN_LEAVE_T()
        Catch ex As Exception
            If lblStatus_tb_PN_LEAVE_T.Text <> "Stop" Then
                lblStatus_tb_PN_LEAVE_T.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_PN_LEAVE_T.Text = "Start"
        btn_tb_PN_LEAVE_T.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_PN_POSITION_FLD_FUNC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_PN_POSITION_FLD_FUNC.Click
        If btn_tb_PN_POSITION_FLD_FUNC.Text = "Start" Then
            T_tb_PN_POSITION_FLD_FUNC = New Threading.Thread(AddressOf Sync_tb_PN_POSITION_FLD_FUNC)
            T_tb_PN_POSITION_FLD_FUNC.Start()
            T_tb_PN_POSITION_FLD_FUNC.IsBackground = True
        Else
            T_tb_PN_POSITION_FLD_FUNC.Abort()
            btn_tb_PN_POSITION_FLD_FUNC.Text = "Start"
            btn_tb_PN_POSITION_FLD_FUNC.Image = My.Resources.Start
            lblStatus_tb_PN_POSITION_FLD_FUNC.Text = "Stop"
        End If
    End Sub

    Public Sub Sync_tb_PN_POSITION_FLD_FUNC()
        Try
            btn_tb_PN_POSITION_FLD_FUNC.Text = "Stop"
            btn_tb_PN_POSITION_FLD_FUNC.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_PN_POSITION_FLD_FUNC.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_PN_POSITION_FLD_FUNC()
        Catch ex As Exception
            If lblStatus_tb_PN_POSITION_FLD_FUNC.Text <> "Stop" Then
                lblStatus_tb_PN_POSITION_FLD_FUNC.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_PN_POSITION_FLD_FUNC.Text = "Start"
        btn_tb_PN_POSITION_FLD_FUNC.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_PK_COURSE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_PK_COURSE.Click
        If btn_tb_PK_COURSE.Text = "Start" Then
            T_tb_PK_COURSE = New Threading.Thread(AddressOf Sync_tb_PK_COURSE)
            T_tb_PK_COURSE.Start()
            T_tb_PK_COURSE.IsBackground = True
        Else
            T_tb_PK_COURSE.Abort()
            btn_tb_PK_COURSE.Text = "Start"
            btn_tb_PK_COURSE.Image = My.Resources.Start
            lblStatus_tb_PK_COURSE.Text = "Stop"
        End If
    End Sub

    Public Sub Sync_tb_PK_COURSE()
        Try
            btn_tb_PK_COURSE.Text = "Stop"
            btn_tb_PK_COURSE.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_PK_COURSE.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_PK_COURSE()
        Catch ex As Exception
            If lblStatus_tb_PK_COURSE.Text <> "Stop" Then
                lblStatus_tb_PK_COURSE.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_PK_COURSE.Text = "Start"
        btn_tb_PK_COURSE.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_PK_EMP_SELECT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_PK_EMP_SELECT.Click
        If btn_tb_PK_EMP_SELECT.Text = "Start" Then
            T_tb_PK_EMP_SELECT = New Threading.Thread(AddressOf Sync_tb_PK_EMP_SELECT)
            T_tb_PK_EMP_SELECT.Start()
            T_tb_PK_EMP_SELECT.IsBackground = True
        Else
            T_tb_PK_EMP_SELECT.Abort()
            btn_tb_PK_EMP_SELECT.Text = "Start"
            btn_tb_PK_EMP_SELECT.Image = My.Resources.Start
            lblStatus_tb_PK_EMP_SELECT.Text = "Stop"
        End If
    End Sub

    Public Sub Sync_tb_PK_EMP_SELECT()
        Try
            btn_tb_PK_EMP_SELECT.Text = "Stop"
            btn_tb_PK_EMP_SELECT.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_PK_EMP_SELECT.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_PK_EMP_SELECT()
        Catch ex As Exception
            If lblStatus_tb_PK_EMP_SELECT.Text <> "Stop" Then
                lblStatus_tb_PK_EMP_SELECT.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_PK_EMP_SELECT.Text = "Start"
        btn_tb_PK_EMP_SELECT.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_PN_PUNISH_CODE_R_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_PN_PUNISH_CODE_R.Click
        If btn_tb_PN_PUNISH_CODE_R.Text = "Start" Then
            T_tb_PN_PUNISH_CODE_R = New Threading.Thread(AddressOf Sync_tb_PN_PUNISH_CODE_R)
            T_tb_PN_PUNISH_CODE_R.Start()
            T_tb_PN_PUNISH_CODE_R.IsBackground = True
        Else
            T_tb_PN_PUNISH_CODE_R.Abort()
            btn_tb_PN_PUNISH_CODE_R.Text = "Start"
            btn_tb_PN_PUNISH_CODE_R.Image = My.Resources.Start
            lblStatus_tb_PN_PUNISH_CODE_R.Text = "Stop"
        End If
    End Sub

    Public Sub Sync_tb_PN_PUNISH_CODE_R()
        Try
            btn_tb_PN_PUNISH_CODE_R.Text = "Stop"
            btn_tb_PN_PUNISH_CODE_R.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_PN_PUNISH_CODE_R.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_PN_PUNISH_CODE_R()
        Catch ex As Exception
            If lblStatus_tb_PN_PUNISH_CODE_R.Text <> "Stop" Then
                lblStatus_tb_PN_PUNISH_CODE_R.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_PN_PUNISH_CODE_R.Text = "Start"
        btn_tb_PN_PUNISH_CODE_R.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_PN_PUNISH_T_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_PN_PUNISH_T.Click
        If btn_tb_PN_PUNISH_T.Text = "Start" Then
            T_tb_PN_PUNISH_T = New Threading.Thread(AddressOf Sync_tb_PN_PUNISH_T)
            T_tb_PN_PUNISH_T.Start()
            T_tb_PN_PUNISH_T.IsBackground = True
        Else
            T_tb_PN_PUNISH_T.Abort()
            btn_tb_PN_PUNISH_T.Text = "Start"
            btn_tb_PN_PUNISH_T.Image = My.Resources.Start
            lblStatus_tb_PN_PUNISH_T.Text = "Stop"
        End If
    End Sub

    Public Sub Sync_tb_PN_PUNISH_T()
        Try
            btn_tb_PN_PUNISH_T.Text = "Stop"
            btn_tb_PN_PUNISH_T.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_PN_PUNISH_T.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_PN_PUNISH_T()
        Catch ex As Exception
            If lblStatus_tb_PN_PUNISH_T.Text <> "Stop" Then
                lblStatus_tb_PN_PUNISH_T.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_PN_PUNISH_T.Text = "Start"
        btn_tb_PN_PUNISH_T.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_CP_SUBJECT_MASTER_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_CP_SUBJECT_MASTER.Click
        If btn_tb_CP_SUBJECT_MASTER.Text = "Start" Then
            T_tb_CP_SUBJECT_MASTER = New Threading.Thread(AddressOf Sync_tb_CP_SUBJECT_MASTER)
            T_tb_CP_SUBJECT_MASTER.Start()
            T_tb_CP_SUBJECT_MASTER.IsBackground = True
        Else
            T_tb_CP_SUBJECT_MASTER.Abort()
            btn_tb_CP_SUBJECT_MASTER.Text = "Start"
            btn_tb_CP_SUBJECT_MASTER.Image = My.Resources.Start
            lblStatus_tb_CP_SUBJECT_MASTER.Text = "Stop"
        End If
    End Sub

    Public Sub Sync_tb_CP_SUBJECT_MASTER()
        Try
            btn_tb_CP_SUBJECT_MASTER.Text = "Stop"
            btn_tb_CP_SUBJECT_MASTER.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_CP_SUBJECT_MASTER.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_CP_SUBJECT_MASTER()
        Catch ex As Exception
            If lblStatus_tb_CP_SUBJECT_MASTER.Text <> "Stop" Then
                lblStatus_tb_CP_SUBJECT_MASTER.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_CP_SUBJECT_MASTER.Text = "Start"
        btn_tb_CP_SUBJECT_MASTER.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_CP_SUBJECT_PERSONAL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_CP_SUBJECT_PERSONAL.Click
        If btn_tb_CP_SUBJECT_PERSONAL.Text = "Start" Then
            T_tb_CP_SUBJECT_PERSONAL = New Threading.Thread(AddressOf Sync_tb_CP_SUBJECT_PERSONAL)
            T_tb_CP_SUBJECT_PERSONAL.Start()
            T_tb_CP_SUBJECT_PERSONAL.IsBackground = True
        Else
            T_tb_CP_SUBJECT_PERSONAL.Abort()
            btn_tb_CP_SUBJECT_PERSONAL.Text = "Start"
            btn_tb_CP_SUBJECT_PERSONAL.Image = My.Resources.Start
            lblStatus_tb_CP_SUBJECT_PERSONAL.Text = "Stop"
        End If
    End Sub

    Public Sub Sync_tb_CP_SUBJECT_PERSONAL()
        Try
            btn_tb_CP_SUBJECT_PERSONAL.Text = "Stop"
            btn_tb_CP_SUBJECT_PERSONAL.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_CP_SUBJECT_PERSONAL.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_CP_SUBJECT_PERSONAL()
        Catch ex As Exception
            If lblStatus_tb_CP_SUBJECT_PERSONAL.Text <> "Stop" Then
                lblStatus_tb_CP_SUBJECT_PERSONAL.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_CP_SUBJECT_PERSONAL.Text = "Start"
        btn_tb_CP_SUBJECT_PERSONAL.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_PN_POSITION_HISTORY_T_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_PN_POSITION_HISTORY_T.Click
        If btn_tb_PN_POSITION_HISTORY_T.Text = "Start" Then
            T_tb_PN_POSITION_HISTORY_T = New Threading.Thread(AddressOf Sync_tb_PN_POSITION_HISTORY_T)
            T_tb_PN_POSITION_HISTORY_T.Start()
            T_tb_PN_POSITION_HISTORY_T.IsBackground = True
        Else
            T_tb_PN_POSITION_HISTORY_T.Abort()
            btn_tb_PN_POSITION_HISTORY_T.Text = "Start"
            btn_tb_PN_POSITION_HISTORY_T.Image = My.Resources.Start
            lblStatus_tb_PN_POSITION_HISTORY_T.Text = "Stop"
        End If
    End Sub

    Public Sub Sync_tb_PN_POSITION_HISTORY_T()
        Try
            btn_tb_PN_POSITION_HISTORY_T.Text = "Stop"
            btn_tb_PN_POSITION_HISTORY_T.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_PN_POSITION_HISTORY_T.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_PN_POSITION_HISTORY_T()
        Catch ex As Exception
            If lblStatus_tb_PN_POSITION_HISTORY_T.Text <> "Stop" Then
                lblStatus_tb_PN_POSITION_HISTORY_T.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_PN_POSITION_HISTORY_T.Text = "Start"
        btn_tb_PN_POSITION_HISTORY_T.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_PN_POSITION_RETURN2CLASS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_PN_POSITION_RETURN2CLASS.Click
        If btn_tb_PN_POSITION_RETURN2CLASS.Text = "Start" Then
            T_tb_PN_POSITION_RETURN2CLASS = New Threading.Thread(AddressOf Sync_tb_PN_POSITION_RETURN2CLASS)
            T_tb_PN_POSITION_RETURN2CLASS.Start()
            T_tb_PN_POSITION_RETURN2CLASS.IsBackground = True
        Else
            T_tb_PN_POSITION_RETURN2CLASS.Abort()
            btn_tb_PN_POSITION_RETURN2CLASS.Text = "Start"
            btn_tb_PN_POSITION_RETURN2CLASS.Image = My.Resources.Start
            lblStatus_tb_PN_POSITION_RETURN2CLASS.Text = "Stop"
        End If
    End Sub

    Public Sub Sync_tb_PN_POSITION_RETURN2CLASS()
        Try
            btn_tb_PN_POSITION_RETURN2CLASS.Text = "Stop"
            btn_tb_PN_POSITION_RETURN2CLASS.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_PN_POSITION_RETURN2CLASS.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_PN_POSITION_RETURN2CLASS()
        Catch ex As Exception
            If lblStatus_tb_PN_POSITION_RETURN2CLASS.Text <> "Stop" Then
                lblStatus_tb_PN_POSITION_RETURN2CLASS.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_PN_POSITION_RETURN2CLASS.Text = "Start"
        btn_tb_PN_POSITION_RETURN2CLASS.Image = My.Resources.Start
    End Sub

    Private Sub btn_tb_PK_SUBJ_G_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tb_PK_SUBJ_G.Click
        If btn_tb_PK_SUBJ_G.Text = "Start" Then
            T_tb_PK_SUBJ_G = New Threading.Thread(AddressOf Sync_tb_PK_SUBJ_G)
            T_tb_PK_SUBJ_G.Start()
            T_tb_PK_SUBJ_G.IsBackground = True
        Else
            T_tb_PK_SUBJ_G.Abort()
            btn_tb_PK_SUBJ_G.Text = "Start"
            btn_tb_PK_SUBJ_G.Image = My.Resources.Start
            lblStatus_tb_PK_SUBJ_G.Text = "Stop"
        End If
    End Sub

    Public Sub Sync_tb_PK_SUBJ_G()
        Try
            btn_tb_PK_SUBJ_G.Text = "Stop"
            btn_tb_PK_SUBJ_G.Image = My.Resources._Stop
            Dim Sync As New SyncData.Sync
            lblStatus_tb_PK_SUBJ_G.Text = "Sync Data"
            AddHandler Sync.RecordCountChanged, AddressOf RecordCountChanged
            AddHandler Sync.SyncComplete, AddressOf SyncComplete
            Sync.Sync_tb_PK_SUBJ_G()
        Catch ex As Exception
            If lblStatus_tb_PK_SUBJ_G.Text <> "Stop" Then
                lblStatus_tb_PK_SUBJ_G.Text = "Attention"
                txtAttention.Text = ex.Message
            End If
        End Try
        btn_tb_PK_SUBJ_G.Text = "Start"
        btn_tb_PK_SUBJ_G.Image = My.Resources.Start
    End Sub

    Private Sub TimerAutoSync_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerAutoSync.Tick
        If cbAuto.Checked Then
            If Now.Hour.ToString.PadLeft(2, "0") = txtHH.Text And Now.Minute.ToString.PadLeft(2, "0") = txtMM.Text Then
                btn_tb_PN_DEPARTMENT_R.PerformClick()
                btn_tb_PN_PERSONAL_STAT_R.PerformClick()
                btn_tb_PN_PERSONAL_VIEW.PerformClick()
                btn_tb_PN_POSITION_FLD_R.PerformClick()
                btn_tb_PN_POSITION_MGR_R.PerformClick()
                btn_tb_PN_POSITION_T.PerformClick()
                btn_tb_PN_LEAVE_T.PerformClick()
                btn_tb_PN_POSITION_FLD_FUNC.PerformClick()
                btn_tb_PK_COURSE.PerformClick()
                btn_tb_PK_EMP_SELECT.PerformClick()
                btn_tb_PN_PUNISH_CODE_R.PerformClick()
                btn_tb_PN_PUNISH_T.PerformClick()
                btn_tb_CP_SUBJECT_MASTER.PerformClick()
                btn_tb_CP_SUBJECT_PERSONAL.PerformClick()
                btn_tb_PN_POSITION_HISTORY_T.PerformClick()
                btn_tb_PN_POSITION_RETURN2CLASS.PerformClick()
                btn_tb_PK_SUBJ_G.PerformClick()
            End If
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDbSetting.Click
        Dim f As New SyncData.DbSetting
        f.ShowDialog()
    End Sub


    Private Sub btnStartAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStartAll.Click
        If True Then
            On Error Resume Next
            T_tb_PN_DEPARTMENT_R.Abort()
            T_tb_PN_PERSONAL_STAT_R.Abort()
            T_tb_PN_PERSONAL_VIEW.Abort()
            T_tb_PN_POSITION_FLD_R.Abort()
            T_tb_PN_POSITION_MGR_R.Abort()
            T_tb_PN_POSITION_T.Abort()
            T_tb_PN_LEAVE_T.Abort()
            T_tb_PN_POSITION_FLD_FUNC.Abort()
            T_tb_PK_COURSE.Abort()
            T_tb_PK_EMP_SELECT.Abort()
            T_tb_PN_PUNISH_CODE_R.Abort()
            T_tb_PN_PUNISH_T.Abort()
            T_tb_CP_SUBJECT_MASTER.Abort()
            T_tb_CP_SUBJECT_PERSONAL.Abort()
            T_tb_PN_POSITION_HISTORY_T.Abort()
            T_tb_PN_POSITION_RETURN2CLASS.Abort()
            T_tb_PK_SUBJ_G.Abort()
        End If

        btn_tb_PN_DEPARTMENT_R.PerformClick()
        btn_tb_PN_PERSONAL_STAT_R.PerformClick()
        btn_tb_PN_PERSONAL_VIEW.PerformClick()
        btn_tb_PN_POSITION_FLD_R.PerformClick()
        btn_tb_PN_POSITION_MGR_R.PerformClick()
        btn_tb_PN_POSITION_T.PerformClick()
        btn_tb_PN_LEAVE_T.PerformClick()
        btn_tb_PN_POSITION_FLD_FUNC.PerformClick()
        btn_tb_PK_COURSE.PerformClick()
        btn_tb_PK_EMP_SELECT.PerformClick()
        btn_tb_PN_PUNISH_CODE_R.PerformClick()
        btn_tb_PN_PUNISH_T.PerformClick()
        btn_tb_CP_SUBJECT_MASTER.PerformClick()
        btn_tb_CP_SUBJECT_PERSONAL.PerformClick()
        btn_tb_PN_POSITION_HISTORY_T.PerformClick()
        btn_tb_PN_POSITION_RETURN2CLASS.PerformClick()
        btn_tb_PK_SUBJ_G.PerformClick()
    End Sub




End Class
