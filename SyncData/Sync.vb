﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OracleClient
Imports SyncData.Org.Mentalis.Files
Imports SyncData.DbSetting

Public Class Sync
    'Public OracleConnStr As String = ""
    'Public SqlConnStr As String = ""
    Public INIFileName As String = "C:\Windows\SyncData.ini"  'Parth ที่ใช้เก็บไฟล์ .ini

    Public Event RecordCountChanged(ByVal TableName As String, ByVal RecordCount As Integer)
    Public Event SyncComplete(ByVal TableName As String, ByVal Status As String)
    Public Event SyncAllDataComplete(ByVal Status As String)

    Public Function FixDB(ByVal TXT As String) As String
        If IsDBNull(TXT) = True Then
            Return ""
        ElseIf TXT = Nothing Then
            Return ""
        Else
            Return TXT.ToString.Replace("'", "''")
        End If
    End Function

    Public Function FixDate(ByVal DateTime As Object) As Object
        If IsDate(DateTime) Then
            If DateTime.Year < 1735 Or DateTime.Year > 9999 Then
                Return DBNull.Value
            Else
                Return DateTime
            End If
        End If
        Return DBNull.Value
    End Function

    Public Function GetOracleConnStr() As String
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SyncData"
        Return "Data Source=" & CStr(ini.ReadString("OraSN")) & ";User Id=" & CStr(ini.ReadString("OraUS")) & ";Password=" & CStr(ini.ReadString("OraPW")) & ";"
    End Function

    Public Function GetSqlConnStr() As String
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SyncData"
        Return "Data Source=" & CStr(ini.ReadString("SqlSV")) & ";Initial Catalog=" & CStr(ini.ReadString("SqlDB")) & ";Persist Security Info=True;User ID=" & CStr(ini.ReadString("SqlUS")) & ";Password=" & CStr(ini.ReadString("SqlPW")) & ";"
    End Function


    Public Sub Sync_tb_PN_DEPARTMENT_R()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT PNDP_DEPT_CODE" & vbCrLf
        SQL &= ",PNDP_MINOR_CODE" & vbCrLf
        SQL &= ",PNDP_COST_CENTER_CD" & vbCrLf
        SQL &= ",PNDP_DEPARTMENT_NAME" & vbCrLf
        SQL &= ",PNDP_SECTOR_NAME" & vbCrLf
        SQL &= ",PNDP_CENTER_NAME" & vbCrLf
        SQL &= ",PNDP_DIVISION_NAME" & vbCrLf
        SQL &= ",PNDP_SUBDIVISION_NAME" & vbCrLf
        SQL &= ",PNDP_MINOR_NAME" & vbCrLf
        SQL &= ",PNDP_SHORT_NAME" & vbCrLf
        SQL &= ",RECORD_STATUS" & vbCrLf
        SQL &= ",USER_ID" & vbCrLf
        SQL &= ",USER_DATE" & vbCrLf
        SQL &= ",PNDP_CCID" & vbCrLf
        SQL &= ",DEPT_CODE_NEW" & vbCrLf
        SQL &= ",DEPT_CODE_OLD" & vbCrLf
        SQL &= ",ORG_ID" & vbCrLf
        SQL &= ",DEPT_SHORT_CODE" & vbCrLf
        SQL &= ",DEPT_SHORT_NAME" & vbCrLf
        SQL &= ",DEPT_STATUS" & vbCrLf
        SQL &= "FROM PNMGR.PN_DEPARTMENT_R"

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        SQL = SQL.Replace("PNMGR.PN_DEPARTMENT_R", "tb_PN_DEPARTMENT_R")
        '--------------- Add Update Time --------------
        SQL = SQL.Replace("FROM tb_PN_DEPARTMENT_R", ",Update_Time " & vbCrLf & "FROM tb_PN_DEPARTMENT_R")

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            DT_SQL.DefaultView.RowFilter = "PNDP_DEPT_CODE = '" & FixDB(DT_ORACLE.Rows(i).Item("PNDP_DEPT_CODE").ToString) & "' AND PNDP_MINOR_CODE = '" & FixDB(DT_ORACLE.Rows(i).Item("PNDP_MINOR_CODE").ToString) & "'"
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                DR("PNDP_DEPT_CODE") = DT_ORACLE.Rows(i).Item("PNDP_DEPT_CODE")
                DR("PNDP_MINOR_CODE") = DT_ORACLE.Rows(i).Item("PNDP_MINOR_CODE")
                DR("PNDP_COST_CENTER_CD") = DT_ORACLE.Rows(i).Item("PNDP_COST_CENTER_CD")
                DR("PNDP_DEPARTMENT_NAME") = DT_ORACLE.Rows(i).Item("PNDP_DEPARTMENT_NAME")
                DR("PNDP_SECTOR_NAME") = DT_ORACLE.Rows(i).Item("PNDP_SECTOR_NAME")
                DR("PNDP_CENTER_NAME") = DT_ORACLE.Rows(i).Item("PNDP_CENTER_NAME")
                DR("PNDP_DIVISION_NAME") = DT_ORACLE.Rows(i).Item("PNDP_DIVISION_NAME")
                DR("PNDP_SUBDIVISION_NAME") = DT_ORACLE.Rows(i).Item("PNDP_SUBDIVISION_NAME")
                DR("PNDP_MINOR_NAME") = DT_ORACLE.Rows(i).Item("PNDP_MINOR_NAME")
                DR("PNDP_SHORT_NAME") = DT_ORACLE.Rows(i).Item("PNDP_SHORT_NAME")
                DR("RECORD_STATUS") = DT_ORACLE.Rows(i).Item("RECORD_STATUS")
                DR("USER_ID") = DT_ORACLE.Rows(i).Item("USER_ID")
                DR("USER_DATE") = FixDate(DT_ORACLE.Rows(i).Item("USER_DATE"))
                DR("PNDP_CCID") = DT_ORACLE.Rows(i).Item("PNDP_CCID")
                DR("DEPT_CODE_NEW") = DT_ORACLE.Rows(i).Item("DEPT_CODE_NEW")
                DR("DEPT_CODE_OLD") = DT_ORACLE.Rows(i).Item("DEPT_CODE_OLD")
                DR("ORG_ID") = DT_ORACLE.Rows(i).Item("ORG_ID")
                DR("DEPT_SHORT_CODE") = DT_ORACLE.Rows(i).Item("DEPT_SHORT_CODE")
                DR("DEPT_SHORT_NAME") = DT_ORACLE.Rows(i).Item("DEPT_SHORT_NAME")
                DR("DEPT_STATUS") = DT_ORACLE.Rows(i).Item("DEPT_STATUS")
                DR("Update_Time") = Now

                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView
                For Each DRV In DT_SQL.DefaultView
                    DRV.BeginEdit()
                    DRV.Item("PNDP_DEPT_CODE") = DT_ORACLE.Rows(i).Item("PNDP_DEPT_CODE")
                    DRV.Item("PNDP_MINOR_CODE") = DT_ORACLE.Rows(i).Item("PNDP_MINOR_CODE")
                    DRV.Item("PNDP_COST_CENTER_CD") = DT_ORACLE.Rows(i).Item("PNDP_COST_CENTER_CD")
                    DRV.Item("PNDP_DEPARTMENT_NAME") = DT_ORACLE.Rows(i).Item("PNDP_DEPARTMENT_NAME")
                    DRV.Item("PNDP_SECTOR_NAME") = DT_ORACLE.Rows(i).Item("PNDP_SECTOR_NAME")
                    DRV.Item("PNDP_CENTER_NAME") = DT_ORACLE.Rows(i).Item("PNDP_CENTER_NAME")
                    DRV.Item("PNDP_DIVISION_NAME") = DT_ORACLE.Rows(i).Item("PNDP_DIVISION_NAME")
                    DRV.Item("PNDP_SUBDIVISION_NAME") = DT_ORACLE.Rows(i).Item("PNDP_SUBDIVISION_NAME")
                    DRV.Item("PNDP_MINOR_NAME") = DT_ORACLE.Rows(i).Item("PNDP_MINOR_NAME")
                    DRV.Item("PNDP_SHORT_NAME") = DT_ORACLE.Rows(i).Item("PNDP_SHORT_NAME")
                    DRV.Item("RECORD_STATUS") = DT_ORACLE.Rows(i).Item("RECORD_STATUS")
                    DRV.Item("USER_ID") = DT_ORACLE.Rows(i).Item("USER_ID")
                    DRV.Item("USER_DATE") = FixDate(DT_ORACLE.Rows(i).Item("USER_DATE"))
                    DRV.Item("PNDP_CCID") = DT_ORACLE.Rows(i).Item("PNDP_CCID")
                    DRV.Item("DEPT_CODE_NEW") = DT_ORACLE.Rows(i).Item("DEPT_CODE_NEW")
                    DRV.Item("DEPT_CODE_OLD") = DT_ORACLE.Rows(i).Item("DEPT_CODE_OLD")
                    DRV.Item("ORG_ID") = DT_ORACLE.Rows(i).Item("ORG_ID")
                    DRV.Item("DEPT_SHORT_CODE") = DT_ORACLE.Rows(i).Item("DEPT_SHORT_CODE")
                    DRV.Item("DEPT_SHORT_NAME") = DT_ORACLE.Rows(i).Item("DEPT_SHORT_NAME")
                    DRV.Item("DEPT_STATUS") = DT_ORACLE.Rows(i).Item("DEPT_STATUS")
                    DRV.Item("Update_Time") = Now

                    DRV.EndEdit()
                Next
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_PN_DEPARTMENT_R", i + 1)
        Next
        RaiseEvent SyncComplete("tb_PN_DEPARTMENT_R", "Complete")
    End Sub

    Public Sub Sync_tb_PN_PERSONAL_STAT_R()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT PNES_PSNL_STAT" & vbCrLf
        SQL &= ",PNES_PSNL_DESC" & vbCrLf
        SQL &= "FROM PNMGR.PN_PERSONAL_STAT_R"

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        SQL = SQL.Replace("PNMGR.PN_PERSONAL_STAT_R", "tb_PN_PERSONAL_STAT_R")
        '--------------- Add Update Time --------------
        SQL = SQL.Replace("FROM tb_PN_PERSONAL_STAT_R", ",Update_Time " & vbCrLf & "FROM tb_PN_PERSONAL_STAT_R")

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            DT_SQL.DefaultView.RowFilter = "PNES_PSNL_STAT = '" & FixDB(DT_ORACLE.Rows(i).Item("PNES_PSNL_STAT").ToString) & "'"
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                DR("PNES_PSNL_STAT") = DT_ORACLE.Rows(i).Item("PNES_PSNL_STAT")
                DR("PNES_PSNL_DESC") = DT_ORACLE.Rows(i).Item("PNES_PSNL_DESC")
                DR("Update_Time") = Now
                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView
                For Each DRV In DT_SQL.DefaultView
                    DRV.BeginEdit()
                    DRV.Item("PNES_PSNL_STAT") = DT_ORACLE.Rows(i).Item("PNES_PSNL_STAT")
                    DRV.Item("PNES_PSNL_DESC") = DT_ORACLE.Rows(i).Item("PNES_PSNL_DESC")
                    DRV.Item("Update_Time") = Now
                    DRV.EndEdit()
                Next
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_PN_PERSONAL_STAT_R", i + 1)
        Next
        RaiseEvent SyncComplete("tb_PN_PERSONAL_STAT_R", "Complete")
    End Sub

    Public Sub Sync_tb_PN_PERSONAL_VIEW()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT PNPS_PSNL_NO" & vbCrLf
        SQL &= ",PNPS_TITLE" & vbCrLf
        SQL &= ",PNPS_PSNL_NAME" & vbCrLf
        SQL &= ",PNPS_PSNL_SURNAME" & vbCrLf
        SQL &= ",PNPS_CLASS" & vbCrLf
        SQL &= ",PNPS_PSNL_TYPE" & vbCrLf
        SQL &= ",PNPS_POSITION_NO" & vbCrLf
        SQL &= ",PNPS_PSNL_STAT" & vbCrLf
        SQL &= ",PNPS_RESIGN_STAT" & vbCrLf
        SQL &= ",PNPS_RESIGN_DATE" & vbCrLf
        SQL &= ",PNPS_ENTER_DATE" & vbCrLf
        '-------------- Retire Date Error-----------
        'SQL &= ",PNPS_RETIRE_DATE" & vbCrLf
        SQL &= ",EXTRACT(DAY FROM PNPS_RETIRE_DATE) PNPS_RETIRE_DATE_d" & vbCrLf
        SQL &= ",EXTRACT(MONTH FROM PNPS_RETIRE_DATE) PNPS_RETIRE_DATE_m" & vbCrLf
        SQL &= ",EXTRACT(YEAR FROM PNPS_RETIRE_DATE) PNPS_RETIRE_DATE_y" & vbCrLf
        '-------------- Position Date Error-----------
        SQL &= ",PNPS_POSITION_DATE" & vbCrLf
        SQL &= ",PNPS_CLASS_DATE" & vbCrLf
        SQL &= ",PNPS_LEVEL_DATE" & vbCrLf
        ''-------------- Add 2014-08-31-------------
        'SQL &= ",PNPS_BIRTH_DATE" & vbCrLf
        'SQL &= ",PNPS_EMAIL" & vbCrLf
        'SQL &= ",PNPS_MOBILE_TEL" & vbCrLf
        'SQL &= ",PNPS_TAX_ID" & vbCrLf
        'SQL &= ",PNPS_SAL_WAGE" & vbCrLf
        'SQL &= ",PNPS_RANGE" & vbCrLf
        SQL &= "FROM PNMGR.PERSONAL_VIEW"

        'System.Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-US")

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        '---------- Compile PNPS_RETIRE_DATE-----------
        DT_ORACLE.Columns.Add("PNPS_RETIRE_DATE", GetType(Date))

        For i As Integer = 0 To DT_ORACLE.Rows.Count - 1
            Try
                Dim rdate As New Date(DT_ORACLE.Rows(i).Item("PNPS_RETIRE_DATE_y"), DT_ORACLE.Rows(i).Item("PNPS_RETIRE_DATE_m"), DT_ORACLE.Rows(i).Item("PNPS_RETIRE_DATE_d"))
                DT_ORACLE.Rows(i).Item("PNPS_RETIRE_DATE") = rdate
            Catch ex As Exception
            End Try
        Next
        DT_ORACLE.Columns.Remove("PNPS_RETIRE_DATE_d")
        DT_ORACLE.Columns.Remove("PNPS_RETIRE_DATE_m")
        DT_ORACLE.Columns.Remove("PNPS_RETIRE_DATE_y")


        'SQL = SQL.Replace("PNMGR.PERSONAL_VIEW", "tb_PN_PERSONAL_VIEW")
        ''--------------- Add Update Time --------------
        'SQL = SQL.Replace("FROM tb_PN_PERSONAL_VIEW", ",Update_Time " & vbCrLf & "FROM tb_PN_PERSONAL_VIEW")
        SQL = " SELECT PNPS_PSNL_NO" & vbCrLf
        SQL &= ",PNPS_TITLE" & vbCrLf
        SQL &= ",PNPS_PSNL_NAME" & vbCrLf
        SQL &= ",PNPS_PSNL_SURNAME" & vbCrLf
        SQL &= ",PNPS_CLASS" & vbCrLf
        SQL &= ",PNPS_PSNL_TYPE" & vbCrLf
        SQL &= ",PNPS_POSITION_NO" & vbCrLf
        SQL &= ",PNPS_PSNL_STAT" & vbCrLf
        SQL &= ",PNPS_RESIGN_STAT" & vbCrLf
        SQL &= ",PNPS_RESIGN_DATE" & vbCrLf
        SQL &= ",PNPS_ENTER_DATE" & vbCrLf
        SQL &= ",PNPS_RETIRE_DATE" & vbCrLf
        SQL &= ",PNPS_POSITION_DATE" & vbCrLf
        SQL &= ",PNPS_CLASS_DATE" & vbCrLf
        SQL &= ",PNPS_LEVEL_DATE" & vbCrLf
        SQL &= ",Update_Time" & vbCrLf
        SQL &= "FROM tb_PN_PERSONAL_VIEW"

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            DT_SQL.DefaultView.RowFilter = "PNPS_PSNL_NO = '" & FixDB(DT_ORACLE.Rows(i).Item("PNPS_PSNL_NO").ToString) & "'"
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                DR("PNPS_PSNL_NO") = DT_ORACLE.Rows(i).Item("PNPS_PSNL_NO")
                DR("PNPS_TITLE") = DT_ORACLE.Rows(i).Item("PNPS_TITLE")
                DR("PNPS_PSNL_NAME") = DT_ORACLE.Rows(i).Item("PNPS_PSNL_NAME")
                DR("PNPS_PSNL_SURNAME") = DT_ORACLE.Rows(i).Item("PNPS_PSNL_SURNAME")
                DR("PNPS_CLASS") = DT_ORACLE.Rows(i).Item("PNPS_CLASS")
                DR("PNPS_PSNL_TYPE") = DT_ORACLE.Rows(i).Item("PNPS_PSNL_TYPE")
                DR("PNPS_POSITION_NO") = DT_ORACLE.Rows(i).Item("PNPS_POSITION_NO")
                DR("PNPS_PSNL_STAT") = DT_ORACLE.Rows(i).Item("PNPS_PSNL_STAT")
                DR("PNPS_RESIGN_STAT") = DT_ORACLE.Rows(i).Item("PNPS_RESIGN_STAT")
                DR("PNPS_RESIGN_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPS_RESIGN_DATE"))
                DR("PNPS_ENTER_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPS_ENTER_DATE"))
                DR("PNPS_RETIRE_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPS_RETIRE_DATE"))
                DR("PNPS_POSITION_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPS_POSITION_DATE"))
                DR("PNPS_CLASS_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPS_CLASS_DATE"))
                DR("PNPS_LEVEL_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPS_LEVEL_DATE"))
                ''-------------- Add 2014-08-31-------------
                'DR("PNPS_BIRTH_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPS_BIRTH_DATE"))
                'If Not IsDBNull(DT_ORACLE.Rows(i).Item("PNPS_EMAIL")) Then DR("PNPS_EMAIL") = DT_ORACLE.Rows(i).Item("PNPS_EMAIL")
                'If Not IsDBNull(DT_ORACLE.Rows(i).Item("PNPS_MOBILE_TEL")) Then DR("PNPS_MOBILE_TEL") = DT_ORACLE.Rows(i).Item("PNPS_MOBILE_TEL")
                'If Not IsDBNull(DT_ORACLE.Rows(i).Item("PNPS_TAX_ID")) Then DR("PNPS_TAX_ID") = DT_ORACLE.Rows(i).Item("PNPS_TAX_ID")
                'If Not IsDBNull(DT_ORACLE.Rows(i).Item("PNPS_SAL_WAGE")) Then DR("PNPS_SAL_WAGE") = DT_ORACLE.Rows(i).Item("PNPS_SAL_WAGE")
                'If Not IsDBNull(DT_ORACLE.Rows(i).Item("PNPS_RANGE")) Then DR("PNPS_RANGE") = DT_ORACLE.Rows(i).Item("PNPS_RANGE")

                DR("Update_Time") = Now
                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView
                For Each DRV In DT_SQL.DefaultView
                    DRV.BeginEdit()
                    DRV.Item("PNPS_PSNL_NO") = DT_ORACLE.Rows(i).Item("PNPS_PSNL_NO")
                    DRV.Item("PNPS_TITLE") = DT_ORACLE.Rows(i).Item("PNPS_TITLE")
                    DRV.Item("PNPS_PSNL_NAME") = DT_ORACLE.Rows(i).Item("PNPS_PSNL_NAME")
                    DRV.Item("PNPS_PSNL_SURNAME") = DT_ORACLE.Rows(i).Item("PNPS_PSNL_SURNAME")
                    DRV.Item("PNPS_CLASS") = DT_ORACLE.Rows(i).Item("PNPS_CLASS")
                    DRV.Item("PNPS_PSNL_TYPE") = DT_ORACLE.Rows(i).Item("PNPS_PSNL_TYPE")
                    DRV.Item("PNPS_POSITION_NO") = DT_ORACLE.Rows(i).Item("PNPS_POSITION_NO")
                    DRV.Item("PNPS_PSNL_STAT") = DT_ORACLE.Rows(i).Item("PNPS_PSNL_STAT")
                    DRV.Item("PNPS_RESIGN_STAT") = DT_ORACLE.Rows(i).Item("PNPS_RESIGN_STAT")
                    DRV.Item("PNPS_RESIGN_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPS_RESIGN_DATE"))
                    DRV.Item("PNPS_ENTER_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPS_ENTER_DATE"))
                    DRV.Item("PNPS_RETIRE_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPS_RETIRE_DATE"))
                    DRV.Item("PNPS_POSITION_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPS_POSITION_DATE"))
                    DRV.Item("PNPS_CLASS_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPS_CLASS_DATE"))
                    DRV.Item("PNPS_LEVEL_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPS_LEVEL_DATE"))
                    ''-------------- Add 2014-08-31-------------
                    'DRV("PNPS_BIRTH_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPS_BIRTH_DATE"))
                    'If Not IsDBNull(DT_ORACLE.Rows(i).Item("PNPS_EMAIL")) Then
                    '    DRV("PNPS_EMAIL") = DT_ORACLE.Rows(i).Item("PNPS_EMAIL")
                    'Else
                    '    DRV("PNPS_EMAIL") = DBNull.Value
                    'End If
                    'If Not IsDBNull(DT_ORACLE.Rows(i).Item("PNPS_MOBILE_TEL")) Then
                    '    DRV("PNPS_MOBILE_TEL") = DT_ORACLE.Rows(i).Item("PNPS_MOBILE_TEL")
                    'Else
                    '    DRV("PNPS_MOBILE_TEL") = DBNull.Value
                    'End If
                    'If Not IsDBNull(DT_ORACLE.Rows(i).Item("PNPS_TAX_ID")) Then
                    '    DRV("PNPS_TAX_ID") = DT_ORACLE.Rows(i).Item("PNPS_TAX_ID")
                    'Else
                    '    DRV("PNPS_TAX_ID") = DBNull.Value
                    'End If
                    'If Not IsDBNull(DT_ORACLE.Rows(i).Item("PNPS_SAL_WAGE")) Then
                    '    DRV("PNPS_SAL_WAGE") = DT_ORACLE.Rows(i).Item("PNPS_SAL_WAGE")
                    'Else
                    '    DRV("PNPS_SAL_WAGE") = DBNull.Value
                    'End If
                    'If Not IsDBNull(DT_ORACLE.Rows(i).Item("PNPS_RANGE")) Then
                    '    DRV("PNPS_RANGE") = DT_ORACLE.Rows(i).Item("PNPS_RANGE")
                    'Else
                    '    DRV("PNPS_RANGE") = DBNull.Value
                    'End If
                    DRV.Item("Update_Time") = Now
                    DRV.EndEdit()
                Next
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_PN_PERSONAL_VIEW", i + 1)
        Next

        '-------------- Add New HR_User ------------
        Dim Conn As New SqlConnection(GetSqlConnStr)
        Try
            Conn.Open()
            Dim Comm As New SqlCommand
            With Comm
                .CommandType = CommandType.Text
                .Connection = Conn
                .CommandText = "EXEC dbo.sp_Syn_New_User"
                .ExecuteNonQuery()
                .Dispose()
            End With
        Catch : End Try
        Try : Conn.Close() : Catch : End Try

        RaiseEvent SyncComplete("tb_PN_PERSONAL_VIEW", "Complete")
    End Sub

    Public Sub Sync_tb_PN_POSITION_FLD_R()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT PNFF_POS_TYPE" & vbCrLf
        SQL &= ",PNFF_POS_FLD_CD" & vbCrLf
        SQL &= ",PNFF_POS_NAME" & vbCrLf
        SQL &= ",PNFF_POS_SHRT" & vbCrLf
        SQL &= "FROM PNMGR.PN_POSITION_FLD_R"

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        SQL = SQL.Replace("PNMGR.PN_POSITION_FLD_R", "tb_PN_POSITION_FLD_R")
        '--------------- Add Update Time --------------
        SQL = SQL.Replace("FROM tb_PN_POSITION_FLD_R", ",Update_Time " & vbCrLf & "FROM tb_PN_POSITION_FLD_R")

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            DT_SQL.DefaultView.RowFilter = "PNFF_POS_TYPE = '" & FixDB(DT_ORACLE.Rows(i).Item("PNFF_POS_TYPE").ToString) & "' AND PNFF_POS_FLD_CD = '" & FixDB(DT_ORACLE.Rows(i).Item("PNFF_POS_FLD_CD").ToString) & "'"
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                DR("PNFF_POS_TYPE") = DT_ORACLE.Rows(i).Item("PNFF_POS_TYPE")
                DR("PNFF_POS_FLD_CD") = DT_ORACLE.Rows(i).Item("PNFF_POS_FLD_CD")
                DR("PNFF_POS_NAME") = DT_ORACLE.Rows(i).Item("PNFF_POS_NAME")
                DR("PNFF_POS_SHRT") = DT_ORACLE.Rows(i).Item("PNFF_POS_SHRT")
                DR("Update_Time") = Now
                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView
                For Each DRV In DT_SQL.DefaultView
                    DRV.BeginEdit()
                    DRV.Item("PNFF_POS_TYPE") = DT_ORACLE.Rows(i).Item("PNFF_POS_TYPE")
                    DRV.Item("PNFF_POS_FLD_CD") = DT_ORACLE.Rows(i).Item("PNFF_POS_FLD_CD")
                    DRV.Item("PNFF_POS_NAME") = DT_ORACLE.Rows(i).Item("PNFF_POS_NAME")
                    DRV.Item("PNFF_POS_SHRT") = DT_ORACLE.Rows(i).Item("PNFF_POS_SHRT")
                    DRV.Item("Update_Time") = Now
                    DRV.EndEdit()
                Next
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_PN_POSITION_FLD_R", i + 1)
        Next
        RaiseEvent SyncComplete("tb_PN_POSITION_FLD_R", "Complete")
    End Sub

    Public Sub Sync_tb_PN_POSITION_MGR_R()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT PNPM_POS_MGR_CD" & vbCrLf
        SQL &= ",PNPM_POS_NAME" & vbCrLf
        SQL &= ",PNPM_POS_SHRT" & vbCrLf
        SQL &= "FROM PNMGR.PN_POSITION_MGR_R"

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        SQL = SQL.Replace("PNMGR.PN_POSITION_MGR_R", "tb_PN_POSITION_MGR_R")
        '--------------- Add Update Time --------------
        SQL = SQL.Replace("FROM tb_PN_POSITION_MGR_R", ",Update_Time " & vbCrLf & "FROM tb_PN_POSITION_MGR_R")

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            DT_SQL.DefaultView.RowFilter = "PNPM_POS_MGR_CD = '" & FixDB(DT_ORACLE.Rows(i).Item("PNPM_POS_MGR_CD").ToString) & "'"
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                DR("PNPM_POS_MGR_CD") = DT_ORACLE.Rows(i).Item("PNPM_POS_MGR_CD")
                DR("PNPM_POS_NAME") = DT_ORACLE.Rows(i).Item("PNPM_POS_NAME")
                DR("PNPM_POS_SHRT") = DT_ORACLE.Rows(i).Item("PNPM_POS_SHRT")
                DR("Update_Time") = Now

                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView
                For Each DRV In DT_SQL.DefaultView
                    DRV.BeginEdit()
                    DRV.Item("PNPM_POS_MGR_CD") = DT_ORACLE.Rows(i).Item("PNPM_POS_MGR_CD")
                    DRV.Item("PNPM_POS_NAME") = DT_ORACLE.Rows(i).Item("PNPM_POS_NAME")
                    DRV.Item("PNPM_POS_SHRT") = DT_ORACLE.Rows(i).Item("PNPM_POS_SHRT")
                    DRV.Item("Update_Time") = Now
                    DRV.EndEdit()
                Next
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_PN_POSITION_MGR_R", i + 1)
        Next
        RaiseEvent SyncComplete("tb_PN_POSITION_MGR_R", "Complete")
    End Sub

    Public Sub Sync_tb_PN_POSITION_T()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT PNPO_POS_NO" & vbCrLf
        SQL &= ",PNPO_PSNL_NO" & vbCrLf
        SQL &= ",PNPO_DEPT_CD" & vbCrLf
        SQL &= ",PNPO_MINOR_CD" & vbCrLf
        SQL &= ",PNPO_POS_FLD_CD" & vbCrLf
        SQL &= ",PNPO_POS_MGR_CD" & vbCrLf
        SQL &= ",PNPO_POS_EXP_CD" & vbCrLf
        SQL &= ",PNPO_PROV_CODE" & vbCrLf
        SQL &= ",PNPO_SUBPROV_CODE" & vbCrLf
        SQL &= ",PNPO_LEVEL" & vbCrLf
        SQL &= ",PNPO_CHANGE_DATE" & vbCrLf
        SQL &= ",PNPO_SINCE_DATE" & vbCrLf
        SQL &= ",PNPO_PSNL_TYPE" & vbCrLf
        SQL &= ",PNPO_CLASS" & vbCrLf
        SQL &= ",PNPO_POS_LEVEL" & vbCrLf
        SQL &= ",PNPO_RECEIVE_DATE" & vbCrLf
        SQL &= ",PNPO_RANGE" & vbCrLf
        SQL &= ",PNPO_POS_STAT" & vbCrLf
        SQL &= ",RECORD_STATUS" & vbCrLf
        SQL &= ",USER_ID" & vbCrLf
        SQL &= ",USER_DATE" & vbCrLf
        SQL &= ",PNPO_PRODLINE_CD" & vbCrLf
        SQL &= ",PNPO_LEVEL1" & vbCrLf
        SQL &= ",PNPO_LEVEL2" & vbCrLf
        SQL &= ",PNPO_CLASS1" & vbCrLf
        SQL &= ",PNPO_CLASS2" & vbCrLf
        SQL &= ",PNPO_TYPE" & vbCrLf
        SQL &= ",PNPO_FLD_RANGE" & vbCrLf
        SQL &= ",PNPO_TEL_NO" & vbCrLf
        SQL &= ",CIGARETTE_AMT" & vbCrLf
        SQL &= ",PNPO_PA_FORM" & vbCrLf
        SQL &= ",CIGA_CD" & vbCrLf
        SQL &= ",PNPO_EQUAL_CLASS" & vbCrLf
        SQL &= ",PNPO_FLD_FUNC" & vbCrLf
        SQL &= "FROM PNMGR.POSITION_VIEW"

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        SQL = SQL.Replace("PNMGR.POSITION_VIEW", "tb_PN_POSITION_T")
        '--------------- Add Update Time --------------
        SQL = SQL.Replace("FROM tb_PN_POSITION_T", ",Update_Time " & vbCrLf & "FROM tb_PN_POSITION_T")

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            'DT_SQL.DefaultView.RowFilter = "PNPO_POS_NO = '" & FixDB(DT_ORACLE.Rows(i).Item("PNPO_POS_NO").ToString) & "' AND PNPO_DEPT_CD = '" & FixDB(DT_ORACLE.Rows(i).Item("PNPO_DEPT_CD").ToString) & "' AND PNPO_MINOR_CD = '" & FixDB(DT_ORACLE.Rows(i).Item("PNPO_MINOR_CD").ToString) & "'"
            DT_SQL.DefaultView.RowFilter = "PNPO_POS_NO = '" & FixDB(DT_ORACLE.Rows(i).Item("PNPO_POS_NO").ToString) & "'"

            If DT_SQL.DefaultView.Count > 1 Then
                SQL = " DELETE FROM tb_PN_POSITION_T WHERE PNPO_POS_NO='" & FixDB(DT_ORACLE.Rows(i).Item("PNPO_POS_NO").ToString) & "'"
                Dim DA_SQL_Del As New SqlDataAdapter(SQL, GetSqlConnStr)
                Dim DT_SQL_Del As New DataTable
                DA_SQL_Del.Fill(DT_SQL_Del)

                DT_SQL.DefaultView.RowFilter = "0=1"

            End If


            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                DR("PNPO_POS_NO") = DT_ORACLE.Rows(i).Item("PNPO_POS_NO")
                DR("PNPO_PSNL_NO") = DT_ORACLE.Rows(i).Item("PNPO_PSNL_NO")
                DR("PNPO_DEPT_CD") = DT_ORACLE.Rows(i).Item("PNPO_DEPT_CD")
                DR("PNPO_MINOR_CD") = DT_ORACLE.Rows(i).Item("PNPO_MINOR_CD")
                DR("PNPO_POS_FLD_CD") = DT_ORACLE.Rows(i).Item("PNPO_POS_FLD_CD")
                DR("PNPO_POS_MGR_CD") = DT_ORACLE.Rows(i).Item("PNPO_POS_MGR_CD")
                DR("PNPO_POS_EXP_CD") = DT_ORACLE.Rows(i).Item("PNPO_POS_EXP_CD")
                DR("PNPO_PROV_CODE") = DT_ORACLE.Rows(i).Item("PNPO_PROV_CODE")
                DR("PNPO_SUBPROV_CODE") = DT_ORACLE.Rows(i).Item("PNPO_SUBPROV_CODE")
                DR("PNPO_LEVEL") = DT_ORACLE.Rows(i).Item("PNPO_LEVEL")
                DR("PNPO_CHANGE_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPO_CHANGE_DATE"))
                DR("PNPO_SINCE_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPO_SINCE_DATE"))
                DR("PNPO_PSNL_TYPE") = DT_ORACLE.Rows(i).Item("PNPO_PSNL_TYPE")
                DR("PNPO_CLASS") = DT_ORACLE.Rows(i).Item("PNPO_CLASS")
                DR("PNPO_POS_LEVEL") = DT_ORACLE.Rows(i).Item("PNPO_POS_LEVEL")
                DR("PNPO_RECEIVE_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPO_RECEIVE_DATE"))
                DR("PNPO_RANGE") = DT_ORACLE.Rows(i).Item("PNPO_RANGE")
                DR("PNPO_POS_STAT") = DT_ORACLE.Rows(i).Item("PNPO_POS_STAT")
                DR("RECORD_STATUS") = DT_ORACLE.Rows(i).Item("RECORD_STATUS")
                DR("USER_ID") = DT_ORACLE.Rows(i).Item("USER_ID")
                DR("USER_DATE") = FixDate(DT_ORACLE.Rows(i).Item("USER_DATE"))
                DR("PNPO_PRODLINE_CD") = DT_ORACLE.Rows(i).Item("PNPO_PRODLINE_CD")
                DR("PNPO_LEVEL1") = DT_ORACLE.Rows(i).Item("PNPO_LEVEL1")
                DR("PNPO_LEVEL2") = DT_ORACLE.Rows(i).Item("PNPO_LEVEL2")
                DR("PNPO_CLASS1") = DT_ORACLE.Rows(i).Item("PNPO_CLASS1")
                DR("PNPO_CLASS2") = DT_ORACLE.Rows(i).Item("PNPO_CLASS2")
                DR("PNPO_TYPE") = DT_ORACLE.Rows(i).Item("PNPO_TYPE")
                DR("PNPO_FLD_RANGE") = DT_ORACLE.Rows(i).Item("PNPO_FLD_RANGE")
                DR("PNPO_TEL_NO") = DT_ORACLE.Rows(i).Item("PNPO_TEL_NO")
                DR("CIGARETTE_AMT") = DT_ORACLE.Rows(i).Item("CIGARETTE_AMT")
                DR("PNPO_PA_FORM") = DT_ORACLE.Rows(i).Item("PNPO_PA_FORM")
                DR("CIGA_CD") = DT_ORACLE.Rows(i).Item("CIGA_CD")
                DR("PNPO_EQUAL_CLASS") = DT_ORACLE.Rows(i).Item("PNPO_EQUAL_CLASS")
                DR("PNPO_FLD_FUNC") = DT_ORACLE.Rows(i).Item("PNPO_FLD_FUNC")
                DR("Update_Time") = Now
                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else

             


                '--------- EDIT ---------
                Dim DRV As DataRowView
                For Each DRV In DT_SQL.DefaultView
                    DRV.BeginEdit()
                    DRV.Item("PNPO_POS_NO") = DT_ORACLE.Rows(i).Item("PNPO_POS_NO")
                    DRV.Item("PNPO_PSNL_NO") = DT_ORACLE.Rows(i).Item("PNPO_PSNL_NO")
                    DRV.Item("PNPO_DEPT_CD") = DT_ORACLE.Rows(i).Item("PNPO_DEPT_CD")
                    DRV.Item("PNPO_MINOR_CD") = DT_ORACLE.Rows(i).Item("PNPO_MINOR_CD")
                    DRV.Item("PNPO_POS_FLD_CD") = DT_ORACLE.Rows(i).Item("PNPO_POS_FLD_CD")
                    DRV.Item("PNPO_POS_MGR_CD") = DT_ORACLE.Rows(i).Item("PNPO_POS_MGR_CD")
                    DRV.Item("PNPO_POS_EXP_CD") = DT_ORACLE.Rows(i).Item("PNPO_POS_EXP_CD")
                    DRV.Item("PNPO_PROV_CODE") = DT_ORACLE.Rows(i).Item("PNPO_PROV_CODE")
                    DRV.Item("PNPO_SUBPROV_CODE") = DT_ORACLE.Rows(i).Item("PNPO_SUBPROV_CODE")
                    DRV.Item("PNPO_LEVEL") = DT_ORACLE.Rows(i).Item("PNPO_LEVEL")
                    DRV.Item("PNPO_CHANGE_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPO_CHANGE_DATE"))
                    DRV.Item("PNPO_SINCE_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPO_SINCE_DATE"))
                    DRV.Item("PNPO_PSNL_TYPE") = DT_ORACLE.Rows(i).Item("PNPO_PSNL_TYPE")
                    DRV.Item("PNPO_CLASS") = DT_ORACLE.Rows(i).Item("PNPO_CLASS")
                    DRV.Item("PNPO_POS_LEVEL") = DT_ORACLE.Rows(i).Item("PNPO_POS_LEVEL")
                    DRV.Item("PNPO_RECEIVE_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNPO_RECEIVE_DATE"))
                    DRV.Item("PNPO_RANGE") = DT_ORACLE.Rows(i).Item("PNPO_RANGE")
                    DRV.Item("PNPO_POS_STAT") = DT_ORACLE.Rows(i).Item("PNPO_POS_STAT")
                    DRV.Item("RECORD_STATUS") = DT_ORACLE.Rows(i).Item("RECORD_STATUS")
                    DRV.Item("USER_ID") = DT_ORACLE.Rows(i).Item("USER_ID")
                    DRV.Item("USER_DATE") = FixDate(DT_ORACLE.Rows(i).Item("USER_DATE"))
                    DRV.Item("PNPO_PRODLINE_CD") = DT_ORACLE.Rows(i).Item("PNPO_PRODLINE_CD")
                    DRV.Item("PNPO_LEVEL1") = DT_ORACLE.Rows(i).Item("PNPO_LEVEL1")
                    DRV.Item("PNPO_LEVEL2") = DT_ORACLE.Rows(i).Item("PNPO_LEVEL2")
                    DRV.Item("PNPO_CLASS1") = DT_ORACLE.Rows(i).Item("PNPO_CLASS1")
                    DRV.Item("PNPO_CLASS2") = DT_ORACLE.Rows(i).Item("PNPO_CLASS2")
                    DRV.Item("PNPO_TYPE") = DT_ORACLE.Rows(i).Item("PNPO_TYPE")
                    DRV.Item("PNPO_FLD_RANGE") = DT_ORACLE.Rows(i).Item("PNPO_FLD_RANGE")
                    DRV.Item("PNPO_TEL_NO") = DT_ORACLE.Rows(i).Item("PNPO_TEL_NO")
                    DRV.Item("CIGARETTE_AMT") = DT_ORACLE.Rows(i).Item("CIGARETTE_AMT")
                    DRV.Item("PNPO_PA_FORM") = DT_ORACLE.Rows(i).Item("PNPO_PA_FORM")
                    DRV.Item("CIGA_CD") = DT_ORACLE.Rows(i).Item("CIGA_CD")
                    DRV.Item("PNPO_EQUAL_CLASS") = DT_ORACLE.Rows(i).Item("PNPO_EQUAL_CLASS")
                    DRV.Item("PNPO_FLD_FUNC") = DT_ORACLE.Rows(i).Item("PNPO_FLD_FUNC")
                    DRV.Item("Update_Time") = Now
                    DRV.EndEdit()
                Next
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_PN_POSITION_T", i + 1)
        Next
        RaiseEvent SyncComplete("tb_PN_POSITION_T", "Complete")
    End Sub

    Public Sub Sync_tb_PN_LEAVE_T()

        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        '----------- Sync Type R ---------------
        SQL = "SELECT * FROM PNMGR.PN_LEAVE_TYPE_R"
        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        SQL = "SELECT * FROM tb_PN_LEAVE_TYPE_R"
        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)
        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            DT_SQL.DefaultView.RowFilter = "PNLT_TYPE = " & DT_ORACLE.Rows(i).Item("PNLT_TYPE")
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                For j As Integer = 0 To DT_ORACLE.Columns.Count - 1
                    Dim ColName As String = DT_ORACLE.Columns(j).ColumnName
                    If Not IsDBNull(DT_ORACLE.Rows(i).Item(ColName)) Then
                        DR(ColName) = DT_ORACLE.Rows(i).Item(ColName)
                    Else
                        DR(ColName) = DBNull.Value
                    End If
                Next
                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView = DT_SQL.DefaultView(0)
                DRV.BeginEdit()
                For j As Integer = 1 To DT_ORACLE.Columns.Count - 1
                    Dim ColName As String = DT_ORACLE.Columns(j).ColumnName
                    If Not IsDBNull(DT_ORACLE.Rows(i).Item(ColName)) Then
                        DRV(ColName) = DT_ORACLE.Rows(i).Item(ColName)
                    Else
                        DRV(ColName) = DBNull.Value
                    End If
                Next
                DRV.EndEdit()
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
        Next

        ''------------- หาวันที่ Update ล่าสุด -------------
        'SQL = "SELECT MAX(USER_DATE) USER_DATE FROM tb_PN_LEAVE_T"
        'DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        'DT_SQL = New DataTable
        'DA_SQL.Fill(DT_SQL)
        'Dim Oracle_WHERE As String = ""
        'If DT_SQL.Rows.Count > 0 AndAlso Not IsDBNull(DT_SQL.Rows(0).Item("USER_DATE")) Then
        '    Dim USER_DATE As DateTime = DT_SQL.Rows(0).Item("USER_DATE")
        '    Oracle_WHERE &= " USER_DATE> TO_DATE('" & USER_DATE.Day.ToString.PadLeft(2, "0") & "-" & USER_DATE.Month.ToString.PadLeft(2, "0") & "-" & USER_DATE.Year & "','DD-MM-YYYY') AND "
        'End If
        DA_SQL = New SqlDataAdapter("Truncate Table tb_PN_LEAVE_T", GetSqlConnStr)
        DA_SQL.Fill(DT_SQL)

        '----------- Sync Leave--------------

        SQL = "SELECT" & vbLf
        SQL &= "PNLL_PSNL_NO," & vbLf
        SQL &= "PNLL_YYMM," & vbLf
        SQL &= "PNLL_LEAVE_TYPE," & vbLf
        SQL &= "PNLL_DATE," & vbLf
        SQL &= "PNLL_FLAG," & vbLf
        SQL &= "RECORD_STATUS," & vbLf
        SQL &= "USER_ID," & vbLf
        SQL &= "USER_DATE," & vbLf
        SQL &= "PNLL_LOCK " & vbLf
        SQL &= "FROM PNMGR.PN_LEAVE_T " & vbLf
        SQL &= "WHERE LENGTH(PNLL_YYMM)=4 AND PNLL_YYMM>='5600'" & vbLf

        'If Oracle_WHERE <> "" Then
        '    SQL &= " WHERE " & Oracle_WHERE.Substring(0, Oracle_WHERE.Length - 4) & vbLf
        'End If

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        'SQL = SQL.Replace("PNMGR.PN_LEAVE_T", "tb_PN_LEAVE_T").Replace("PNLL_LOCK " & vbLf, "PNLL_LOCK, " & vbLf & "Update_Time" & vbLf)

        SQL = "SELECT "
        SQL &= "PNLL_PSNL_NO," & vbLf
        SQL &= "PNLL_YYMM," & vbLf
        SQL &= "PNLL_LEAVE_TYPE," & vbLf
        SQL &= "PNLL_DATE," & vbLf
        SQL &= "PNLL_FLAG," & vbLf
        SQL &= "RECORD_STATUS," & vbLf
        SQL &= "USER_ID," & vbLf
        SQL &= "USER_DATE," & vbLf
        SQL &= "PNLL_LOCK," & vbLf
        SQL &= "Update_Time" & vbLf
        SQL &= "FROM tb_PN_LEAVE_T"
        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)
        Dim DT_Search As DataTable = DT_SQL.Copy

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            'DT_SQL.DefaultView.RowFilter = "PNLL_PSNL_NO = '" & FixDB(DT_ORACLE.Rows(i).Item("PNLL_PSNL_NO").ToString) & "' AND PNLL_YYMM = '" & FixDB(DT_ORACLE.Rows(i).Item("PNLL_YYMM").ToString) & "' AND PNLL_LEAVE_TYPE = '" & FixDB(DT_ORACLE.Rows(i).Item("PNLL_LEAVE_TYPE").ToString) & "'"
            DT_Search.DefaultView.RowFilter = "PNLL_PSNL_NO = '" & FixDB(DT_ORACLE.Rows(i).Item("PNLL_PSNL_NO").ToString) & "' AND PNLL_YYMM = '" & FixDB(DT_ORACLE.Rows(i).Item("PNLL_YYMM").ToString) & "' AND PNLL_LEAVE_TYPE = '" & FixDB(DT_ORACLE.Rows(i).Item("PNLL_LEAVE_TYPE").ToString) & "'"
            If DT_Search.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                For j As Integer = 0 To DT_ORACLE.Columns.Count - 1
                    Dim ColName As String = DT_ORACLE.Columns(j).ColumnName
                    If Not IsDBNull(DT_ORACLE.Rows(i).Item(ColName)) Then
                        DR(ColName) = DT_ORACLE.Rows(i).Item(ColName)
                    Else
                        DR(ColName) = DBNull.Value
                    End If
                Next
                DR("Update_Time") = Now
                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
                DT_SQL.Rows(DT_SQL.Rows.Count - 1).Delete()
                DT_SQL.Rows(DT_SQL.Rows.Count - 1).AcceptChanges()
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView = DT_SQL.DefaultView(0)
                DRV.BeginEdit()
                For j As Integer = 3 To DT_ORACLE.Columns.Count - 1
                    Dim ColName As String = DT_ORACLE.Columns(j).ColumnName
                    If Not IsDBNull(DT_ORACLE.Rows(i).Item(ColName)) Then
                        DRV(ColName) = DT_ORACLE.Rows(i).Item(ColName)
                    Else
                        DRV(ColName) = DBNull.Value
                    End If
                Next
                DRV("Update_Time") = Now
                DRV.EndEdit()
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
                '----------- Remove From Search -----------
                DT_Search.DefaultView(0).Row.Delete()
                DT_Search.AcceptChanges()
            End If
            'DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_PN_LEAVE_T", i + 1)
        Next
        RaiseEvent SyncComplete("tb_PN_LEAVE_T", "Complete")
    End Sub

    Public Sub Sync_tb_PN_POSITION_FLD_FUNC()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT PNFF_CODE" & vbCrLf
        SQL &= ",PNFF_FLD_DESC" & vbCrLf
        SQL &= "FROM PNMGR.PN_POSITION_FLD_FUNC"

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        SQL = SQL.Replace("PNMGR.PN_POSITION_FLD_FUNC", "tb_PN_POSITION_FLD_FUNC")
        '--------------- Add Update Time --------------
        SQL = SQL.Replace("FROM tb_PN_POSITION_FLD_FUNC", ",Update_Time " & vbCrLf & "FROM tb_PN_POSITION_FLD_FUNC")

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            DT_SQL.DefaultView.RowFilter = "PNFF_CODE = '" & FixDB(DT_ORACLE.Rows(i).Item("PNFF_CODE").ToString) & "'"
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                DR("PNFF_CODE") = DT_ORACLE.Rows(i).Item("PNFF_CODE")
                DR("PNFF_FLD_DESC") = DT_ORACLE.Rows(i).Item("PNFF_FLD_DESC")
                DR("Update_Time") = Now

                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView
                For Each DRV In DT_SQL.DefaultView
                    DRV.BeginEdit()
                    DRV.Item("PNFF_CODE") = DT_ORACLE.Rows(i).Item("PNFF_CODE")
                    DRV.Item("PNFF_FLD_DESC") = DT_ORACLE.Rows(i).Item("PNFF_FLD_DESC")
                    DRV.Item("Update_Time") = Now
                    DRV.EndEdit()
                Next
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_PN_POSITION_FLD_FUNC", i + 1)
        Next
        RaiseEvent SyncComplete("tb_PN_POSITION_FLD_FUNC", "Complete")
    End Sub

    Public Sub Sync_tb_PK_COURSE()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT  COURSE_ID" & vbCrLf
        SQL &= ", COURSE_DESC" & vbCrLf
        SQL &= ", SUBJ_G_ID" & vbCrLf
        SQL &= ", TR_TYPE_ID" & vbCrLf
        SQL &= ", TEST_FLAG" & vbCrLf
        SQL &= ", PASS_PCT" & vbCrLf
        SQL &= ", TEST_SCORE" & vbCrLf
        SQL &= ", TEST_PASS_PCT" & vbCrLf
        SQL &= ", UPD_BY" & vbCrLf
        SQL &= ", UPD_DATE" & vbCrLf
        SQL &= "FROM PNMGR.PK_COURSE"

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        SQL = SQL.Replace("PNMGR.PK_COURSE", "tb_PK_COURSE")
        '--------------- Add Update Time --------------
        SQL = SQL.Replace("FROM tb_PK_COURSE", ",Update_Time " & vbCrLf & "FROM tb_PK_COURSE")

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            DT_SQL.DefaultView.RowFilter = "COURSE_ID = '" & FixDB(DT_ORACLE.Rows(i).Item("COURSE_ID").ToString) & "'"
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                DR("COURSE_ID") = DT_ORACLE.Rows(i).Item("COURSE_ID")
                DR("COURSE_DESC") = DT_ORACLE.Rows(i).Item("COURSE_DESC")
                DR("SUBJ_G_ID") = DT_ORACLE.Rows(i).Item("SUBJ_G_ID")
                DR("TR_TYPE_ID") = DT_ORACLE.Rows(i).Item("TR_TYPE_ID")
                DR("TEST_FLAG") = DT_ORACLE.Rows(i).Item("TEST_FLAG")
                DR("PASS_PCT") = DT_ORACLE.Rows(i).Item("PASS_PCT")
                DR("TEST_SCORE") = DT_ORACLE.Rows(i).Item("TEST_SCORE")
                DR("TEST_PASS_PCT") = DT_ORACLE.Rows(i).Item("TEST_PASS_PCT")
                DR("UPD_BY") = DT_ORACLE.Rows(i).Item("UPD_BY")
                DR("UPD_DATE") = DT_ORACLE.Rows(i).Item("UPD_DATE")
                DR("Update_Time") = Now

                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView
                For Each DRV In DT_SQL.DefaultView
                    DRV.BeginEdit()
                    DRV.Item("COURSE_ID") = DT_ORACLE.Rows(i).Item("COURSE_ID")
                    DRV.Item("COURSE_DESC") = DT_ORACLE.Rows(i).Item("COURSE_DESC")
                    DRV.Item("SUBJ_G_ID") = DT_ORACLE.Rows(i).Item("SUBJ_G_ID")
                    DRV.Item("TR_TYPE_ID") = DT_ORACLE.Rows(i).Item("TR_TYPE_ID")
                    DRV.Item("TEST_FLAG") = DT_ORACLE.Rows(i).Item("TEST_FLAG")
                    DRV.Item("PASS_PCT") = DT_ORACLE.Rows(i).Item("PASS_PCT")
                    DRV.Item("TEST_SCORE") = DT_ORACLE.Rows(i).Item("TEST_SCORE")
                    DRV.Item("TEST_PASS_PCT") = DT_ORACLE.Rows(i).Item("TEST_PASS_PCT")
                    DRV.Item("UPD_BY") = DT_ORACLE.Rows(i).Item("UPD_BY")
                    DRV.Item("UPD_DATE") = DT_ORACLE.Rows(i).Item("UPD_DATE")
                    DRV.Item("Update_Time") = Now
                    DRV.EndEdit()
                Next
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_PK_COURSE", i + 1)
        Next
        RaiseEvent SyncComplete("tb_PK_COURSE", "Complete")
    End Sub

    Public Sub Sync_tb_PK_EMP_SELECT()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT YEAR" & vbCrLf
        SQL &= ", COURSE_ID" & vbCrLf
        SQL &= ", CLASS_NO" & vbCrLf
        SQL &= ", EMP_SEQ" & vbCrLf
        SQL &= ", PNPS_PSNL_NO" & vbCrLf
        SQL &= ", SELECT_METHOD" & vbCrLf
        SQL &= ", ENTRY_HOUR" & vbCrLf
        SQL &= ", ENTRY_DAY" & vbCrLf
        SQL &= ", ENTRY_RESULT" & vbCrLf
        SQL &= ", EMP_SCORE" & vbCrLf
        SQL &= ", TEST_RESULT" & vbCrLf
        SQL &= ", RESULT_FLAG" & vbCrLf
        SQL &= ", REMARK" & vbCrLf
        SQL &= ", UPD_BY" & vbCrLf
        SQL &= ", UPD_DATE" & vbCrLf
        SQL &= ", ROI_AMT" & vbCrLf
        SQL &= ", SEND_STAT" & vbCrLf

        SQL &= "FROM PNMGR.PK_EMP_SELECT"
        SQL &= " WHERE UPD_DATE > (SELECT MAX(UPD_DATE)  FROM tb_PK_EMP_SELECT) "




        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        SQL = SQL.Replace("PNMGR.PK_EMP_SELECT", "tb_PK_EMP_SELECT")
        '--------------- Add Update Time --------------
        SQL = SQL.Replace("FROM tb_PK_EMP_SELECT", ",Update_Time " & vbCrLf & "FROM tb_PK_EMP_SELECT")

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)



        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            DT_SQL.DefaultView.RowFilter = "YEAR = '" & FixDB(DT_ORACLE.Rows(i).Item("YEAR").ToString) & "' AND COURSE_ID = '" & FixDB(DT_ORACLE.Rows(i).Item("COURSE_ID").ToString) & "' AND CLASS_NO = '" & FixDB(DT_ORACLE.Rows(i).Item("CLASS_NO").ToString) & "' AND EMP_SEQ = '" & FixDB(DT_ORACLE.Rows(i).Item("EMP_SEQ").ToString) & "'"
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                DR("YEAR") = DT_ORACLE.Rows(i).Item("YEAR")
                DR("COURSE_ID") = DT_ORACLE.Rows(i).Item("COURSE_ID")
                DR("CLASS_NO") = DT_ORACLE.Rows(i).Item("CLASS_NO")
                DR("EMP_SEQ") = DT_ORACLE.Rows(i).Item("EMP_SEQ")
                DR("PNPS_PSNL_NO") = DT_ORACLE.Rows(i).Item("PNPS_PSNL_NO")
                DR("SELECT_METHOD") = DT_ORACLE.Rows(i).Item("SELECT_METHOD")
                DR("ENTRY_HOUR") = DT_ORACLE.Rows(i).Item("ENTRY_HOUR")
                DR("ENTRY_DAY") = DT_ORACLE.Rows(i).Item("ENTRY_DAY")
                DR("ENTRY_RESULT") = DT_ORACLE.Rows(i).Item("ENTRY_RESULT")
                DR("EMP_SCORE") = DT_ORACLE.Rows(i).Item("EMP_SCORE")
                DR("TEST_RESULT") = DT_ORACLE.Rows(i).Item("TEST_RESULT")
                DR("RESULT_FLAG") = DT_ORACLE.Rows(i).Item("RESULT_FLAG")
                DR("REMARK") = DT_ORACLE.Rows(i).Item("REMARK")
                DR("UPD_BY") = DT_ORACLE.Rows(i).Item("UPD_BY")
                DR("UPD_DATE") = DT_ORACLE.Rows(i).Item("UPD_DATE")
                DR("ROI_AMT") = DT_ORACLE.Rows(i).Item("ROI_AMT")
                DR("SEND_STAT") = DT_ORACLE.Rows(i).Item("SEND_STAT")
                DR("Update_Time") = Now

                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView
                For Each DRV In DT_SQL.DefaultView
                    DRV.BeginEdit()
                    DRV.Item("YEAR") = DT_ORACLE.Rows(i).Item("YEAR")
                    DRV.Item("COURSE_ID") = DT_ORACLE.Rows(i).Item("COURSE_ID")
                    DRV.Item("CLASS_NO") = DT_ORACLE.Rows(i).Item("CLASS_NO")
                    DRV.Item("EMP_SEQ") = DT_ORACLE.Rows(i).Item("EMP_SEQ")
                    DRV.Item("PNPS_PSNL_NO") = DT_ORACLE.Rows(i).Item("PNPS_PSNL_NO")
                    DRV.Item("SELECT_METHOD") = DT_ORACLE.Rows(i).Item("SELECT_METHOD")
                    DRV.Item("ENTRY_HOUR") = DT_ORACLE.Rows(i).Item("ENTRY_HOUR")
                    DRV.Item("ENTRY_DAY") = DT_ORACLE.Rows(i).Item("ENTRY_DAY")
                    DRV.Item("ENTRY_RESULT") = DT_ORACLE.Rows(i).Item("ENTRY_RESULT")
                    DRV.Item("EMP_SCORE") = DT_ORACLE.Rows(i).Item("EMP_SCORE")
                    DRV.Item("TEST_RESULT") = DT_ORACLE.Rows(i).Item("TEST_RESULT")
                    DRV.Item("RESULT_FLAG") = DT_ORACLE.Rows(i).Item("RESULT_FLAG")
                    DRV.Item("REMARK") = DT_ORACLE.Rows(i).Item("REMARK")
                    DRV.Item("UPD_BY") = DT_ORACLE.Rows(i).Item("UPD_BY")
                    DRV.Item("UPD_DATE") = DT_ORACLE.Rows(i).Item("UPD_DATE")
                    DRV.Item("ROI_AMT") = DT_ORACLE.Rows(i).Item("ROI_AMT")
                    DRV.Item("SEND_STAT") = DT_ORACLE.Rows(i).Item("SEND_STAT")
                    DRV.Item("Update_Time") = Now
                    DRV.EndEdit()
                Next
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_PK_EMP_SELECT", i + 1)
        Next
        RaiseEvent SyncComplete("tb_PK_EMP_SELECT", "Complete")
    End Sub

    Public Sub Sync_tb_PN_PUNISH_CODE_R()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT PNPI_PUNISH_CODE" & vbCrLf
        SQL &= ", PNPI_PUNISH_NAME" & vbCrLf
        SQL &= "FROM PNMGR.PN_PUNISH_CODE_R"

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        SQL = SQL.Replace("PNMGR.PN_PUNISH_CODE_R", "tb_PN_PUNISH_CODE_R")
        '--------------- Add Update Time --------------
        SQL = SQL.Replace("FROM tb_PN_PUNISH_CODE_R", ",Update_Time " & vbCrLf & "FROM tb_PN_PUNISH_CODE_R")

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            DT_SQL.DefaultView.RowFilter = "PNPI_PUNISH_CODE = '" & FixDB(DT_ORACLE.Rows(i).Item("PNPI_PUNISH_CODE").ToString) & "'"
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                DR("PNPI_PUNISH_CODE") = DT_ORACLE.Rows(i).Item("PNPI_PUNISH_CODE")
                DR("PNPI_PUNISH_NAME") = DT_ORACLE.Rows(i).Item("PNPI_PUNISH_NAME")
                DR("Update_Time") = Now

                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView
                For Each DRV In DT_SQL.DefaultView
                    DRV.BeginEdit()
                    DRV.Item("PNPI_PUNISH_CODE") = DT_ORACLE.Rows(i).Item("PNPI_PUNISH_CODE")
                    DRV.Item("PNPI_PUNISH_NAME") = DT_ORACLE.Rows(i).Item("PNPI_PUNISH_NAME")
                    DRV.Item("Update_Time") = Now
                    DRV.EndEdit()
                Next
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_PN_PUNISH_CODE_R", i + 1)
        Next
        RaiseEvent SyncComplete("tb_PN_PUNISH_CODE_R", "Complete")
    End Sub

    Public Sub Sync_tb_PN_PUNISH_T()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT PNPH_PSNL_NO " & vbCrLf
        SQL &= ", PNPH_CRIME_DOC " & vbCrLf
        SQL &= ", PNPH_CRIME_DATE " & vbCrLf
        SQL &= ", PNPH_CRIME_CODE " & vbCrLf
        SQL &= ", PNPH_PERC1 " & vbCrLf
        SQL &= ", PNPH_DATE_STR " & vbCrLf
        SQL &= ", PNPH_DATE_END " & vbCrLf
        SQL &= ", PNPH_PRO1 " & vbCrLf
        SQL &= ", PNPH_PRO2 " & vbCrLf
        SQL &= ", PNPH_PUNISH_DOC " & vbCrLf
        SQL &= ", PNPH_PUNISH_DATE " & vbCrLf
        SQL &= ", PNPH_PUNISH_CODE " & vbCrLf
        SQL &= ", PNPH_PERC2 " & vbCrLf
        SQL &= ", PNPH_DATE_STR2 " & vbCrLf
        SQL &= ", PNPH_DATE_END2 " & vbCrLf
        SQL &= ", PNPH_REMARK1 " & vbCrLf
        SQL &= ", PNPH_REMARK2 " & vbCrLf
        SQL &= ", PNPH_GILL " & vbCrLf
        SQL &= ", PNPH_REMARK " & vbCrLf
        SQL &= ", PNPH_FLAG " & vbCrLf
        SQL &= ", RECORD_STATUS " & vbCrLf
        SQL &= ", USER_ID " & vbCrLf
        SQL &= ", USER_DATE " & vbCrLf
        SQL &= "FROM PNMGR.PN_PUNISH_T"

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        SQL = SQL.Replace("PNMGR.PN_PUNISH_T", "tb_PN_PUNISH_T")
        '--------------- Add Update Time --------------
        SQL = SQL.Replace("FROM tb_PN_PUNISH_T", ",Update_Time " & vbCrLf & "FROM tb_PN_PUNISH_T")

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            DT_SQL.DefaultView.RowFilter = "PNPH_PSNL_NO = '" & FixDB(DT_ORACLE.Rows(i).Item("PNPH_PSNL_NO").ToString) & "' AND PNPH_CRIME_CODE = '" & FixDB(DT_ORACLE.Rows(i).Item("PNPH_CRIME_CODE").ToString) & "' AND PNPH_PUNISH_CODE = '" & FixDB(DT_ORACLE.Rows(i).Item("PNPH_PUNISH_CODE").ToString) & "' AND PNPH_DATE_STR2 = '" & FixDB(DT_ORACLE.Rows(i).Item("PNPH_DATE_STR2").ToString) & "' AND PNPH_DATE_END2 = '" & FixDB(DT_ORACLE.Rows(i).Item("PNPH_DATE_END2").ToString) & "'"
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                DR("PNPH_PSNL_NO") = DT_ORACLE.Rows(i).Item("PNPH_PSNL_NO")
                DR("PNPH_CRIME_DOC") = DT_ORACLE.Rows(i).Item("PNPH_CRIME_DOC")
                DR("PNPH_CRIME_DATE") = DT_ORACLE.Rows(i).Item("PNPH_CRIME_DATE")
                DR("PNPH_CRIME_CODE") = DT_ORACLE.Rows(i).Item("PNPH_CRIME_CODE")
                DR("PNPH_PERC1") = DT_ORACLE.Rows(i).Item("PNPH_PERC1")
                DR("PNPH_DATE_STR") = DT_ORACLE.Rows(i).Item("PNPH_DATE_STR")
                DR("PNPH_DATE_END") = DT_ORACLE.Rows(i).Item("PNPH_DATE_END")
                DR("PNPH_PRO1") = DT_ORACLE.Rows(i).Item("PNPH_PRO1")
                DR("PNPH_PRO2") = DT_ORACLE.Rows(i).Item("PNPH_PRO2")
                DR("PNPH_PUNISH_DOC") = DT_ORACLE.Rows(i).Item("PNPH_PUNISH_DOC")
                DR("PNPH_PUNISH_DATE") = DT_ORACLE.Rows(i).Item("PNPH_PUNISH_DATE")
                DR("PNPH_PUNISH_CODE") = DT_ORACLE.Rows(i).Item("PNPH_PUNISH_CODE")
                DR("PNPH_PERC2") = DT_ORACLE.Rows(i).Item("PNPH_PERC2")
                DR("PNPH_DATE_STR2") = DT_ORACLE.Rows(i).Item("PNPH_DATE_STR2")
                DR("PNPH_DATE_END2") = DT_ORACLE.Rows(i).Item("PNPH_DATE_END2")
                DR("PNPH_REMARK1") = DT_ORACLE.Rows(i).Item("PNPH_REMARK1")
                DR("PNPH_REMARK2") = DT_ORACLE.Rows(i).Item("PNPH_REMARK2")
                DR("PNPH_GILL") = DT_ORACLE.Rows(i).Item("PNPH_GILL")
                DR("PNPH_REMARK") = DT_ORACLE.Rows(i).Item("PNPH_REMARK")
                DR("PNPH_FLAG") = DT_ORACLE.Rows(i).Item("PNPH_FLAG")
                DR("RECORD_STATUS") = DT_ORACLE.Rows(i).Item("RECORD_STATUS")
                DR("USER_ID") = DT_ORACLE.Rows(i).Item("USER_ID")
                DR("USER_DATE") = DT_ORACLE.Rows(i).Item("USER_DATE")
                DR("Update_Time") = Now

                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView
                For Each DRV In DT_SQL.DefaultView
                    DRV.BeginEdit()
                    DRV.Item("PNPH_PSNL_NO") = DT_ORACLE.Rows(i).Item("PNPH_PSNL_NO")
                    DRV.Item("PNPH_CRIME_DOC") = DT_ORACLE.Rows(i).Item("PNPH_CRIME_DOC")
                    DRV.Item("PNPH_CRIME_DATE") = DT_ORACLE.Rows(i).Item("PNPH_CRIME_DATE")
                    DRV.Item("PNPH_CRIME_CODE") = DT_ORACLE.Rows(i).Item("PNPH_CRIME_CODE")
                    DRV.Item("PNPH_PERC1") = DT_ORACLE.Rows(i).Item("PNPH_PERC1")
                    DRV.Item("PNPH_DATE_STR") = DT_ORACLE.Rows(i).Item("PNPH_DATE_STR")
                    DRV.Item("PNPH_DATE_END") = DT_ORACLE.Rows(i).Item("PNPH_DATE_END")
                    DRV.Item("PNPH_PRO1") = DT_ORACLE.Rows(i).Item("PNPH_PRO1")
                    DRV.Item("PNPH_PRO2") = DT_ORACLE.Rows(i).Item("PNPH_PRO2")
                    DRV.Item("PNPH_PUNISH_DOC") = DT_ORACLE.Rows(i).Item("PNPH_PUNISH_DOC")
                    DRV.Item("PNPH_PUNISH_DATE") = DT_ORACLE.Rows(i).Item("PNPH_PUNISH_DATE")
                    DRV.Item("PNPH_PUNISH_CODE") = DT_ORACLE.Rows(i).Item("PNPH_PUNISH_CODE")
                    DRV.Item("PNPH_PERC2") = DT_ORACLE.Rows(i).Item("PNPH_PERC2")
                    DRV.Item("PNPH_DATE_STR2") = DT_ORACLE.Rows(i).Item("PNPH_DATE_STR2")
                    DRV.Item("PNPH_DATE_END2") = DT_ORACLE.Rows(i).Item("PNPH_DATE_END2")
                    DRV.Item("PNPH_REMARK1") = DT_ORACLE.Rows(i).Item("PNPH_REMARK1")
                    DRV.Item("PNPH_REMARK2") = DT_ORACLE.Rows(i).Item("PNPH_REMARK2")
                    DRV.Item("PNPH_GILL") = DT_ORACLE.Rows(i).Item("PNPH_GILL")
                    DRV.Item("PNPH_REMARK") = DT_ORACLE.Rows(i).Item("PNPH_REMARK")
                    DRV.Item("PNPH_FLAG") = DT_ORACLE.Rows(i).Item("PNPH_FLAG")
                    DRV.Item("RECORD_STATUS") = DT_ORACLE.Rows(i).Item("RECORD_STATUS")
                    DRV.Item("USER_ID") = DT_ORACLE.Rows(i).Item("USER_ID")
                    DRV.Item("USER_DATE") = DT_ORACLE.Rows(i).Item("USER_DATE")
                    DRV.Item("Update_Time") = Now
                    DRV.EndEdit()
                Next
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_PN_PUNISH_T", i + 1)
        Next
        RaiseEvent SyncComplete("tb_PN_PUNISH_T", "Complete")
    End Sub

    Public Sub Sync_tb_CP_SUBJECT_MASTER()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT CPSM_SUBJECT_CODE " & vbCrLf
        SQL &= ", CPSM_SUBJECT_NAME " & vbCrLf
        SQL &= ", CPSM_SUBJECT_TYPE " & vbCrLf
        SQL &= ", CPSM_SUBJECT_POINT " & vbCrLf
        SQL &= ", CPSM_SUBJECT_POINT_RULE " & vbCrLf
        SQL &= "FROM PNMGR.CP_SUBJECT_MASTER"

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        SQL = SQL.Replace("PNMGR.CP_SUBJECT_MASTER", "tb_CP_SUBJECT_MASTER")
        '--------------- Add Update Time --------------
        SQL = SQL.Replace("FROM tb_CP_SUBJECT_MASTER", ",Update_Time " & vbCrLf & "FROM tb_CP_SUBJECT_MASTER")

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            DT_SQL.DefaultView.RowFilter = "CPSM_SUBJECT_CODE = '" & FixDB(DT_ORACLE.Rows(i).Item("CPSM_SUBJECT_CODE").ToString) & "'"
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                DR("CPSM_SUBJECT_CODE") = DT_ORACLE.Rows(i).Item("CPSM_SUBJECT_CODE")
                DR("CPSM_SUBJECT_NAME") = DT_ORACLE.Rows(i).Item("CPSM_SUBJECT_NAME")
                DR("CPSM_SUBJECT_TYPE") = DT_ORACLE.Rows(i).Item("CPSM_SUBJECT_TYPE")
                DR("CPSM_SUBJECT_POINT") = DT_ORACLE.Rows(i).Item("CPSM_SUBJECT_POINT")
                DR("CPSM_SUBJECT_POINT_RULE") = DT_ORACLE.Rows(i).Item("CPSM_SUBJECT_POINT_RULE")
                DR("Update_Time") = Now
                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView
                For Each DRV In DT_SQL.DefaultView
                    DRV.BeginEdit()
                    DRV.Item("CPSM_SUBJECT_CODE") = DT_ORACLE.Rows(i).Item("CPSM_SUBJECT_CODE")
                    DRV.Item("CPSM_SUBJECT_NAME") = DT_ORACLE.Rows(i).Item("CPSM_SUBJECT_NAME")
                    DRV.Item("CPSM_SUBJECT_TYPE") = DT_ORACLE.Rows(i).Item("CPSM_SUBJECT_TYPE")
                    DRV.Item("CPSM_SUBJECT_POINT") = DT_ORACLE.Rows(i).Item("CPSM_SUBJECT_POINT")
                    DRV.Item("CPSM_SUBJECT_POINT_RULE") = DT_ORACLE.Rows(i).Item("CPSM_SUBJECT_POINT_RULE")
                    DRV.Item("Update_Time") = Now
                    DRV.EndEdit()
                Next
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_CP_SUBJECT_MASTER", i + 1)
        Next
        RaiseEvent SyncComplete("tb_CP_SUBJECT_MASTER", "Complete")
    End Sub

    Public Sub Sync_tb_CP_SUBJECT_PERSONAL()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT CPSP_PERSONAL_NO " & vbCrLf
        SQL &= ", CPSP_SUBJECT_CODE " & vbCrLf
        SQL &= ", CPSP_POINT " & vbCrLf
        SQL &= ", CPSP_EXAM_DATE " & vbCrLf
        SQL &= "FROM PNMGR.CP_SUBJECT_PERSONAL"

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        SQL = SQL.Replace("PNMGR.CP_SUBJECT_PERSONAL", "tb_CP_SUBJECT_PERSONAL")
        '--------------- Add Update Time --------------
        SQL = SQL.Replace("FROM tb_CP_SUBJECT_PERSONAL", ",Update_Time " & vbCrLf & "FROM tb_CP_SUBJECT_PERSONAL")

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            'DT_SQL.DefaultView.RowFilter = " CPSP_SUBJECT_CODE = '" & FixDB(DT_ORACLE.Rows(i).Item("CPSP_SUBJECT_CODE").ToString) & "' AND CPSP_EXAM_DATE = '" & FixDB(DT_ORACLE.Rows(i).Item("CPSP_EXAM_DATE").ToString) & "'"
            DT_SQL.DefaultView.RowFilter = "CPSP_PERSONAL_NO = '" & FixDB(DT_ORACLE.Rows(i).Item("CPSP_PERSONAL_NO").ToString) & "' AND CPSP_SUBJECT_CODE = '" & FixDB(DT_ORACLE.Rows(i).Item("CPSP_SUBJECT_CODE").ToString) & "' AND CPSP_EXAM_DATE = '" & FixDB(DT_ORACLE.Rows(i).Item("CPSP_EXAM_DATE").ToString) & "'"
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                DR("CPSP_PERSONAL_NO") = DT_ORACLE.Rows(i).Item("CPSP_PERSONAL_NO")
                DR("CPSP_SUBJECT_CODE") = DT_ORACLE.Rows(i).Item("CPSP_SUBJECT_CODE")
                DR("CPSP_POINT") = DT_ORACLE.Rows(i).Item("CPSP_POINT")
                DR("CPSP_EXAM_DATE") = DT_ORACLE.Rows(i).Item("CPSP_EXAM_DATE")
                DR("Update_Time") = Now
                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView
                For Each DRV In DT_SQL.DefaultView
                    DRV.BeginEdit()
                    DRV.Item("CPSP_PERSONAL_NO") = DT_ORACLE.Rows(i).Item("CPSP_PERSONAL_NO")
                    DRV.Item("CPSP_SUBJECT_CODE") = DT_ORACLE.Rows(i).Item("CPSP_SUBJECT_CODE")
                    DRV.Item("CPSP_POINT") = DT_ORACLE.Rows(i).Item("CPSP_POINT")
                    DRV.Item("CPSP_EXAM_DATE") = DT_ORACLE.Rows(i).Item("CPSP_EXAM_DATE")
                    DRV.Item("Update_Time") = Now
                    DRV.EndEdit()
                Next
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_CP_SUBJECT_PERSONAL", i + 1)
        Next
        RaiseEvent SyncComplete("tb_CP_SUBJECT_PERSONAL", "Complete")
    End Sub

    Public Sub Sync_tb_PN_POSITION_HISTORY_T()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT PNHH_PSNL_NO " & vbCrLf
        SQL &= ",  PNHH_POS_NO  " & vbCrLf
        SQL &= ",  PNHH_POS_FLD  " & vbCrLf
        SQL &= ",  PNHH_POS_MGR  " & vbCrLf
        SQL &= ",  PNHH_POS_EXP  " & vbCrLf
        SQL &= ",  PNHH_CLASS  " & vbCrLf
        SQL &= ",  PNHH_LEVEL  " & vbCrLf
        SQL &= ",  PNHH_DEPT  " & vbCrLf
        SQL &= ",  PNHH_MINOR  " & vbCrLf
        SQL &= ",  PNHH_DOC_DATE  " & vbCrLf
        SQL &= ",  PNHH_DOC_NO  " & vbCrLf
        SQL &= ",  PNHH_MOVE_CD  " & vbCrLf
        SQL &= ",  PNHH_EFFECT_STR  " & vbCrLf
        SQL &= ",  PNHH_DATE_END  " & vbCrLf
        SQL &= ",  PNHH_PRODLINE_CD  " & vbCrLf
        SQL &= ",  PNHH_POS_NAME  " & vbCrLf
        SQL &= ",  PNHH_EDU_NAME  " & vbCrLf
        SQL &= ",  RECORD_STATUS  " & vbCrLf
        SQL &= ",  USER_ID  " & vbCrLf
        SQL &= ",  USER_DATE  " & vbCrLf
        SQL &= ",  PNHH_TYPE  " & vbCrLf
        SQL &= ",  PNHH_PSNL_TYPE  " & vbCrLf
        SQL &= ",  PNHH_CLASS_DATE  " & vbCrLf
        SQL &= ",  PNHH_LEVEL_DATE  " & vbCrLf
        SQL &= ",  PNHH_POSITION_DATE  " & vbCrLf
        SQL &= ",  DEPT_CODE_NEW  " & vbCrLf
        SQL &= "FROM PNMGR.PN_POSITION_HISTORY_T WHERE PNHH_PSNL_NO NOT in ('000640')"

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        SQL = SQL.Replace("PNMGR.PN_POSITION_HISTORY_T", "tb_PN_POSITION_HISTORY_T")
        '--------------- Add Update Time --------------
        SQL = SQL.Replace("FROM tb_PN_POSITION_HISTORY_T", ",Update_Time " & vbCrLf & "FROM tb_PN_POSITION_HISTORY_T ")

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            DT_SQL.DefaultView.RowFilter = "PNHH_PSNL_NO = '" & FixDB(DT_ORACLE.Rows(i).Item("PNHH_PSNL_NO").ToString) & "' AND PNHH_EFFECT_STR = '" & FixDate(DT_ORACLE.Rows(i).Item("PNHH_EFFECT_STR")) & "'"
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------

                If IsDBNull((DT_ORACLE.Rows(i).Item("PNHH_EFFECT_STR"))) Then
                Else
                    Dim DR As DataRow = DT_SQL.NewRow
                    DR("PNHH_PSNL_NO") = DT_ORACLE.Rows(i).Item("PNHH_PSNL_NO")
                    DR("PNHH_POS_NO") = DT_ORACLE.Rows(i).Item("PNHH_POS_NO")
                    DR("PNHH_POS_FLD") = DT_ORACLE.Rows(i).Item("PNHH_POS_FLD")
                    DR("PNHH_POS_MGR") = DT_ORACLE.Rows(i).Item("PNHH_POS_MGR")
                    DR("PNHH_POS_EXP") = DT_ORACLE.Rows(i).Item("PNHH_POS_EXP")
                    DR("PNHH_CLASS") = DT_ORACLE.Rows(i).Item("PNHH_CLASS")
                    DR("PNHH_LEVEL") = DT_ORACLE.Rows(i).Item("PNHH_LEVEL")
                    DR("PNHH_DEPT") = DT_ORACLE.Rows(i).Item("PNHH_DEPT")
                    DR("PNHH_MINOR") = DT_ORACLE.Rows(i).Item("PNHH_MINOR")
                    DR("PNHH_DOC_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNHH_DOC_DATE"))
                    DR("PNHH_DOC_NO") = DT_ORACLE.Rows(i).Item("PNHH_DOC_NO")
                    DR("PNHH_MOVE_CD") = DT_ORACLE.Rows(i).Item("PNHH_MOVE_CD")
                    DR("PNHH_EFFECT_STR") = FixDate(DT_ORACLE.Rows(i).Item("PNHH_EFFECT_STR"))
                    DR("PNHH_DATE_END") = FixDate(DT_ORACLE.Rows(i).Item("PNHH_DATE_END"))
                    DR("PNHH_PRODLINE_CD") = DT_ORACLE.Rows(i).Item("PNHH_PRODLINE_CD")
                    DR("PNHH_POS_NAME") = DT_ORACLE.Rows(i).Item("PNHH_POS_NAME")
                    DR("PNHH_EDU_NAME") = DT_ORACLE.Rows(i).Item("PNHH_EDU_NAME")
                    DR("RECORD_STATUS") = DT_ORACLE.Rows(i).Item("RECORD_STATUS")
                    DR("USER_ID") = DT_ORACLE.Rows(i).Item("USER_ID")
                    DR("USER_DATE") = FixDate(DT_ORACLE.Rows(i).Item("USER_DATE"))
                    DR("PNHH_TYPE") = DT_ORACLE.Rows(i).Item("PNHH_TYPE")
                    DR("PNHH_PSNL_TYPE") = DT_ORACLE.Rows(i).Item("PNHH_PSNL_TYPE")
                    DR("PNHH_CLASS_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNHH_CLASS_DATE"))
                    DR("PNHH_LEVEL_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNHH_LEVEL_DATE"))
                    DR("PNHH_POSITION_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNHH_POSITION_DATE"))
                    DR("DEPT_CODE_NEW") = DT_ORACLE.Rows(i).Item("DEPT_CODE_NEW")
                    DR("Update_Time") = Now
                    DT_SQL.Rows.Add(DR)
                    Dim cmd As New SqlCommandBuilder(DA_SQL)
                    DA_SQL.Update(DT_SQL)
                End If
            Else
                If IsDBNull((DT_ORACLE.Rows(i).Item("PNHH_EFFECT_STR"))) Then
                    Exit For
                Else
                    '--------- EDIT ---------
                    Dim DRV As DataRowView
                    For Each DRV In DT_SQL.DefaultView
                        DRV.BeginEdit()
                        'DRV.Item("PNHH_PSNL_NO") = DT_ORACLE.Rows(i).Item("PNHH_PSNL_NO")
                        'DRV.Item("PNHH_POS_NO") = DT_ORACLE.Rows(i).Item("PNHH_POS_NO")
                        'DRV.Item("PNHH_POS_FLD") = DT_ORACLE.Rows(i).Item("PNHH_POS_FLD")
                        'DRV.Item("PNHH_POS_MGR") = DT_ORACLE.Rows(i).Item("PNHH_POS_MGR")
                        'DRV.Item("PNHH_POS_EXP") = DT_ORACLE.Rows(i).Item("PNHH_POS_EXP")
                        'DRV.Item("PNHH_CLASS") = DT_ORACLE.Rows(i).Item("PNHH_CLASS")
                        'DRV.Item("PNHH_LEVEL") = DT_ORACLE.Rows(i).Item("PNHH_LEVEL")
                        'DRV.Item("PNHH_DEPT") = DT_ORACLE.Rows(i).Item("PNHH_DEPT")
                        'DRV.Item("PNHH_MINOR") = DT_ORACLE.Rows(i).Item("PNHH_MINOR")
                        'DRV.Item("PNHH_DOC_DATE") = DT_ORACLE.Rows(i).Item("PNHH_DOC_DATE")
                        'DRV.Item("PNHH_DOC_NO") = DT_ORACLE.Rows(i).Item("PNHH_DOC_NO")
                        'DRV.Item("PNHH_MOVE_CD") = DT_ORACLE.Rows(i).Item("PNHH_MOVE_CD")
                        'DRV.Item("PNHH_EFFECT_STR") = DT_ORACLE.Rows(i).Item("PNHH_EFFECT_STR")
                        'DRV.Item("PNHH_DATE_END") = DT_ORACLE.Rows(i).Item("PNHH_DATE_END")
                        'DRV.Item("PNHH_PRODLINE_CD") = DT_ORACLE.Rows(i).Item("PNHH_PRODLINE_CD")
                        'DRV.Item("PNHH_POS_NAME") = DT_ORACLE.Rows(i).Item("PNHH_POS_NAME")
                        'DRV.Item("PNHH_EDU_NAME") = DT_ORACLE.Rows(i).Item("PNHH_EDU_NAME")
                        'DRV.Item("RECORD_STATUS") = DT_ORACLE.Rows(i).Item("RECORD_STATUS")
                        'DRV.Item("USER_ID") = DT_ORACLE.Rows(i).Item("USER_ID")
                        'DRV.Item("USER_DATE") = DT_ORACLE.Rows(i).Item("USER_DATE")
                        'DRV.Item("PNHH_TYPE") = DT_ORACLE.Rows(i).Item("PNHH_TYPE")
                        'DRV.Item("PNHH_PSNL_TYPE") = DT_ORACLE.Rows(i).Item("PNHH_PSNL_TYPE")
                        'DRV.Item("PNHH_CLASS_DATE") = DT_ORACLE.Rows(i).Item("PNHH_CLASS_DATE")
                        'DRV.Item("PNHH_LEVEL_DATE") = DT_ORACLE.Rows(i).Item("PNHH_LEVEL_DATE")
                        'DRV.Item("PNHH_POSITION_DATE") = DT_ORACLE.Rows(i).Item("PNHH_POSITION_DATE")
                        'DRV.Item("DEPT_CODE_NEW") = DT_ORACLE.Rows(i).Item("DEPT_CODE_NEW")
                        'DRV.Item("Update_Time") = Now
                        DRV.Item("PNHH_PSNL_NO") = DT_ORACLE.Rows(i).Item("PNHH_PSNL_NO")
                        DRV.Item("PNHH_POS_NO") = DT_ORACLE.Rows(i).Item("PNHH_POS_NO")
                        DRV.Item("PNHH_POS_FLD") = DT_ORACLE.Rows(i).Item("PNHH_POS_FLD")
                        DRV.Item("PNHH_POS_MGR") = DT_ORACLE.Rows(i).Item("PNHH_POS_MGR")
                        DRV.Item("PNHH_POS_EXP") = DT_ORACLE.Rows(i).Item("PNHH_POS_EXP")
                        DRV.Item("PNHH_CLASS") = DT_ORACLE.Rows(i).Item("PNHH_CLASS")
                        DRV.Item("PNHH_LEVEL") = DT_ORACLE.Rows(i).Item("PNHH_LEVEL")
                        DRV.Item("PNHH_DEPT") = DT_ORACLE.Rows(i).Item("PNHH_DEPT")
                        DRV.Item("PNHH_MINOR") = DT_ORACLE.Rows(i).Item("PNHH_MINOR")
                        DRV.Item("PNHH_DOC_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNHH_DOC_DATE"))
                        DRV.Item("PNHH_DOC_NO") = DT_ORACLE.Rows(i).Item("PNHH_DOC_NO")
                        DRV.Item("PNHH_MOVE_CD") = DT_ORACLE.Rows(i).Item("PNHH_MOVE_CD")
                        DRV.Item("PNHH_EFFECT_STR") = FixDate(DT_ORACLE.Rows(i).Item("PNHH_EFFECT_STR"))
                        DRV.Item("PNHH_DATE_END") = FixDate(DT_ORACLE.Rows(i).Item("PNHH_DATE_END"))
                        DRV.Item("PNHH_PRODLINE_CD") = DT_ORACLE.Rows(i).Item("PNHH_PRODLINE_CD")
                        DRV.Item("PNHH_POS_NAME") = DT_ORACLE.Rows(i).Item("PNHH_POS_NAME")
                        DRV.Item("PNHH_EDU_NAME") = DT_ORACLE.Rows(i).Item("PNHH_EDU_NAME")
                        DRV.Item("RECORD_STATUS") = DT_ORACLE.Rows(i).Item("RECORD_STATUS")
                        DRV.Item("USER_ID") = DT_ORACLE.Rows(i).Item("USER_ID")
                        DRV.Item("USER_DATE") = FixDate(DT_ORACLE.Rows(i).Item("USER_DATE"))
                        DRV.Item("PNHH_TYPE") = DT_ORACLE.Rows(i).Item("PNHH_TYPE")
                        DRV.Item("PNHH_PSNL_TYPE") = DT_ORACLE.Rows(i).Item("PNHH_PSNL_TYPE")
                        DRV.Item("PNHH_CLASS_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNHH_CLASS_DATE"))
                        DRV.Item("PNHH_LEVEL_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNHH_LEVEL_DATE"))
                        DRV.Item("PNHH_POSITION_DATE") = FixDate(DT_ORACLE.Rows(i).Item("PNHH_POSITION_DATE"))
                        DRV.Item("DEPT_CODE_NEW") = DT_ORACLE.Rows(i).Item("DEPT_CODE_NEW")
                        DRV.Item("Update_Time") = Now
                        DRV.EndEdit()
                    Next
                    Dim cmd As New SqlCommandBuilder(DA_SQL)
                    DA_SQL.Update(DT_SQL)
                End If
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_PN_POSITION_HISTORY_T", i + 1)
        Next
        RaiseEvent SyncComplete("tb_PN_POSITION_HISTORY_T", "Complete")
    End Sub

    Public Sub Sync_tb_PN_POSITION_RETURN2CLASS()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT  " & vbCrLf
        SQL &= " PNHH_PSNL_NO   " & vbCrLf
        SQL &= ", PNHH_CLASS1   " & vbCrLf
        SQL &= ", PNHH_EFFECT_DATE1   " & vbCrLf
        SQL &= ", PNHH_CLASS2   " & vbCrLf
        '------------PNHH_EFFECT_DATE2 Error---------
        'SQL &= ", PNHH_EFFECT_DATE2   " & vbCrLf
        SQL &= ",EXTRACT(DAY FROM PNHH_EFFECT_DATE2) PNHH_EFFECT_DATE2_d" & vbCrLf
        SQL &= ",EXTRACT(MONTH FROM PNHH_EFFECT_DATE2) PNHH_EFFECT_DATE2_m" & vbCrLf
        SQL &= ",EXTRACT(YEAR FROM PNHH_EFFECT_DATE2) PNHH_EFFECT_DATE2_y" & vbCrLf
        '--------------------------------------------
        SQL &= ", PNHH_CLASS3   " & vbCrLf
        SQL &= ", PNHH_EFFECT_DATE3   " & vbCrLf
        SQL &= ", USER_ID   " & vbCrLf
        SQL &= ", USER_DATE   " & vbCrLf
        SQL &= "FROM PNMGR.PN_POSITION_RETURN2CLASS"

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        '---------- Compile PNPS_RETIRE_DATE-----------
        DT_ORACLE.Columns.Add("PNHH_EFFECT_DATE2", GetType(Date))

        For i As Integer = 0 To DT_ORACLE.Rows.Count - 1
            Try
                Dim rdate As New Date(DT_ORACLE.Rows(i).Item("PNHH_EFFECT_DATE2_y"), DT_ORACLE.Rows(i).Item("PNHH_EFFECT_DATE2_m"), DT_ORACLE.Rows(i).Item("PNHH_EFFECT_DATE2_d"))
                DT_ORACLE.Rows(i).Item("PNHH_EFFECT_DATE2") = rdate
            Catch ex As Exception
            End Try
        Next
        DT_ORACLE.Columns.Remove("PNHH_EFFECT_DATE2_d")
        DT_ORACLE.Columns.Remove("PNHH_EFFECT_DATE2_m")
        DT_ORACLE.Columns.Remove("PNHH_EFFECT_DATE2_y")

        'SQL = SQL.Replace("PNMGR.PN_POSITION_RETURN2CLASS", "tb_PN_POSITION_RETURN2CLASS")
        ''--------------- Add Update Time --------------
        'SQL = SQL.Replace("FROM tb_PN_POSITION_RETURN2CLASS", ",Update_Time " & vbCrLf & "FROM tb_PN_POSITION_RETURN2CLASS")

        SQL = " SELECT  " & vbCrLf
        SQL &= " PNHH_PSNL_NO   " & vbCrLf
        SQL &= ", PNHH_CLASS1   " & vbCrLf
        SQL &= ", PNHH_EFFECT_DATE1   " & vbCrLf
        SQL &= ", PNHH_CLASS2   " & vbCrLf
        SQL &= ", PNHH_EFFECT_DATE2   " & vbCrLf
        SQL &= ", PNHH_CLASS3   " & vbCrLf
        SQL &= ", PNHH_EFFECT_DATE3   " & vbCrLf
        SQL &= ", USER_ID   " & vbCrLf
        SQL &= ", USER_DATE   " & vbCrLf
        SQL &= ",Update_Time" & vbCrLf
        SQL &= "FROM tb_PN_POSITION_RETURN2CLASS"

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            DT_SQL.DefaultView.RowFilter = "PNHH_PSNL_NO = '" & FixDB(DT_ORACLE.Rows(i).Item("PNHH_PSNL_NO").ToString) & "'"
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                DR("PNHH_PSNL_NO") = DT_ORACLE.Rows(i).Item("PNHH_PSNL_NO")
                DR("PNHH_CLASS1") = DT_ORACLE.Rows(i).Item("PNHH_CLASS1")
                DR("PNHH_EFFECT_DATE1") = DT_ORACLE.Rows(i).Item("PNHH_EFFECT_DATE1")
                DR("PNHH_CLASS2") = DT_ORACLE.Rows(i).Item("PNHH_CLASS2")
                DR("PNHH_EFFECT_DATE2") = DT_ORACLE.Rows(i).Item("PNHH_EFFECT_DATE2")
                DR("PNHH_CLASS3") = DT_ORACLE.Rows(i).Item("PNHH_CLASS3")
                DR("PNHH_EFFECT_DATE3") = DT_ORACLE.Rows(i).Item("PNHH_EFFECT_DATE3")
                DR("USER_ID") = DT_ORACLE.Rows(i).Item("USER_ID")
                DR("USER_DATE") = DT_ORACLE.Rows(i).Item("USER_DATE")
                DR("Update_Time") = Now
                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView
                For Each DRV In DT_SQL.DefaultView
                    DRV.BeginEdit()
                    DRV.Item("PNHH_PSNL_NO") = DT_ORACLE.Rows(i).Item("PNHH_PSNL_NO")
                    DRV.Item("PNHH_CLASS1") = DT_ORACLE.Rows(i).Item("PNHH_CLASS1")
                    DRV.Item("PNHH_EFFECT_DATE1") = DT_ORACLE.Rows(i).Item("PNHH_EFFECT_DATE1")
                    DRV.Item("PNHH_CLASS2") = DT_ORACLE.Rows(i).Item("PNHH_CLASS2")
                    DRV.Item("PNHH_EFFECT_DATE2") = DT_ORACLE.Rows(i).Item("PNHH_EFFECT_DATE2")
                    DRV.Item("PNHH_CLASS3") = DT_ORACLE.Rows(i).Item("PNHH_CLASS3")
                    DRV.Item("PNHH_EFFECT_DATE3") = DT_ORACLE.Rows(i).Item("PNHH_EFFECT_DATE3")
                    DRV.Item("USER_ID") = DT_ORACLE.Rows(i).Item("USER_ID")
                    DRV.Item("USER_DATE") = DT_ORACLE.Rows(i).Item("USER_DATE")
                    DRV.Item("Update_Time") = Now
                    DRV.EndEdit()
                Next
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_PN_POSITION_RETURN2CLASS", i + 1)
        Next
        RaiseEvent SyncComplete("tb_PN_POSITION_RETURN2CLASS", "Complete")
    End Sub

    Public Sub Sync_tb_PK_SUBJ_G()
        Dim SQL As String = ""
        Dim DT_ORACLE As New DataTable
        Dim DT_SQL As New DataTable
        Dim DA_ORACLE As New OracleDataAdapter
        Dim DA_SQL As New SqlDataAdapter

        SQL = " SELECT  " & vbCrLf
        SQL &= " SUBJ_G_ID   " & vbCrLf
        SQL &= ", SUBJ_G_DESC   " & vbCrLf
        SQL &= "FROM PNMGR.PK_SUBJ_G"

        DA_ORACLE = New OracleDataAdapter(SQL, GetOracleConnStr)
        DT_ORACLE = New DataTable
        DA_ORACLE.Fill(DT_ORACLE)

        SQL = SQL.Replace("PNMGR.PK_SUBJ_G", "tb_PK_SUBJ_G")
        '--------------- Add Update Time --------------
        SQL = SQL.Replace("FROM tb_PK_SUBJ_G", ",Update_Time " & vbCrLf & "FROM tb_PK_SUBJ_G")

        DA_SQL = New SqlDataAdapter(SQL, GetSqlConnStr)
        DT_SQL = New DataTable
        DA_SQL.Fill(DT_SQL)

        For i As Int32 = 0 To DT_ORACLE.Rows.Count - 1
            DT_SQL.DefaultView.RowFilter = "SUBJ_G_ID = '" & FixDB(DT_ORACLE.Rows(i).Item("SUBJ_G_ID").ToString) & "'"
            If DT_SQL.DefaultView.Count = 0 Then
                '---------- ADD ----------
                Dim DR As DataRow = DT_SQL.NewRow
                DR("SUBJ_G_ID") = DT_ORACLE.Rows(i).Item("SUBJ_G_ID")
                DR("SUBJ_G_DESC") = DT_ORACLE.Rows(i).Item("SUBJ_G_DESC")
                DR("Update_Time") = Now
                DT_SQL.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            Else
                '--------- EDIT ---------
                Dim DRV As DataRowView
                For Each DRV In DT_SQL.DefaultView
                    DRV.BeginEdit()
                    DRV.Item("SUBJ_G_ID") = DT_ORACLE.Rows(i).Item("SUBJ_G_ID")
                    DRV.Item("SUBJ_G_DESC") = DT_ORACLE.Rows(i).Item("SUBJ_G_DESC")
                    DRV.Item("Update_Time") = Now
                    DRV.EndEdit()
                Next
                Dim cmd As New SqlCommandBuilder(DA_SQL)
                DA_SQL.Update(DT_SQL)
            End If
            DT_SQL.DefaultView.RowFilter = ""
            RaiseEvent RecordCountChanged("tb_PK_SUBJ_G", i + 1)
        Next
        RaiseEvent SyncComplete("tb_PK_SUBJ_G", "Complete")
    End Sub


    Public Sub SyncAllDAta(ByVal OracleConnStr As String, ByVal SqlConnStr As String)
        Sync_tb_PN_DEPARTMENT_R()
        Sync_tb_PN_PERSONAL_STAT_R()
        Sync_tb_PN_PERSONAL_VIEW()
        Sync_tb_PN_POSITION_FLD_R()
        Sync_tb_PN_POSITION_MGR_R()
        Sync_tb_PN_POSITION_T()
        Sync_tb_PN_LEAVE_T()
        Sync_tb_PN_POSITION_FLD_FUNC()
        Sync_tb_PK_COURSE()
        Sync_tb_PK_EMP_SELECT()
        Sync_tb_PN_PUNISH_CODE_R()
        Sync_tb_PN_PUNISH_T()
        Sync_tb_CP_SUBJECT_MASTER()
        Sync_tb_CP_SUBJECT_PERSONAL()
        Sync_tb_PN_POSITION_HISTORY_T()
        Sync_tb_PN_POSITION_RETURN2CLASS()
        Sync_tb_PK_SUBJ_G()
        RaiseEvent SyncAllDataComplete("Complete")
    End Sub
End Class
