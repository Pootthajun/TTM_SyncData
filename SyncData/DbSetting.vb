﻿Imports SyncData.Org.Mentalis.Files
Imports System.Data
Imports System.Windows.Forms.MessageBox
Imports System.Data.SqlClient
Imports System.Data.OracleClient
Imports SyncData

Public Class DbSetting
    Public INIFileName As String = "C:\Windows\SyncData.ini"  'Parth ที่ใช้เก็บไฟล์ .ini

    Private Sub DbSetting_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SyncData"
        txtOraSN.Text = CStr(ini.ReadString("OraSN"))
        txtOraUS.Text = CStr(ini.ReadString("OraUS"))
        txtOraPW.Text = CStr(ini.ReadString("OraPW"))

        txtSqlSV.Text = CStr(ini.ReadString("SqlSV"))
        txtSqlDB.Text = CStr(ini.ReadString("SqlDB"))
        txtSqlUS.Text = CStr(ini.ReadString("SqlUS"))
        txtSqlPW.Text = CStr(ini.ReadString("SqlPW"))
    End Sub

    Function getConnectionStringOraTest() As String
        Return "Data Source=" & txtOraSN.Text & ";User Id=" & txtOraUS.Text & ";Password=" & txtOraPW.Text & ";"
    End Function

    Function getConnectionStringSqlTest() As String
        Return "Data Source=" & txtSqlSV.Text & ";Initial Catalog=" & txtSqlDB.Text & ";Persist Security Info=True;User ID=" & txtSqlUS.Text & ";Password=" & txtSqlPW.Text & ";"
    End Function

    Public Function CheckConnOra(ByVal ConnectionString As String) As Boolean
        Dim ConnOra As New OracleConnection
        Try
            If ConnOra.State = ConnectionState.Open Then
                ConnOra.Close()
            End If
            ConnOra.ConnectionString = ConnectionString
            ConnOra.Open()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function CheckConnSql(ByVal ConnectionString As String) As Boolean
        Dim ConnSql As New SqlConnection
        Try
            If ConnSql.State = ConnectionState.Open Then
                ConnSql.Close()
            End If
            ConnSql.ConnectionString = ConnectionString
            ConnSql.Open()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub btnOraTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOraTest.Click
        If txtOraSN.Text = "" Then
            System.Windows.Forms.MessageBox.Show("Please Enter Service Name !!!", "Attention", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Information)
            txtOraSN.Focus()
            Exit Sub
        End If

        If txtOraUS.Text = "" Then
            System.Windows.Forms.MessageBox.Show("Please Enter Username !!!", "Attention", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Information)
            txtOraUS.Focus()
            Exit Sub
        End If

        If txtOraPW.Text = "" Then
            System.Windows.Forms.MessageBox.Show("Please Enter Password !!!", "Attention", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Information)
            txtOraPW.Focus()
            Exit Sub
        End If

        If CheckConnOra(getConnectionStringOraTest) = False Then
            MsgBox("The test did not succeed.")
            txtOraSN.Focus()
            Exit Sub
        End If

        MsgBox("The connection test was successful.")

    End Sub

    Private Sub btnSqlTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSqlTest.Click
        If txtSqlSV.Text = "" Then
            System.Windows.Forms.MessageBox.Show("Please Enter Server Name !!!", "Attention", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Information)
            txtSqlSV.Focus()
            Exit Sub
        End If

        If txtSqlDB.Text = "" Then
            System.Windows.Forms.MessageBox.Show("Please Enter Database !!!", "Attention", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Information)
            txtSqlDB.Focus()
            Exit Sub
        End If

        If txtSqlUS.Text = "" Then
            System.Windows.Forms.MessageBox.Show("Please Enter Username !!!", "Attention", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Information)
            txtSqlUS.Focus()
            Exit Sub
        End If

        If CheckConnSql(getConnectionStringSqlTest) = False Then
            MsgBox("The test did not succeed.")
            txtOraSN.Focus()
            Exit Sub
        End If

        MsgBox("The connection test was successful.")
    End Sub

    Private Sub txtOraSN_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOraSN.LostFocus
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SyncData"
        ini.Write("OraSN", txtOraSN.Text)
    End Sub

    Private Sub txtOraUS_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOraUS.LostFocus
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SyncData"
        ini.Write("OraUS", txtOraUS.Text)
    End Sub

    Private Sub txtOraPW_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOraPW.LostFocus
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SyncData"
        ini.Write("OraPW", txtOraPW.Text)
    End Sub

    Private Sub txtSqlSV_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSqlSV.LostFocus
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SyncData"
        ini.Write("SqlSV", txtSqlSV.Text)
    End Sub

    Private Sub txtSqlDB_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSqlDB.LostFocus
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SyncData"
        ini.Write("SqlDB", txtSqlDB.Text)
    End Sub

    Private Sub txtSqlUS_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSqlUS.LostFocus
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SyncData"
        ini.Write("SqlUS", txtSqlUS.Text)
    End Sub

    Private Sub txtSqlPW_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSqlPW.LostFocus
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SyncData"
        ini.Write("SqlPW", txtSqlPW.Text)
    End Sub
End Class