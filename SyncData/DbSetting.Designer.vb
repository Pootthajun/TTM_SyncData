﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DbSetting
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnOraTest = New System.Windows.Forms.Button
        Me.txtOraPW = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtOraUS = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtOraSN = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.btnSqlTest = New System.Windows.Forms.Button
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtSqlSV = New System.Windows.Forms.TextBox
        Me.txtSqlPW = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtSqlUS = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtSqlDB = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnOraTest)
        Me.GroupBox1.Controls.Add(Me.txtOraPW)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtOraUS)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtOraSN)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(204, 164)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Oracle"
        '
        'btnOraTest
        '
        Me.btnOraTest.Location = New System.Drawing.Point(92, 129)
        Me.btnOraTest.Name = "btnOraTest"
        Me.btnOraTest.Size = New System.Drawing.Size(100, 23)
        Me.btnOraTest.TabIndex = 2
        Me.btnOraTest.Text = "Test Connection"
        Me.btnOraTest.UseVisualStyleBackColor = True
        '
        'txtOraPW
        '
        Me.txtOraPW.Location = New System.Drawing.Point(92, 75)
        Me.txtOraPW.Name = "txtOraPW"
        Me.txtOraPW.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtOraPW.Size = New System.Drawing.Size(100, 20)
        Me.txtOraPW.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Service Name"
        '
        'txtOraUS
        '
        Me.txtOraUS.Location = New System.Drawing.Point(92, 49)
        Me.txtOraUS.Name = "txtOraUS"
        Me.txtOraUS.Size = New System.Drawing.Size(100, 20)
        Me.txtOraUS.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Username"
        '
        'txtOraSN
        '
        Me.txtOraSN.Location = New System.Drawing.Point(92, 23)
        Me.txtOraSN.Name = "txtOraSN"
        Me.txtOraSN.Size = New System.Drawing.Size(100, 20)
        Me.txtOraSN.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 78)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Password"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnSqlTest)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.txtSqlSV)
        Me.GroupBox2.Controls.Add(Me.txtSqlPW)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtSqlUS)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.txtSqlDB)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Location = New System.Drawing.Point(236, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(204, 164)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Sql Server"
        '
        'btnSqlTest
        '
        Me.btnSqlTest.Location = New System.Drawing.Point(92, 129)
        Me.btnSqlTest.Name = "btnSqlTest"
        Me.btnSqlTest.Size = New System.Drawing.Size(100, 23)
        Me.btnSqlTest.TabIndex = 8
        Me.btnSqlTest.Text = "Test Connection"
        Me.btnSqlTest.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 52)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(53, 13)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Database"
        '
        'txtSqlSV
        '
        Me.txtSqlSV.Location = New System.Drawing.Point(92, 23)
        Me.txtSqlSV.Name = "txtSqlSV"
        Me.txtSqlSV.Size = New System.Drawing.Size(100, 20)
        Me.txtSqlSV.TabIndex = 11
        '
        'txtSqlPW
        '
        Me.txtSqlPW.Location = New System.Drawing.Point(92, 103)
        Me.txtSqlPW.Name = "txtSqlPW"
        Me.txtSqlPW.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtSqlPW.Size = New System.Drawing.Size(100, 20)
        Me.txtSqlPW.TabIndex = 15
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 106)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Password"
        '
        'txtSqlUS
        '
        Me.txtSqlUS.Location = New System.Drawing.Point(92, 75)
        Me.txtSqlUS.Name = "txtSqlUS"
        Me.txtSqlUS.Size = New System.Drawing.Size(100, 20)
        Me.txtSqlUS.TabIndex = 13
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 78)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Username"
        '
        'txtSqlDB
        '
        Me.txtSqlDB.Location = New System.Drawing.Point(92, 49)
        Me.txtSqlDB.Name = "txtSqlDB"
        Me.txtSqlDB.Size = New System.Drawing.Size(100, 20)
        Me.txtSqlDB.TabIndex = 12
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Server Name"
        '
        'DbSetting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(451, 188)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "DbSetting"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DbSetting"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtOraSN As System.Windows.Forms.TextBox
    Friend WithEvents txtOraUS As System.Windows.Forms.TextBox
    Friend WithEvents txtOraPW As System.Windows.Forms.TextBox
    Friend WithEvents btnOraTest As System.Windows.Forms.Button
    Friend WithEvents txtSqlUS As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtSqlDB As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtSqlSV As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnSqlTest As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtSqlPW As System.Windows.Forms.TextBox
End Class
